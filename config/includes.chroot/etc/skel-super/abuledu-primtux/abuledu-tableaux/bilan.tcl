#!/bin/sh
#bilan.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier: andre.connes@toulouse.iufm.fr
#  Date    : 24/04/2002 modification : 26/03/2005
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     David Lucardi
#  @modifier   Andre Connes
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *************************************************************************

source tableaux.conf

set didacticiel ""
source msg.tcl
source fonts.tcl

# pour chaque didacticiel/module/difficulte faire une sentinelle
foreach didacticiel $glob(didacticiels) {
  for { set i 0 } { $i <= 5 } { incr i } {
    for { set j -1 } { $j <= 5 } { incr j } {
      set lt_array($didacticiel,$i,$j,duree) 0
      set lt_array($didacticiel,$i,$j,reussite) 0
      set lt_array($didacticiel,$i,$j,nbtours) 0
    }
  }
}
# les sentinelles sont en place

. configure -width 640 -height 480
wm geometry . +0+0
wm title . [mc bilan]

#frame pour les boutons de choix : eleve classe tous
#
frame .top -borderwidth 10
pack .top -side top -fill x
#
#creer les boutons
#
  # Relire le nom de l'utilisateur sous windows
  if {$glob(platform) == "windows"} {
    catch {set f [open [file join $glob(home_tableaux) reglages trace_user] "r"]}
    gets $f glob(trace_user)
    close $f
  }
set env(USER) [lindex [split $glob(trace_user) / ] end]
if { [file exists [file join $glob(trace_dir) $env(USER).log]] } {
  button .top.but_lemien -text [mc {Mon bilan}] -command mon_bilan
} else {
  button .top.but_lemien -state disable -text [mc {Mon bilan}]
}
if { $env(HOME) == "/home/profs/$env(USER)" } {
  button .top.but_eleve -text [mc eleve] -command bilan_eleve
  button .top.but_classe -state disable -text [mc classe] -command bilan_classe
  button .top.but_tous -text [mc tous] -command bilan_tous
} elseif { $glob(platform) == "windows" } {
  button .top.but_eleve -text [mc eleve] -command bilan_eleve
  button .top.but_classe -state disable -text [mc classe] -command bilan_classe
  button .top.but_tous -state disable -text [mc tous] -command bilan_tous
} else {
  button .top.but_eleve -state disable -text [mc eleve] -command bilan_eleve
  button .top.but_classe -state disable -text [mc classe] -command bilan_classe
  button .top.but_tous -state disable -text [mc tous] -command bilan_tous
}
button .top.but_efface -state disabled -text [mc effacer_fichiers] -command efface_bilan
button .top.but_exit -text [mc Fermer] -command exit
pack .top.but_lemien .top.but_eleve .top.but_classe .top.but_tous .top.but_efface .top.but_exit -side left

set bg #ffffff

frame .t
text .t.text -yscrollcommand {.t.scroll set} -setgrid true \
        -width 80 -height 40 -wrap word -background $bg
scrollbar .t.scroll -command {.t.text yview}
pack .t.scroll -side right -fill y
pack .t.text -side left -fill both -expand true
pack .t -side top -fill both -expand true

frame .b
pack .b -side bottom -fill x


############################################################
proc bilan {qui} {
  global env glob lt_array didacticiel fnom file
  set curdir [pwd]
  cd $glob(trace_dir)
  set file $qui
  if {[catch { set f [open [file join $file] "r" ] }] } {
        exit
  }
  close $f
  #
  # on recupere le nom
  #
  set xnom [lindex [split $file "/"] end]
  set ppoint [string last "." $xnom]
  set nom [string range $xnom 0 [expr $ppoint-1]]
  #
  # lire et traiter les resulats
  #
  set f [open [file join $file] "r" ]
  while {![eof $f]} {
    set ligne [gets $f]
    if { [string length $ligne] > 0 } {
      set nom_bof [lindex $ligne 0]
      set classe [lindex $ligne 1]
    }
    set tmp_date [lindex $ligne 2]
    set duree [lindex $ligne 3]
    set didacticiel [lindex [lindex $ligne 4] 0]
    set module [lindex [lindex $ligne 4] 1]
    set difficulte [lindex [lindex $ligne 4] 2]
    set version [lindex $ligne 5]
    if { [lindex [lindex $ligne 6] 0] == [lindex [lindex $ligne 6] 1] } {
      set reussite 1
    } else {
      set reussite 0
    }
    if { $didacticiel != ""} {
      incr lt_array($didacticiel,$module,$difficulte,duree) [expr 0+$duree]
      incr lt_array($didacticiel,$module,$difficulte,reussite) [expr 0+$reussite]
      incr lt_array($didacticiel,$module,$difficulte,nbtours)

      incr lt_array($didacticiel,$module,5,duree) [expr 0+$duree]
      incr lt_array($didacticiel,$module,5,reussite) [expr 0+$reussite]
      incr lt_array($didacticiel,$module,5,nbtours)

      incr lt_array($didacticiel,5,$difficulte,duree) [expr 0+$duree]
      incr lt_array($didacticiel,5,$difficulte,reussite) [expr 0+$reussite]
      incr lt_array($didacticiel,5,$difficulte,nbtours)

      incr lt_array($didacticiel,5,5,duree) [expr 0+$duree]
      incr lt_array($didacticiel,5,5,reussite) [expr 0+$reussite]
      incr lt_array($didacticiel,5,5,nbtours)
    }
  }
  close $f

  # #######################################################
  # les r�sultats sont maintenant dans le tableau lt_array
  # quels traitements ?
  # #######################################################

  set fnom [file join [pwd] $nom.bilan]
  set f [open $fnom.html w]
  set ojourdui [clock format [clock seconds] -format "%d/%m/%Y"]
  puts $f "<html>\n<head>\n<title>Bilan de $nom</title>\n</head><body>\n"
  puts $f "<h2> $ojourdui - [mc {synth�se}] : $nom - [mc {classe}] : $classe</h2>"

  puts $f "<h1>\n\n[mc {pourcent_reussite}]</h1>"
  puts $f "<table border='1' cellspacing='0'>\n<tr>\n<th>didacticiel</th>"
  puts $f "<th>[mc bilan_c0]</th><th>[mc bilan_c1]</th><th>[mc bilan_c2]</th>"
  puts $f "<th>[mc bilan_c3]</th><th>[mc bilan_c4]</th><th>[mc bilan_c5]</th></tr>"

  foreach didacticiel $glob(didacticiels) {
    for { set i 0 } { $i <= 5 } { incr i } {
      set line "$didacticiel $i : "
      for { set j 0 } { $j <= 5 } { incr j } {
        if { $lt_array($didacticiel,$i,$j,nbtours)> 0 } {
          set v($j) [expr 100*$lt_array($didacticiel,$i,$j,reussite)/$lt_array($didacticiel,$i,$j,nbtours)]
        } else {
          set v($j) "."
        }
      }
      if { $lt_array($didacticiel,$i,5,nbtours) >0 } {
        puts $f "<tr>\n<th>$didacticiel<p>[mc sub_menu$i]</th>"
	puts $f "<th>$v(0)</th><th>$v(1)</th><th>$v(2)</th>"
	puts $f "<th>$v(3)</th><th>$v(4)</th><th>$v(5)</th></tr>"
      }
    }
  }
  puts $f "</table>\n"
  
# if {$glob(platform) != "windows" } {
  puts $f "<h1>\n\n[mc {duree_moyenne}]</h1>"
  puts $f "<table border='1' cellspacing='0'>\n<tr>\n<th>didacticiel</th>"
  puts $f "<th>[mc bilan_c0]</th><th>[mc bilan_c1]</th><th>[mc bilan_c2]</th>"
  puts $f "<th>[mc bilan_c3]</th><th>[mc bilan_c4]</th><th>[mc bilan_c5]</th></tr>"

  foreach didacticiel $glob(didacticiels) {
    for { set i 0 } { $i <= 5 } { incr i } {
      set line "$didacticiel $i : "
      for { set j 0 } { $j <= 5 } { incr j } {
        if { $lt_array($didacticiel,$i,$j,nbtours)> 0 } {
          set v($j) \
	    [format "%.1f" \
		[expr 1.0*$lt_array($didacticiel,$i,$j,duree)/$lt_array($didacticiel,$i,$j,nbtours)]
	    ]
        } else {
          set v($j) "."
        }
      }
      if { $lt_array($didacticiel,$i,5,nbtours) >0 } {
        puts $f "<tr>\n<th>$didacticiel<p>[mc sub_menu$i]</th>"
	puts $f "<th>$v(0)</th><th>$v(1)</th><th>$v(2)</th>"
	puts $f "<th>$v(3)</th><th>$v(4)</th><th>$v(5)</th></tr>"
      }
    }
  }
  puts $f "</table>"
 #}
  puts $f "\n</body>\n</html>"
  close $f
  #fin bilan.html
  #
  # on change les boutons
  #
  if { [file exists [file join $glob(trace_dir) $env(USER).log]] } {
    .top.but_lemien configure -state normal
  } else {
    .top.but_lemien configure -state disable
  }
  .t.text insert end "\n[mc trace_en] \n $file"
  .t.text see end
  .t.text insert end "\n[mc synthese_en] \n $fnom.html "
  .t.text see end
  .t.text insert end "\n\n*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*\n "
  .t.text see end
  #
  # on revient au r�pertoire d'avant le bilan
  #
  cd $curdir
  show_html_bilan
  .top.but_efface configure -state normal
} ;# fin bilan

###################################################################################

proc mon_bilan {} {
  global env glob
  set qui [file join $glob(trace_user).log]
  bilan $qui
}

proc bilan_eleve { } {
  global glob
  if { [file exists /home/eleves] } {
    set types {
        {"Cat�gories" {/home/eleves/*/leterrier/tableaux/log/*.log}        }
    }
  } else {
    set types {
        {"Cat�gories" *.log        }
    }
  }
  catch {set qui [tk_getOpenFile -filetypes $types]}
  if {[catch { set f [open [file join $qui] "r" ] }] } {
        exit
  }
  close $f
  bilan $qui
}

proc bilan_classe {} {
  .t.text insert end "\n\nBilan classe : [mc non_implante].\n\n"
}

proc bilan_tous {} {
  global glob
  # exec cp_bilans ne fonctionne pas : bug ?
  set cpbilans ./cp_bilans
  exec $cpbilans
  set qui [glob [file join $glob(trace_dir) *_eleves.log]] 
  bilan $qui
}

proc efface_bilan {} {
  global fnom file
  catch { file delete $file}
  file delete "$fnom.html"
  tk_messageBox -message "Fichier(s) effac�(s)."
  .top.but_efface configure -state disabled
}

proc show_html_bilan {} {
  global fnom glob
  if {$glob(platform) == "windows"} {
    set fichier [file attributes "$fnom.html" -shortname]
  } else {
    set fichier "$fnom.html"
  }
  catch { exec $glob(progaide) file:$fichier & }

}
