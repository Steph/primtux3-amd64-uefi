############################################################################
# Copyright (C) 2002 Eric Seigne
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : anous.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: anous.tcl,v 1.6 2006/01/21 14:12:11 david Exp $
# @author     David Lucardi
# @project
# @copyright  Eric Seigne
#
#
#########################################################################
#!/bin/sh
#anous.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}


set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source fonts.tcl
source path.tcl
source msg.tcl


init $plateforme
inithome
initlog $plateforme $ident
changehome
wm geometry . +0+0
#####################################################################"
proc majmenu {} {
global Home
catch {.menu.action.m1 delete 0 end}
catch {.menu.action.m2 delete 0 end}
catch {.menu.action.m3 delete 0 end}
catch {.menu.action.m4 delete 0 end}
catch {.menu.action.m5 delete 0 end}
catch {.menu.action.m6 delete 0 end}
catch {.menu.action.m7 delete 0 end}


set f [open [file join $Home reglages calapa.conf] "r"]
set listdata [gets $f]
close $f
	for {set k 0} {$k < [llength $listdata]} {incr k 1} {
	.menu.action.m1 add command -label "[lindex [lindex [lindex $listdata $k] 0] 0]" -command "lanceappli place1.tcl calapa $k 0"
	}
set f [open [file join $Home reglages barque.conf] "r"]
set listdata [gets $f]
close $f
	for {set k 0} {$k < [llength $listdata]} {incr k 1} {
	.menu.action.m2 add command -label "[lindex [lindex [lindex $listdata $k] 0] 0]" -command "lanceappli place1.tcl barque $k 0"
	}

set f [open [file join $Home reglages train.conf] "r"]
set listdata [gets $f]
close $f
	for {set k 0} {$k < [llength $listdata]} {incr k 1} {
	.menu.action.m3 add command -label "[lindex [lindex [lindex $listdata $k] 0] 0]" -command "lanceappli train.tcl train $k 0"
	}


set f [open [file join $Home reglages constellation.conf] "r"]
set listdata [gets $f]
close $f
	for {set k 0} {$k < [llength $listdata]} {incr k 1} {
	.menu.action.m4 add command -label "[lindex [lindex [lindex $listdata $k] 0] 0]" -command "lanceappli place1.tcl constellation $k 0"
	}

set f [open [file join $Home reglages nombre.conf] "r"]
set listdata [gets $f]
close $f
	for {set k 0} {$k < [llength $listdata]} {incr k 1} {
	.menu.action.m5 add command -label "[lindex [lindex [lindex $listdata $k] 0] 0]" -command "lanceappli place1.tcl nombre $k 0"
	}

set f [open [file join $Home reglages ordinal.conf] "r"]
set listdata [gets $f]
close $f
	for {set k 0} {$k < [llength $listdata]} {incr k 1} {
	.menu.action.m6 add command -label "[lindex [lindex [lindex $listdata $k] 0] 0]" -command "lanceappli ordinal.tcl ordinal $k 0"
	}
set f [open [file join $Home reglages voir.conf] "r"]
set listdata [gets $f]
close $f
	for {set k 0} {$k < [llength $listdata]} {incr k 1} {
	.menu.action.m7 add command -label "[lindex [lindex [lindex $listdata $k] 0] 0]" -command "lanceappli quevoir.tcl voir $k 0"
	}

}


#######################################################################"

proc interface {} {

global iwish basedir Home plateforme progaide sysFont abuledu prof baseHome

set f [open [file join $Home reglages repert.conf] "r"]
set listdata [gets $f]
close $f
set repert $listdata

. configure -background #ffff80
catch {
destroy .menu
destroy .wleft
destroy .wcenter
destroy .wright
}
menu .menu

# Creation du menu Fichier
menu .menu.fichier -tearoff 0
.menu add cascade -label [mc {Fichier}] -menu .menu.fichier

.menu.fichier add cascade -label [mc {Editeur}] -menu .menu.fichier.editeur

menu .menu.fichier.editeur -tearoff 0

.menu.fichier.editeur add command -label [mc {Calapa}] -command "exec $iwish editcalapa.tcl calapa &"
.menu.fichier.editeur add command -label [mc {Constellation}] -command "exec $iwish editcalapa.tcl constellation &"
.menu.fichier.editeur add command -label [mc {Nombre}] -command "exec $iwish editcalapa.tcl nombre &"
.menu.fichier.editeur add command -label [mc {Barques}] -command "exec $iwish editbarques.tcl barque &"
.menu.fichier.editeur add command -label [mc {Train}] -command "exec $iwish edittrain.tcl train &"
.menu.fichier.editeur add command -label [mc {Ordinal}] -command "exec $iwish editordinal.tcl ordinal &"
.menu.fichier.editeur add command -label [mc {Que voir}] -command "exec $iwish editvoir.tcl voir &"
.menu.fichier.editeur add command -label [mc {Parcours}] -command "exec $iwish editparcours.tcl parcours &"


.menu.fichier add command -label [mc {Bilans}] -command "exec $iwish bilan.tcl &"
.menu.fichier add sep
.menu.fichier add command -label [mc {Quitter}] -command exit

menu .menu.action -tearoff 0
.menu add cascade -label [mc {Activites}] -menu .menu.action
menu .menu.action.m1 -tearoff 0
.menu.action add cascade -label [mc {Calapa}] -menu .menu.action.m1

menu .menu.action.m2 -tearoff 0 
.menu.action add cascade -label [mc {Barques}] -menu .menu.action.m2 
menu .menu.action.m3 -tearoff 0 
.menu.action add cascade -label [mc {Train}] -menu .menu.action.m3 
menu .menu.action.m4 -tearoff 0 
.menu.action add cascade -label [mc {Constellation}] -menu .menu.action.m4 
menu .menu.action.m5 -tearoff 0 
.menu.action add cascade -label [mc {Nombre}] -menu .menu.action.m5 
menu .menu.action.m6 -tearoff 0 
.menu.action add cascade -label [mc {Ordinal}] -menu .menu.action.m6
menu .menu.action.m7 -tearoff 0 
.menu.action add cascade -label [mc {Que voir}] -menu .menu.action.m7 
 
.menu.action add command -label [mc {Parcours}] -command "parcours" 

menu .menu.options -tearoff 0
.menu add cascade -label [mc {Reglages}] -menu .menu.options

set ext .msg
menu .menu.options.lang -tearoff 0 
.menu.options add cascade -label "[mc {Langue}]" -menu .menu.options.lang

foreach i [glob [file join  $basedir msgs *$ext]] {
set langue [string map {.msg ""} [file tail $i]]
.menu.options.lang add radio -label $langue -variable langue -command "setlang $langue"
}


menu .menu.options.dossier -tearoff 0 
.menu.options add cascade -label [mc {Dossier de travail}] -menu .menu.options.dossier
.menu.options.dossier add radio -label [mc {Individuel}] -variable repert -value 0 -command "changehomeupdate"
.menu.options.dossier add radio -label [mc {Commun}] -variable repert -value 1 -command "changehomeupdate"


changehome

if {$plateforme == "windows"} {
.menu add command -label [mc {Utilisateur}] -command "setwindowsusername"
}

menu .menu.aide -tearoff 0
.menu add cascade -label [mc {?}] -menu .menu.aide


	if {[file exists [file join $baseHome reglages lang.conf]] == 1} {
	set f [open [file join $baseHome reglages lang.conf] "r"]
  	gets $f lang
  	close $f
	set fich "_index.htm"
	set fich $lang$fich
	set fichier [file join [pwd] aide $fich]
	} else {
	set fichier [file join [pwd] aide fr_index.htm]
	}
  


if {[file exists $fichier] != 1} {
set fichier [file join [pwd] aide fr_index.htm]
}



.menu.aide add command -label [mc {Aide}] -command "exec $progaide file:$fichier &"
.menu.aide add command -label [mc {A propos ...}] -command "source apropos.tcl"

. configure -menu .menu

. configure -background blue
frame .wleft -background blue -height 320 -width 220
pack .wleft -side left

set myimage [image create photo -file sysdata/calapa.gif]
button .wleft.labA1 -image $myimage -text [mc {Calapa}] -background blue -font $sysFont(t) -pady 6 -justify left -foreground orange -height 128 -width 130
pack .wleft.labA1 -side top -anchor w
bind .wleft.labA1 <1> "lanceappli place1.tcl calapa 0 0"
#bind .wleft.labA1 <Any-Enter> ".wleft.labA1 configure -foreground yellow"
#bind .wleft.labA1 <Any-Leave> ".wleft.labA1 configure -foreground orange"

set myimage [image create photo -file sysdata/barques.gif]
button .wleft.labA2 -image $myimage -text [mc {Barques}] -background blue -font $sysFont(t) -pady 6 -justify left -foreground orange -height 128 -width 130
pack .wleft.labA2 -side top -anchor w
bind .wleft.labA2 <1> "lanceappli place1.tcl barque 0 0"
#bind .wleft.labA2 <Any-Enter> ".wleft.labA2 configure -foreground yellow"
#bind .wleft.labA2 <Any-Leave> ".wleft.labA2 configure -foreground orange"

set myimage [image create photo -file sysdata/train.gif]
button .wleft.labA3 -image $myimage -text [mc {Train}] -background blue -font $sysFont(t) -pady 6 -justify left -foreground orange -height 128 -width 130
pack .wleft.labA3 -side top -anchor w
bind .wleft.labA3 <1> "lanceappli train.tcl train 0 0"
#bind .wleft.labA3 <Any-Enter> ".wleft.labA3 configure -foreground yellow"
#bind .wleft.labA3 <Any-Leave> ".wleft.labA3 configure -foreground orange"



frame .wcenter -background blue -height 320 -width 220
pack .wcenter -side left

frame .wright -background blue -height 320 -width 220
pack .wright -side left
set myimage [image create photo -file sysdata/constell.gif]
button .wright.labA2 -image $myimage -text [mc {Constellation}] -background blue -font $sysFont(t) -pady 6 -justify right -foreground orange -height 128 -width 130
pack .wright.labA2 -side top -anchor e
bind .wright.labA2 <1> "lanceappli place1.tcl constellation 0 0"
#bind .wright.labA2 <Any-Enter> ".wright.labA2 configure -foreground yellow"
#bind .wright.labA2 <Any-Leave> ".wright.labA2 configure -foreground orange"

set myimage [image create photo -file sysdata/nombres.gif]
button .wright.labA3 -image $myimage -text [mc {Nombre}] -background blue -font $sysFont(t) -pady 6 -justify right -foreground orange -height 128 -width 130
pack .wright.labA3 -side top -anchor e
bind .wright.labA3 <1> "lanceappli place1.tcl nombre 0 0"
#bind .wright.labA3 <Any-Enter> ".wright.labA3 configure -foreground yellow"
#bind .wright.labA3 <Any-Leave> ".wright.labA3 configure -foreground orange"

set myimage [image create photo -file sysdata/ordinal.gif]
button .wright.labA4 -image $myimage -text [mc {Ordinal}] -background blue -font $sysFont(t) -pady 6 -justify right -foreground orange -height 130 -width 130
pack .wright.labA4 -side top -anchor e
bind .wright.labA4 <1> "lanceappli ordinal.tcl ordinal 0 0"
#bind .wright.labA4 <Any-Enter> ".wright.labA4 configure -foreground yellow"
#bind .wright.labA4 <Any-Leave> ".wright.labA4 configure -foreground orange"

set myimage [image create photo -file sysdata/quevoir.gif]
button .wcenter.labA4 -image $myimage -text [mc {Que voir}] -background blue -font $sysFont(t) -pady 6 -justify right -foreground orange -height 118 -width 130
pack .wcenter.labA4 -side top
bind .wcenter.labA4 <1> "lanceappli quevoir.tcl voir 0 0"
#bind .wleft.labA4 <Any-Enter> ".wleft.labA4 configure -foreground yellow"
#bind .wleft.labA4 <Any-Leave> ".wleft.labA4 configure -foreground orange"

set myimage [image create photo -file sysdata/background.gif]
label .wcenter.imagedisplayer -image $myimage -background blue
pack .wcenter.imagedisplayer -side top
set myimage [image create photo -file sysdata/parcours.gif]
button .wcenter.labA1 -image $myimage -text [mc {Parcours}] -background blue -font $sysFont(t) -pady 6 -justify right -foreground orange -height 118 -width 130
pack .wcenter.labA1 -side top
bind .wcenter.labA1 <1> "parcours"
#bind .wcenter.labA1 <Any-Enter> ".wcenter.labA1 configure -foreground yellow"
#bind .wcenter.labA1 <Any-Leave> ".wcenter.labA1 configure -foreground orange"



majmenu

}

interface

############
# Bindings #
############
bind . <Control-q> {exit}

bind . <FocusIn> {majmenu}



proc lanceappli {appli what niveau parcours} {
global iwish
set appli [file join $appli]
#appli.tcl activit�index_dans_le_sc�ario flag_parcours
exec $iwish $appli $what $niveau $parcours &
}


proc parcours {} {
global Home iwish
	set f [open [file join $Home reglages parcours.conf] "r"]
	set listparcours [gets $f]
	close $f
	set activite [lindex [lindex $listparcours 0] 0]
	set ext .conf
	set f [open [file join $Home reglages [lindex $activite 0]$ext] "r"]
	set listactivite [gets $f]
	close $f
	set compt 0
		foreach item $listactivite {
		if {[string first [lindex [lindex $listparcours 0] 1] $item] != -1} {
		break}
		incr compt
		}
		if {$compt >= [llength $listactivite ]} {
		tk_messageBox -message [format [mc {Le scenario %1$s n'existe pas.}] [lindex $listparcours 0]] -type ok -title [mc {A nous les nombres}]
		return
		}
		if {[lindex $activite 0] == "calapa"} {
		#appli.tcl activit�index_dans_le_sc�ario flag_parcours index_dans_le_parcours
		exec $iwish place1.tcl calapa $compt 1 0 &
		}
		if {[lindex $activite 0] == "constellation"} {
		exec $iwish place1.tcl constellation $compt 1 0 &
		}
		if {[lindex $activite 0] == "nombre"} {
		exec $iwish place1.tcl nombre $compt 1 0 &
		}
		if {[lindex $activite 0] == "barque"} {
		exec $iwish place1.tcl barque $compt 1 0 &
		}
		if {[lindex $activite 0] == "train"} {
		exec $iwish train.tcl train $compt 1 0 &
		}
		if {[lindex $activite 0] == "ordinal"} {
		exec $iwish ordinal.tcl ordinal $compt 1 0 &
		}
		if {[lindex $activite 0] == "voir"} {
		exec $iwish quevoir.tcl voir $compt 1 0 &
		}

}


proc changehomeupdate {} {
changehome
interface
majmenu
}

proc setlang {lang} {
global env plateforme baseHome
set env(LANG) $lang
set f [open [file join $baseHome reglages lang.conf] "w"]
puts $f $lang
close $f

::msgcat::mclocale $lang
::msgcat::mcload [file join [file dirname [info script]] msgs]
interface
}


