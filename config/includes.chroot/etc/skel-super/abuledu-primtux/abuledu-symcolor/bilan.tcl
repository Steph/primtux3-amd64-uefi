############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : fichier.php
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#
#
#########################################################################
#!/bin/sh
#bilan.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

source path.tcl

global plateforme
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
initlog $plateforme $ident

set c .frame.c
set bgn #ffff80
set bgl #ff80c0

source fonts.tcl


source msg.tcl



. configure -background black -width 640 -height 480
wm geometry . +0+0

set types {
	{"Catégories"		{.log}	}
         }
catch {set file [tk_getOpenFile -initialdir $LogHome -filetypes $types]}

set file $file

wm title . Bilan\040[lindex [split [lindex [split $file /] end] .] 0]
text .text -yscrollcommand ".scroll set" -setgrid true -width 49 -height 20 -wrap word -background black -font $sysFont(l)
scrollbar .scroll -command ".text yview"
pack .scroll -side right -fill y
pack .text -expand yes -fill both

.text tag configure green -foreground green
.text tag configure red -foreground red
.text tag configure yellow -foreground yellow
.text tag configure normal -foreground black
.text tag configure white -foreground white


if {[catch { set f [open [file join $file] "r" ] }] } {
 
      set answer [tk_messageBox -message [mc {Erreur de fichier}] -type ok -icon info]
      exit
}

       while {![eof $f]} {
         set listeval [gets $f]
         .text insert end "[mc {Image}] :[lindex $listeval 0]\n" red
	 .text insert end "[mc {Nombre d'essais}] :[lindex $listeval 1]\n" white
         .text insert end "[mc {Nombre total d'erreurs}] :[lindex $listeval 2]\n" white                                  
          }
	   .text insert end \n
            
 
close $f













