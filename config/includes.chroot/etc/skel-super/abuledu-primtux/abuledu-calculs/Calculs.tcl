#!/bin/sh
#Calcul_en_Pluie.tcl
#\
exec wish "$0" ${1+"$@"}
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jlsendral@free.fr
#  Modifier:
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#  @version
#  @author     Jean-Louis Sendral
#  @modifier
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *************************************************************************
##bind . <F1> "showaide { }"
global sysFont rep_travail
set basedir [pwd]
## pour macro openoffice win
##set basedir [file dirname [lindex $argv 0 ] ]
##set basedir "C:/Program\ Files/calculs"
cd $basedir
source calculs.conf
#source i18n
source msg.tcl
source fonts.tcl
source path.tcl
set plateforme $tcl_platform(platform) ; set ident $tcl_platform(user)
init $plateforme
#initlog $plateforme  $ident

set rep_travail $basedir
if { $tcl_platform(platform) == "unix" } {
set nom_elev $tcl_platform(user)
set nom_classe [lindex [exec  id -G -n ] 1]
if { $nom_classe == {} } {
set nom_classe classe
}
} else {
 set nom_elev eleve
 set nom_classe classe
}



#utilit� de ce cd ?
wm geometry . +0+0
. configure -background blue
##wm resizable . 0 0
wm title . "[mc {Calculs}]mental,d'expressions,d'�quations, de quotients"
##. configure -background #ffff80

proc interface {} {
global Home DOSSIER_EXOS basedir rep_travail
catch {destroy .menu}
catch {destroy .frame}
global tcl_platform basedir progaide
set font {Arial 16}

########################
# 	Barre de menus	#
#-----------------------#

# Creation du menu utilise comme barre de menu:
menu .menu -tearoff 0

# Creation du menu Fichier
menu .menu.fichier -tearoff 0
.menu add cascade -label [mc {Fichier}] -menu .menu.fichier
.menu.fichier add command -label [mc {edit_scena_ment_appr} ] -command {lanceappli editeurp.tcl ${rep_travail} }
.menu.fichier add command -label [ mc {edit_seq_ment_appr}] -command {lanceappli editeurseq.tcl  $rep_travail }
.menu.fichier add command -label [ mc {edit_division}] -command { lanceappli scenario_div.tcl $rep_travail }
 if {  $tcl_platform(platform) == "unix" } {
 menu .menu.fichier.bilan -tearoff 0
.menu.fichier add cascade -label [ mc {Bilans}]    -menu .menu.fichier.bilan
## -command "lanceappli bilan.tcl"
.menu.fichier.bilan add radio -label [mc {Bilan Calcul mental}] -variable bilan  -value m \
-command {lanceappli bilan_pluie.tcl $rep_travail }
if {  [lindex [exec  id -G -n ] 0 ]  != "eleves" } {
.menu.fichier.bilan add radio -label [mc {Bilan Calcul mental par classe}] -variable bilan  -value c \
-command {lanceappli bilan.tcl $rep_travail }
}
.menu.fichier.bilan add radio -label [mc {Bilan Calcul arbre}] -variable bilan   -value a \
-command {lanceappli bilan_a.tcl $rep_travail }
 }
if  { $tcl_platform(platform) == "windows" }  {
  menu .menu.fichier.bilan -tearoff 0
.menu.fichier add cascade -label [ mc {Bilans}]    -menu .menu.fichier.bilan
 .menu.fichier.bilan  add  radio  -label [mc {Bilan Calcul mental}]   -variable bilan -value m \
 -command { lanceappli bilan_pluie.tcl $rep_travail }
.menu.fichier.bilan add radio -label [mc {Bilan Calcul mental par classe}] -variable bilan  -value c \
-command {lanceappli bilan.tcl $rep_travail }
 }
#.menu.fichier add sep
#.menu.fichier add command -label [ mc {quit}] -command exit
#menu .menu.action -tearoff 0
#.menu add cascade -label [ mc {Activit�s}] -menu .menu.action
#.menu.action add command -label [ mc {Calcul_Mental}] -command { lanceappli choix.tcl $rep_travail }
#.menu.action add command -label [ mc {Calcul_Approch�} ] -command { lanceappli choix_app1.tcl $rep_travail }
#.menu.action add command -label [ mc {Calcul_Equation} ] -command { lanceappli choix_eq.tcl $rep_travail }
#.menu.action add command -label [ mc {Calcul_en_arbre} ] -command { lanceappli Calcul_en_arbre.tcl $rep_travail }
#.menu.action add command -label [ mc {A_nous_la_Division} ] -command { lanceappli choix_div.tcl $rep_travail }
#.menu.action add cascade -label [mc {Calculs_sur_bandes}] -menu .menu.action.bande
#menu .menu.action.bande   -tearoff 0
#.menu.action.bande add command -label [mc {Serpent} ] -command  {lanceappli serpent.tcl  $rep_travail}
#.menu.action.bande add command -label [mc {Cache_tampon} ] -command  {lanceappli  cache_tampon.tcl  $rep_travail}
#.menu.action.bande add command -label [mc {Piquet} ] -command  {lanceappli  piquet.tcl  $rep_travail}
menu .menu.option -tearoff 0
.menu add cascade -label [mc {Reglages}] -menu .menu.option
set ext .msg
##choix des r�pertoires travail
menu .menu.option.repert -tearoff 0 
.menu.option add cascade -label "[mc {R�pertoires}]" -menu .menu.option.repert
menu .menu.option.repert.ssmenu_perso -tearoff 0
.menu.option.repert add cascade -label "[mc {Personnel}]" -menu .menu.option.repert.ssmenu_perso
##.menu.option.repert.ssmenu_perso add cascade -label [mc {perso}] -menu .menu.option.repert.ssmenu_perso.rep
### -command "choix_repert [file join $Hom] personnel "
.menu.option.repert.ssmenu_perso  add radio -label "Personnel" -variable choix_rep -value [file join $Home data] \
-command  { choix_repert "[file join  $Home data ]"  personnel } 
##.menu.option.repert.ssmenu_perso invoke 
catch {
foreach i [lsort [glob -type d -dir [file join $Home data ] *  ]] {
.menu.option.repert.ssmenu_perso  add radio -label [file tail $i] -variable choix_rep -value $i \
-command { choix_repert  $choix_rep  personnel }
}
}
##.menu.option.repert.ssmenu_perso entryconfigure "Personnel" -state active
menu .menu.option.repert.ssmenu_commun -tearoff 0
.menu.option.repert add cascade -label "[mc {Commun}]"  -menu .menu.option.repert.ssmenu_commun
.menu.option.repert.ssmenu_commun  add radio -label "Commun" -variable choix_rep -value $DOSSIER_EXOS \
-command { choix_repert $DOSSIER_EXOS  commun }

catch {
foreach i [lsort [glob -type d -dir [file join $DOSSIER_EXOS  ] *  ]] {
.menu.option.repert.ssmenu_commun  add radio -label [file tail $i] -variable choix_rep -value $i \
-command   { choix_repert $choix_rep  commun }
}
}

.menu.option.repert add command  -label "[mc {Exemple}]" -command {choix_repert  \
 "$basedir"  exemple }
 choix_repert [file join  $Home data ]  personnel 
 
 #.menu.option.repert.ssmenu_perso entryconfigure "Personnel" -state active
menu .menu.option.lang -tearoff 0 
.menu.option add cascade -label "[mc {Langue}]" -menu .menu.option.lang
foreach i [glob [file join  $basedir msgs *$ext]] {
set langue [string map {.msg ""} [file tail $i]]
.menu.option.lang add radio -label $langue -variable langue -command "setlang $langue"
}
menu .menu.aide -tearoff 0
.menu add cascade -label [mc {Aide}] -menu .menu.aide
	 switch $tcl_platform(platform) {
		    "unix" {
	    .menu.aide add command -label [ mc {Aide} ]  -command "exec firefox [pwd]/aide/index.html  & "
			    }
		    "windows" {
	    .menu.aide add command -label [ mc {Aide} ]  -command "exec firefox ${basedir}/aide/index.html  & "
		    

		    }
			    }
.menu.aide add command -label [ mc {� propos}] -command "source apropos.tcl"
. configure -menu .menu

###showaide {}"
##  if {$plateforme == "windows"} {
##.menu add command -label [mc {Utilisateur}] -command "setwindowsusername"
###}

. configure -menu .menu
set normal "-background {} -relief flat"
################
# Image de fond #
#################
frame .gauche -background blue -height 400 -width 200
##pack .frame -expand true

pack .gauche -side left
set image [image create photo -file images/tux-mental.png]
button .gauche.labA3 -image $image -text [mc {Mental1}] -background blue -font {Helvetica 14 normal} -pady 6 -justify left -foreground black -height 128 -width 130
bind .gauche.labA3 <1> {lanceappli choix.tcl $rep_travail }
##lanceappli1 mental1
pack .gauche.labA3 -side top -anchor w

set image [image create photo -file images/tux-approche.png]
button .gauche.labA1 -image $image -text [mc {approche}] -background blue -font {Helvetica 14 normal} -pady 6 -justify left -foreground black -height 128 -width 130
pack .gauche.labA1 -side top -anchor w
bind .gauche.labA1 <1> "lanceappli1 approche"

set image [image create photo -file images/tux-equation.png]
button .gauche.labA2 -image $image -text [mc {equation}] -background blue -font {Helvetica 14 normal} -pady 6 -justify left -foreground black -height 128 -width 130
pack .gauche.labA2 -side top -anchor center
bind .gauche.labA2 <1> { lanceappli choix_eq.tcl $rep_travail }

frame .centre -background blue
pack .centre  -side left

###grid .frame.imagedisplayer -column 1 -row 0 -sticky e
set image [image create photo -file images/parcours1.gif]
button .centre.labA1 -image $image -text [mc {Parcours1}] -background blue -font {Helvetica 14 normal} -pady 6 -justify left -foreground black -height 128 -width 130
pack .centre.labA1 -side top -anchor center
bind .centre.labA1 <1> "lanceappli1 parcours1"
set myimage [image create photo -file images/background.png]
label .centre.imagedisplayer -image $myimage -background blue 
pack .centre.imagedisplayer  -side top -anchor nw
set image [image create photo -file images/parcours2.gif]
button .centre.labA2 -image $image -text [mc {Parcours2}] -background blue -font {Helvetica 14 normal} -pady 6 -justify left -foreground black -height 128 -width 130
pack .centre.labA2 -side top -anchor center
bind .centre.labA2 <1> "lanceappli1 parcours2"
frame .droite -background blue
pack .droite -side left
set image [image create photo -file images/tux-arbre.png]
button .droite.labA2 -image $image -text [mc {arbres}] -background blue -font {Helvetica 14 normal} -pady 6 -justify left -foreground black -height 128 -width 130
pack .droite.labA2 -side top -anchor center
bind .droite.labA2 <1> "lanceappli1 arbre"

set image [image create photo -file images/tux-quotient.png]
button .droite.labA3 -image $image -text [mc {quotient}] -background blue -font {Helvetica 14 normal} -pady 6 -justify left -foreground black -height 128 -width 130
pack .droite.labA3 -side top -anchor center
bind .droite.labA3 <1> { lanceappli choix_div.tcl $rep_travail }
set image [image create photo -file images/sortie2.png]
button .droite.labA4 -image $image -text [mc {equation}] -background blue -font {Helvetica 14 normal} -pady 6 -justify left -foreground black -height 128 -width 130
pack .droite.labA4 -side top -anchor center
bind .droite.labA4 <1> { exit }

frame .droite1 -background blue
pack .droite1 -side left
set image [image create photo -file images/piquets.png]
button .droite1.labA2 -image $image -text [mc {piquet}] -background blue -font {Helvetica 14 normal} -pady 6 -justify left -foreground black -height 128 -width 130
pack .droite1.labA2 -side top -anchor center
bind .droite1.labA2 <1> {lanceappli  piquet.tcl  $rep_travail }

set image [image create photo -file images/serpent.png]
button .droite1.labA3 -image $image -text [mc {serpent}] -background blue -font {Helvetica 14 normal} -pady 6 -justify left -foreground black -height 128 -width 130
pack .droite1.labA3 -side top -anchor center
bind .droite1.labA3 <1> {lanceappli serpent.tcl  $rep_travail }
set image [image create photo -file images/cache_tampon.png]
button .droite1.labA4 -image $image -text [mc {cache_tampon}] -background blue -font {Helvetica 14 normal} -pady 6 -justify left -foreground black -height 128 -width 130
pack .droite1.labA4 -side top -anchor center
bind .droite1.labA4 <1> {lanceappli  cache_tampon.tcl  $rep_travail }





}
proc lanceappli1 {type} {
global iwish rep_travail nom_elev nom_classe basedir
##set exo cf buutons lby pour seq refaire le choi!!
switch $type {
		"arbre" {
		exec  $iwish  [file join $basedir Calcul_en_arbre.tcl]
		}
		"mental1" {
	destroy .	
exec $iwish  [file join $basedir pluie_un.tcl] [file join $basedir add9.sce ] $nom_elev &nom_classe 1000 $basedir  h m	 
exec  $iwish  [file join $basedir pluie_un.tcl] [file join $basedir div3.sce ] $nom_elev &nom_classe 1000 $basedir k p 
exec  $iwish  [file join $basedir pluie_un.tcl] [file join $basedir essai_expr1.sce ] $nom_elev &nom_classe 1500 $basedir h p 
exec  $iwish  [file join $basedir pluie.tcl] [file join $basedir add11.sce ] $nom_elev &nom_classe 1000 $basedir				
exec  $iwish  [file join $basedir Calculs.tcl]				
				}
				
		"mental2" {
	tk_messageBox  -message "[mc {Calculs soustractifs ;. un peu , beaucoup,..}]"    -icon info  -type ok -title "Calculs. Soustraction"		
			set liste  " sous5.sce sous11.sce sous_bcp.sce sous_peu.sce "
			catch { destroy .}
			foreach el $liste {
			if { [file exists [file join $basedir $el ] ] } {
exec  $iwish  [file join $basedir pluie_un.tcl] [file join $basedir $el ] $nom_elev &nom_classe 1500 $basedir 
				}
				
				}
				exec  $iwish  [file join $basedir Calculs.tcl]
				}	
				
				
		"equation" {
exec  $iwish  [file join $basedir pluie_eq.tcl] [file join $basedir equa1.sce ] $nom_elev &nom_classe 1500 $rep_travail 
		}
		"approche" {
		
		lanceappli choix_app1.tcl $rep_travail
	
		}
		"quotient" {
		catch { destroy .}
		exec  $iwish  [file join $basedir  division.tcl ] fixe_ici1  $nom_elev &nom_classe   $rep_travail  scenarios_div.conf  
		exec  $iwish  [file join $basedir  division.tcl ] af_rien  $nom_elev &nom_classe   $rep_travail  scenarios_div.conf 
		exec  $iwish  [file join $basedir  division.tcl ] af_tout  $nom_elev &nom_classe   $rep_travail  scenarios_div.conf 
			exec  $iwish  [file join $basedir Calculs.tcl]
		}
		"piquet" { catch {destroy .}
	exec  $iwish	 [file join $basedir  piquet.tcl ] $rep_travail 1  
	exec  $iwish  [file join $basedir Calculs.tcl]
		}
		"serpent" {
	exec  $iwish	 [file join $basedir  serpent.tcl ] $rep_travail 1
		}
		"cache_tampon" {
			tk_messageBox  -message "[mc {Pour le  2i�me choisir trois �carts pour chaud ti�de froid}]"    -icon info  -type ok -title "Cache_tampon"	
		catch { destroy .}
	exec  $iwish	 [file join $basedir  cache_tampon.tcl ] $rep_travail 1  
	exec  $iwish	 [file join $basedir  cache_tampon.tcl ] $rep_travail 2 
	exec  $iwish  [file join $basedir Calculs.tcl]
		}
		"parcours1" {
	lanceappli choix_seq_ment.tcl $rep_travail
		}
	"parcours2" {
	lanceappli choix_seq_appr.tcl $rep_travail	
		}
		
		
		
		
		}

		}
############
# Bindings #
############
bind . <Control-q> {exit}

proc choix_repert { dossier type} {
global rep_travail DOSSIER_EXOS Home
if { $type == "exemple"  || $dossier == $DOSSIER_EXOS || \
$dossier == [file join $Home data ]}  {
set rep_travail $dossier
return
}
 catch  { set dir  [tk_chooseDirectory  -initialdir $dossier  -title "Choisir le r�pertoire de travail" ] ; \
  set rep_travail $dir  }
}


proc lanceappli {appli rep} {
    global iwish
    exec $iwish $appli $rep &
}
proc setlang {lang} {
global env plateforme
set env(LANG) $lang
::msgcat::mclocale $lang
::msgcat::mcload [file join [file dirname [info script]] msgs]
 catch {interface}
}
setlang fr
##interface

#modifier wish pour PC windaube






