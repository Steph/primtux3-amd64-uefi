#!/bin/sh
#editeurp.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jlsendral@free.fr
#  Modifier:
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     Jean- Louis Sendral jlsendral@free.fr
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *************************************************************************
source calculs.conf
source i18n
source path.tcl
###################"gestion du son
#"global sound
#"if {[catch {package require -exact snack 2.1}]} {
#"    set sound 0
#"} else {
#"    set sound 1
#"}
set bgcolor orange
. configure  -height 440 -width 660 -background  $bgcolor
##wm resizable . 0 0
global sysFont     DOSSIER_EXOS1 test_elev
source msg.tcl
source fonts.tcl
set basedir [pwd]
##cd $basedir
set  DOSSIER_EXOS1 [lindex $argv 0 ]
set test_elev 1
if { $tcl_platform(platform) == "unix" && [lindex [exec  id -G -n ] 0 ]  == "eleves" } {
if { [string match  "${basedir}*"  [lindex $argv 0 ] ] == 1 } {
set test_elev 0
} 
}
set flag_gener 0
########## Construction des widgets de l'interface###########
## set basedir [pwd]
 ##cd $DOSSIER_EXOS
#frame1 pour lister les sc�narios, ajouter des noms de sc�narios qu' il reste � construire, supprimer
#supprimer des sc�narios. des sc�narios
wm geometry . +0+0
wm title . "[mc {Editeur de Sc�nario}] pour expressions, �quations, approx et pluie."
frame .frame1 -background  orange -height 450 -width 320
grid .frame1 -column 0 -row 0
listbox .frame1.list1 -background #c0c0c0 -height 13 -width 35 -yscrollcommand ".frame1.scroll1 set"
scrollbar .frame1.scroll1 -command ".frame1.list1 yview"
place .frame1.list1 -x 5 -y 50
place .frame1.scroll1 -x 230 -y 50 -height 220
#172
label .frame1.menu3 -text [mc {Double_clic_scena}]  -background  $bgcolor
place .frame1.menu3 -x 0 -y 20
label .frame1.categorie -text [mc {Sc�narios}] -background  $bgcolor
place .frame1.categorie -x 95 -y 5
label .frame1.menu1 -text [mc {ajout_supp_scena}] -background  $bgcolor
place .frame1.menu1 -x 15 -y 280
button .frame1.ajouter -text [mc {Ajouter}]
place .frame1.ajouter -x 140 -y 325
button .frame1.supprimer -text [mc {Supprimer} ]
place .frame1.supprimer -x 60 -y 325
entry .frame1.text1 -width 40
place .frame1.text1 -x 5 -y 300
set message "[mc {dabord_pluie}] :\n1\) [mc {tap_nom_sce}]\n2) [mc {clic_ajsup}]\n3) [mc {double_clic_nom}]\n4)[mc {Puis1}]"
message  .frame1.consigne -text $message -foreground red  -aspect 350
place .frame1.consigne -x 10 -y 355
#frame2 pour �diter des sc�narios existant, donc les modifier, ou � construire.
frame .frame2 -background  $bgcolor -height 450 -width 360
grid .frame2 -column 1 -row 0
label .frame2.oper -text [mc {Op�ration}] -background  $bgcolor
place .frame2.oper -x 0 -y 40
entry .frame2.op -width 1
place .frame2.op -x 70 -y 40
    
    label .frame2.lig -text [mc {n_lignes}] -background  $bgcolor
    place .frame2.lig -x 95 -y 40
    entry .frame2.nlig -width 2
    place .frame2.nlig -x 150 -y 40
    label .frame2.col -text [mc {n_colonnes}] -background  $bgcolor 
    place .frame2.col -x 185 -y 40
    entry .frame2.ncol -width 2
    place .frame2.ncol -x 260 -y 40
    label .frame2.coupl -text [mc {couples}]  -background  $bgcolor
    place .frame2.coupl -x 0 -y 70
    entry .frame2.couples -width 50 -xscrollcommand  ".frame2.scroll2 set"
    scrollbar .frame2.scroll2 -command ".frame2.couples xview" -orient horizontal 
    place .frame2.couples -x 0 -y 90
    place .frame2.scroll2 -x 0 -y 113 -width 355 -anchor nw
        label .frame2.scenario -text "[mc {parametres}] [.frame1.list1 get active] :" -background  $bgcolor
        place .frame2.scenario -x 100 -y 10
	  button .frame2.aide -text [mc {Aide}]
	  place .frame2.aide -x 280 -y 380
##	 cd  $basedir
set inp ""
	 bind .frame2.aide <1>  "exec $progaide file:${basedir}/aide1.html#cmental & "

	 ###   " exec $progaide ${basedir}/aide/index.html"
          ## "showaide  cmental"
		  button .frame2.genere_liste -text [mc {generer_liste}]
        place .frame2.genere_liste -x 110 -y 230 
##		  .frame2.genere_liste configure -state disabled 
	proc si_genere {} {
	global choix_gener inp
	if {  $inp  != "" } { .frame2.couples delete 0 end ; .frame2.couples insert 0 $inp}
	if {$choix_gener == "o"} {.frame2.genere_liste configure -state active } else {
	.frame2.genere_liste configure -state disabled}
	}		  
		 
		checkbutton .frame2.gener -variable choix_gener -onvalue o -offvalue n -text "h" \
		-background  $bgcolor  -command "si_genere"
		switch $tcl_platform(platform) {
    windows {
		place .frame2.gener -x 280 -y 135 }
	default {
		place .frame2.gener -x 310 -y 160
	}
	}
		set choix_gener n ; si_genere
        button .frame2.ajoutsc -text [mc {sauver_maj_scena}] -font {helvetica  9 bold}
        place .frame2.ajoutsc -x 20 -y 310
		
	label .frame2.hasard -text "[mc {choix_hasard_nombre}] :" -background  $bgcolor 
        place .frame2.hasard -x 0 -y 140
label .frame2.choix_max -text [mc {n_max}] -background  $bgcolor
place .frame2.choix_max -x 0 -y 165
entry .frame2.n_max -width 4
place .frame2.n_max -x 170 -y 165

label .frame2.choix_min -text [mc {n_min}] -background  $bgcolor
place .frame2.choix_min -x 0 -y 185
entry .frame2.n_min -width 4
place .frame2.n_min -x 170 -y 185

label .frame2.choix_n_couples -text [mc {n_couples}] -background  $bgcolor
place .frame2.choix_n_couples -x 0 -y 205
entry .frame2.n_couples -width 4
place .frame2.n_couples -x 170 -y 205
set message1 "[mc {Puis}]\n[mc {scena_choisi}]\n5) [mc {remplir_champ}]\n6) [mc {clic_sau_maj}]"
message  .frame2.consigne -text $message1 -foreground red    -aspect 300
place .frame2.consigne -x 0 -y 355
 bind .frame2.ajoutsc <Button-1> "ajoutescenario"
bind .frame1.ajouter <Button-1> "nouveauscenario"
bind .frame1.supprimer <Button-1> "supprimescenario"
bind .frame1.list1 <ButtonRelease-1> "capturelist1"
.frame2.ajoutsc configure -state disabled
bind .frame2.genere_liste <Button-1> "generer_liste ; set flag_gener 0"
# les fichiers sc�narios ont pour extension .sce
    set scenario ""
    set ext .sce
    set currentlist ""
# lister les sc�narios exitant
#test nbe lignes 18 si incoh�rence
proc teste_li { input} {
if  {[ regexp {(^[0-9]+$)} $input tout lig ] == 1 } {
			if { $input < 19 } {
			return $input
				} else {
			return 18
				}
            } else {
return 12
}
}
proc teste_co { input} {
if  {[ regexp {(^[0-9]+$)} $input tout  col] == 1 } {
       if { $input < 10 } {
            return $input
         } else {
         return 10
         }   
    } else {

return 2
}
}
proc teste_op { input} {
if  { [ regexp {^[x|\-|\+|\:]$} $input ] } {
            return $input
    } else {
return "+"
}
}
proc change_oper {expr} {
set e1 [regsub -all  {:}  $expr "\/" var]
set e2 [regsub -all  {x} $var "\*" var1 ]
set e3 [regsub -all  {\,} $var1 "\." var2 ]
return $var2
}


proc teste_cou {liste oper} {
##enlever ; de$liste  puis les remettre
set li $liste
if { [expr [llength $li] % 2 ]  != 0 } {
set li [linsert  $li end  1] 
}
##il peut rester de ptsvirgules coll�s
##.frame1.consigne  configure -text "etetet $li "

###tester la forme (4+5)+(1+2) avec virgule
foreach el $li {
if {![regexp {^([0-9]*\,?[0-9]*[\+|\-|x|:]{0,1}\d\,?[0-9]*)*[+|\-|x|:]{0,1}(\d\,?[0-9]*[+|\-|x|:]{0,1}\d\,?[0-9]*)*$} $el ]  } {
set indice [lsearch -exact $li $el]
set li [lreplace $li $indice $indice 2]
##^([0-9]*.?[0-9]*[\+|\-|\*|\/]{0,1}\d.?[0-9]*)*[+|\-|*|\/]{0,1}(\d.?[0-9]*[+|\-|*|\/]{0,1}\d.?[0-9]*)*$
}
}
##cela devrait suffire !!!!!
foreach el $li {
if { [ regexp {\,{2,}} $el ] == 1 || [regexp {[\+|\-|x|\:|\,]{2,}} $el ] == 1 || [regexp {[\+|\-|x|\:]{1}$} $el ] == 1 } {
set indice [lsearch -exact $li $el]
set li [lreplace $li $indice $indice 3]
}
}
foreach el $li {
if { [regexp {[0-9]*\\,[0-9]+\\,[0-9]*} $el ] || [regexp {^\,\,*$} $el ] == 1 } {
set indice [lsearch -exact $li $el]
set li [lreplace $li $indice $indice 4]
}
}
if { $oper == ":" } {
foreach el $li {
for {set  i 1 } {$i  <  [ llength $li ]  } { incr i 2}  {
if  { [ lindex $li $i ] == 0 } { set li [lreplace $li $i $i 1 ] }
}
for {set  i 0 } {$i  <  [expr [ llength $li ] -1]  } { incr i 2}  {
if  { [expr [expr [change_oper [ lindex $li $i ]]] % [expr [ change_oper [ lindex $li [expr $i +1] ]]] ] != 0 } {
set li [lreplace $li [expr $i +1]  [expr $i +1] 1]
}
}
}
}
##pour la soustraction ?? et division ??
if { $oper == "-" } {
set liste_p $li

for {set  i 0 } {$i  < [expr [ llength $li ] - 1] } { incr i 2}  {
if  { [expr [change_oper [ lindex $li $i ]]]  < [expr [ change_oper [ lindex $li [expr $i +1]]] ]} { set liste_p [lreplace $liste_p $i [expr $i + 1] [ lindex $li [expr $i +1]] [ lindex $li $i ] ] }
}
set li $liste_p
}




return $li
}

proc peuplelist1 {} {
       global DOSSIER_EXOS  basedir choix_lieu DOSSIER_EXOS1  argv  env Home test_elev choix_gener
.frame1.supprimer configure  -state  normal
 .frame1.ajouter configure  -state  normal
 .frame2.ajoutsc configure  -state  normal
 ##set choix_gener "n"
 set DOSSIER_EXOS1  [lindex $argv 0 ]
   set types {
    {"Cat�gories"		{.sce}	}
}
 cd $DOSSIER_EXOS1
  if { $DOSSIER_EXOS1 == $basedir  || $test_elev == 0 } {
 .frame1.supprimer configure  -state  disabled
 .frame1.ajouter configure  -state  disabled
 .frame2.ajoutsc configure  -state  disabled
}
              .frame1.list1 delete 0 end
        catch {foreach i [lsort [glob [file join *.sce]  ]] {
                .frame1.list1 insert end [file rootname [file tail $i]]
            }
        }
cd  $basedir 
 }
peuplelist1
# pour ajouter des noms de sc�narios et en  mettre � jour la liste##############
proc nouveauscenario {} {
    global ext DOSSIER_EXOS1
    set scena [.frame1.text1 get]
    if {[catch {set f [open [file join $DOSSIER_EXOS1 ${scena}$ext] "w"]}]} {
        bell
        return
    } else {
        close $f
cd $DOSSIER_EXOS1
         .frame1.list1 delete 0 end
        catch {foreach i [lsort [glob [file join  *.sce]  ]] {
                .frame1.list1 insert end [file rootname [file tail $i]]
            }
        }

###        peuplelist1

        set scena ""
        .frame1.text1 delete 0 end
    }
}
#pour supprimer un sc�nario et en mettre � jour la liste######

proc supprimescenario {} {
    global currentlist1 ext DOSSIER_EXOS1
    set scena [.frame1.text1 get]
    if {$scena==""} {
        bell
        return
    }

    catch {[file delete  [file join $DOSSIER_EXOS1 "${scena}.sce"]]}
cd $DOSSIER_EXOS1
        .frame1.list1 delete 0 end
        catch {foreach i [lsort [glob [ file join  *.sce]  ]] {
                .frame1.list1 insert end [file rootname [file tail $i]]
            }
        }
##    peuplelist1 catch {[file delete [file join $scena$ext]]}
   .frame1.text1 delete 0 end
##pour d�bug .frame1.text1 insert 0 [file join  $DOSSIER_EXOS1 "${scena}.sce" ]
}

#pour sauver un sc�nario mis � jour ou construit ou generer liste sans sauver#################

proc fais_liste { oper ajout ou n_fois max min } {
set liste {}
##v�rifier nature et liaison max min ajout

if  {[ regexp {(^[0-9]+$)} $max tout  col] != 1 } {
set max 100 ; .frame2.n_max delete 0 end ; .frame2.n_max insert 0 100
}
if  {[ regexp {(^[0-9]+$)} $min tout  col] != 1 } {
set min 0 ; .frame2.n_min delete 0 end ; .frame2.n_min insert 0 0
}
if  {[ regexp {(^[0-9]+$)} $n_fois tout  col] != 1 } {
set n_fois 3 ; .frame2.n_couples delete 0 end ; .frame2.n_couples insert 0 $n_fois
}

if {  $min > $max } {
set min1 $min ; set min $max ; set max $min1
.frame2.n_max delete 0 end ; .frame2.n_max insert 0 $max 
.frame2.n_min delete 0 end ;.frame2.n_min insert 0 $min
}
if {  $min == $max } {
set max 100 ; set min 0
.frame2.n_max delete 0 end ; .frame2.n_max insert 0 $max 
.frame2.n_min delete 0 end ;.frame2.n_min insert 0 $min
}

if { $oper == ":" } {
##pas de boucle infini!!! que pour h:2
## $max-$min>ajout et ajout <min pour h3 
if { $ou == "derriere" } {return []}
if {[expr $max-$min] <= $ajout || $ajout >= $min } {
return }
}
switch --  $oper {
		"+" {
		for {set i 1} {$i <= $n_fois }  { incr i} {
			if { $ou == "devant" } {
			lappend liste [expr $min + int([expr $max-$min]*(rand()))+ 1]  $ajout
			} else {
			lappend liste $ajout [expr $min + int([expr $max-$min]*(rand()))+ 1]
			}
			}
##			return [mettre_virg $liste ]
			}
		 "-" {
		     for {set i 1} {$i <= $n_fois } { incr i} {
			 
			if { $ou == "derriere" } {
					if { $ajout > $max} { 
								lappend liste $ajout [expr $min + int([expr $max - $min]*(rand()))+ 1]
										} elseif { $ajout >= $min } {
										lappend liste $ajout [expr $min + int([expr $ajout - $min]*(rand()))+ 1]
										} else {return}
			}
			
			if { $ou == "devant" } {
					if { $ajout <= $min} { 
								lappend liste  [expr $min + int([expr $max - $min]*(rand()))+ 1] $ajout
										} elseif { $ajout <= $max } {
										lappend liste [expr $ajout + int([expr $max - $ajout]*(rand()))+ 1] $ajout
										} else {return}
			}
			
						
			}
##			return [mettre_virg $liste ]
			}
	   "x" {
		for {set i 1} {$i <= $n_fois }  { incr i} {
			if { $ou == "devant" } {
			lappend liste  [expr $min + int([expr $max-$min]*(rand()))+ 1] $ajout
			} else {
			lappend liste $ajout [expr $min + int([expr $max-$min]*(rand()))+ 1]
			}
			}
##			return [mettre_virg $liste]
			}

		":" {
			for {set i 1} {$i <= $n_fois } { incr i} {
			if { $ou == "devant" } {
		set liste	[concat $liste [ divise [expr $min + int([expr $max-$min]*(rand()))+ 1] $ajout $max $min "devant" ]]
# liste  [expr int($ajout*(rand()))+ 1] $ajout
			} else {
		set liste [concat $liste [ divise $ajout [expr $min + int([expr $max-$min]*(rand()))+ 1]  $max $min "derriere" ]]
#liste $ajout [expr int($max*(rand()))+ $ajout ]
			}
			}
##			return [mettre_virg $liste ]
			}

}
return [mettre_virg [enleve_repet $liste ]]
}
proc divise {n1 n2 nmax nmin ou } {
##pb 12,0
if { [ expr $n1 % $n2 ] == 0 } {
                 return [list $n1 $n2]
              }
 if { $ou == "devant" } {
return [ divise [expr $nmin + int([expr $nmax-$nmin]*(rand()))+ 1] $n2 $nmax $nmin "devant" ]
	} else {
return [ divise $n1 [expr $nmin + int([expr $nmax-$nmin]*(rand()))+ 1]  $nmax $nmin "derriere" ]

}
}
proc sans_virg1 {list} {
set l_p $list
foreach el $list {
if {[regexp {^([\-]+)([0-9]+\,?[0-9]*){1}$} $el tout prem deux ] == 1  } {
set indice [lsearch -exact $l_p $el]
set l_p [lreplace $l_p $indice $indice $prem $deux]
}
}
set list $l_p
##.frame1.consigne  configure -text "etp $list "
##.frame2.consigne configure -text "[regexp {^\;{1}[0-9]+\,?[0-9]*$} "45;" ]"
set l_p $list
foreach el $list {
 if {[regexp {^([0-9]+\,?[0-9]*){1}([\-]+)$} $el tout var1 var2 ]== 1 } {
set indice [lsearch -exact $l_p $el]
##set n [regsub -all {;} $el  "$var1 \;" recup]
set l_p [lreplace  $l_p $indice $indice $var1 $var2 ] 
}
}
set list $l_p
##.frame2.consigne configure -text " no $list" 
### [regexp {^([0-9]+\,?[0-9]*){1}(\-{1})$} 45\; tout var1 var2 ] ou
##return $list
return [sans_virg $list ]
}
proc sans_virg {list} {
##liste bien faite pb infini
if {$list == "" } {return ""}
if { [expr [llength $list]] < 2} {return $list } else {
 return [ concat [lindex $list 0]  [lindex $list 1] [sans_virg [lrange $list 3 end] ]]
}
}
proc mettre_virg {list} {
##la liste est bonne
##set long 
if { [expr [llength $list]] <  2} {return ""}
if { [expr [llength $list]] == 2} {return $list } else {
return [concat [concat [lindex $list 0]  [lindex $list 1]  " - "]  [ mettre_virg [lrange $list 2 end ]] ]

}
}


proc dans {el1 el2 list} {
global message

	if {$list == [] } { return 0}
	if { $el1 == [lindex  $list 0] && $el2 == [lindex $list 1 ] } { 
##.frame1.consigne configure -text "$el1 $el2  et $list f"
#	.frame2.n_max insert 0 $el1
	return 1 } else {
##	.frame1.consigne configure -text "$el1 $el2  et $list g"
##.frame2.consigne configure -text "e"
	return [dans $el1 $el2 [lrange $list 2 end]]
	}
}

proc enleve_repet {list} {
set liste_p []
if { [llength $list ] == 2 } {
		return $list } else {
		for {set i 0} {$i <= [expr [llength $list] -2]} { incr i 2} {
		if {[expr [dans  [lindex $list $i] [lindex $list [expr $i +1]]  $liste_p] == 1]} {
##		.frame2.consigne configure -text "$i et f et $liste_p"
		set liste_p $liste_p 
##		.frame1.text1 insert 0 $liste_p
		} else {
##		.frame2.consigne configure -text "$i et r et $liste_p"
		set liste_p [concat $liste_p [list [lindex $list $i] [lindex $list [expr $i+1]]] ]
##		.frame1.text1 insert 0 $liste_p
		}
		
		}
		return $liste_p
}
}
proc ajoutescenario {} {
global ext  scenario DOSSIER_EXOS1 flag_gener choix_gener data_gener ;# obtenu ds capturelist1
    if {[catch {set f [open [file join $DOSSIER_EXOS1 $scenario$ext] "w"]}]} {
        bell
        return
    } else {
     set inp [lindex [.frame2.couples get ] 0]
set oper [teste_op [.frame2.op get]] ; set n_lig [teste_li [.frame2.nlig get]]
set n_col [teste_co [.frame2.ncol get]]
.frame2.op delete 0 end ; .frame2.op insert 0 $oper
.frame2.nlig delete 0 end ; .frame2.nlig insert 0 $n_lig
.frame2.ncol delete 0 end ; .frame2.ncol insert 0 $n_col
## si inp vide return close
    if { [regexp {^([0-9]+h|h[0-9]+)$} $inp ] != 1 } {
###set oper [teste_op [.frame2.op get]] ; set n_lig [teste_li [.frame2.nlig get]]
####set n_col [teste_co [.frame2.nlig get]]
##	if {[llength $inp ] != 2} {return}
##cas fg ghj
##if { [regexp {^([a-z]*)$} $inp ] == 1 }  .frame2.couples delete 0 end ; return
##	.frame2.genere_liste configure -state disabled

		
##remplacer les ; par  espace dans couples regsub /; espace les garder pour insert ?
 set couples  [teste_cou [sans_virg1 [.frame2.couples get]] $oper ]
##debug
 ##.frame2.consigne  configure -text "[lreplace "0 2 3 4"  1 1 - 5] ol[.frame2.couples get] ey  [sans_virg1  "4 ;5; 6 7 8 9"]"
##les remettre pour affichage[lreplace $l_p $indice $indice \; $el]
  .frame2.couples  delete 0 end ; .frame2.couples insert 0  [ mettre_virg $couples ]

  set data [list $oper $couples $n_lig $n_col]
#### [.frame2.couples get] [.frame2.nlig get] [.frame2.ncol get] ]
	} else {close $f ; return}
}
##if { $choix_gener == "n" || $flag_gener == 0} 
##.frame2.genere_liste configure -state active
###pour debug
##.frame2.couples insert 0 $scenario$ext
if {$scenario == "" } { close $f ; return} else {
puts $f $data
close $f 
}
set currentlist1 ""
 #       set scenario ""
        .frame1.text1 delete 0 end
}
proc generer_liste {} {
    global ext  scenario DOSSIER_EXOS1 flag_gener choix_gener inp ;# obtenu ds capturelist1 data_gener
    
	set flag_gener 1
     set inp [lindex [.frame2.couples get ] 0]
	 if {$inp == "" } { return}
set oper [teste_op [.frame2.op get]] ; set n_lig [teste_li [.frame2.nlig get]]
set n_col [teste_co [.frame2.ncol get]]
.frame2.op delete 0 end ; .frame2.op insert 0 $oper
.frame2.nlig delete 0 end ; .frame2.nlig insert 0 $n_lig
.frame2.ncol delete 0 end ; .frame2.ncol insert 0 $n_col

    if { [regexp {^([0-9]+h|h[0-9]+)$} $inp ] != 1 } {
##		.frame2.couples  delete 0 end 
##	set inp "h3" ; .frame2.couples insert 0 "h3"
##		.frame2.couples insert 0 $couples
##		close $f 
		return
##cas fg ghj
##	.frame2.genere_liste configure -state disabled
##remplacer les ; par  espace dans couples regsub /; espace les garder pour insert ?
 #### [.frame2.couples get] [.frame2.nlig get] [.frame2.ncol get] ]
	} 
##	if { $choix_gener == "n" } {return}
 	if { [string index $inp 0] == "h" } {
     set ou "devant" ; set ajout [string range $inp 1 [ expr [string length $inp] - 1] ]
	 }
	 if { [string index $inp end] == "h" } {
	    set ou "derriere" ; set ajout [string range $inp 0 [ expr [string length $inp]-2 ] ]
	 if {  $oper == ":"  } { .frame2.couples delete 0 end ; return}
	 }
	 
 #set ou "devant"
## sans /; pour data avec pour insert
##if { [expr [string compare $oper  ":"] == 0] && [expr [string compare $ou "derriere"] == 0] } {return}
set data_gener [list [.frame2.op get] [fais_liste [.frame2.op get] $ajout $ou [.frame2.n_couples get ] [.frame2.n_max get] [.frame2.n_min get]] [.frame2.nlig get] [.frame2.ncol get] ]
##mettre les ;
.frame2.couples delete 0 end ; .frame2.couples insert 0  [lindex $data_gener 1]
.frame2.genere_liste configure -state disabled ; set choix_gener "n"
##set data_gener $data

##.frame2.op delete 0 end ; .frame2.op insert 0 [lindex $data 0]
##.frame2.nlig delete 0 end ; .frame2.nlig insert 0 [lindex $data 2]
##.frame2.ncol delete 0 end ; .frame2.ncol insert 0 [lindex $data 3]

        set currentlist1 ""
##        set scenario ""
        .frame1.text1 delete 0 end
    
}

#pour construire ou mettre � jour un sc�nario #################

proc capturelist1 {} {
    global currentlist1 ext scenario DOSSIER_EXOS1 choix_gener
    set scenario [.frame1.list1 get active]
	.frame2.ajoutsc configure -state active
   set choix_gener n ; .frame2.genere_liste configure -state disabled
catch {
    set f [open [file join $DOSSIER_EXOS1 $scenario$ext] "r"]
    set listdata [gets $f]
    close $f
#    set total [llength $listdata]
}
.frame2.op delete  0 end
.frame2.nlig delete 0 end
.frame2.ncol delete 0 end
.frame2.couples delete 0 end
if  { $listdata != "" } {
 ;#sc�nario existant
set oper [lindex  $listdata 0]
set listcouples [lindex $listdata 1]
set n_col [lindex $listdata 3]
set n_lig [lindex $listdata 2]
.frame2.op insert 0 $oper
.frame2.nlig insert 0 $n_lig
.frame2.ncol insert 0 $n_col
##mettre ;
.frame2.couples insert 0 [mettre_virg $listcouples]
.frame2.scenario configure -text  "[mc {parametres}] [.frame1.list1 get active] :"
.frame2.ajoutsc configure -text "[mc {ajouter_maj_scena}] $scenario"
} else  {
     .frame2.scenario configure -text  "[mc {parametres}] [.frame1.list1 get active] :"
    .frame2.ajoutsc configure -text "[mc {ajouter_maj_scena}] $scenario"
}
}










