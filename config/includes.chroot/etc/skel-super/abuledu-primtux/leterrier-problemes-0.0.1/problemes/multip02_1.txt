set etapes 1
set niveaux {0 1 2 3 4 5}
::1
set niveau 2
set ope {{2 6}}
set interope {{1 10 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set volatil 0
set operations [list [list [expr $ope1 ]+[expr $ope1]+[expr $ope1 ]] [list [expr $ope1]*3] [list 3*[expr $ope1]]]
set enonce "Les champignons.\nTim a cueilli des champignons, il les a mis dans 3 paniers.\nDans chaque panier, il y a $ope1 champignons.\nCombien a-t-il en tout de champignons?"
set cible {{4 2 {} source0} {4 2 {} source0} {4 2 {} source0}}
set intervalcible 60
set taillerect 45
set orgy 40
set orgxorig 50
set orgsourcey 100
set orgsourcexorig 600
set source {champignon.gif}
set orient 0
set labelcible {{Panier 1} {Panier 2} {Panier 3}}
set quadri 0
set reponse [list [list {1} [list {Il y a} [expr $ope1*3] {champignons en tout.}]]]
set ensembles [list [expr $ope1] [expr $ope1] [expr $ope1]]
set canvash 300
set c1height 160
set opnonautorise {0}
::
