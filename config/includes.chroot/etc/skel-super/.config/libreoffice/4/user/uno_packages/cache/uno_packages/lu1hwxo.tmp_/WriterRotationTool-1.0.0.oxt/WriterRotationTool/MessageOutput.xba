<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE script:module PUBLIC "-//OpenOffice.org//DTD OfficeDocument 1.0//EN" "module.dtd">
<script:module xmlns:script="http://openoffice.org/2000/script" script:name="MessageOutput" script:language="StarBasic">REM  *****  BASIC  *****

&apos;**************************************************
&apos;AUTHOR		: T. VATAIRE
&apos;DATE		: 12/10/09
&apos;VERSION	: 0.1.0
&apos;**************************************************
&apos;This module contains generic functions relating on message log and display.
&apos;It allows to display a message in a message box, or to log it through the logger module.
&apos;The display of a message is deferred when setMessage is called. To display the message,
&apos;showMessage must be called thereafter.
&apos;If an error message is already defined when setMessage is called,
&apos;this one is instantly displayed, but warning and debug messages are replaced.
&apos;**************************************************
&apos;Dependencies : Logger, Message
&apos;**************************************************
&apos;licence : LGPL v3.0
&apos;**************************************************

option explicit


&apos;-------------------------------------
&apos;Private members
&apos;-------------------------------------

&apos;the next message to be displayed when the function &quot;showMessage&quot; is called
&apos;this allow to display the message asynchronously
private m_nextMessage as object


&apos;-------------------------------------
&apos;Private functions
&apos;-------------------------------------

function _messageOutput_isNextAlreadyDefined() as boolean

	_messageOutput_isNextAlreadyDefined() = message_isValid(m_nextMessage)
	
end function


&apos;-------------------------------------
&apos;Public functions
&apos;-------------------------------------

&apos;defines the next message to display
&apos;param message	: object	: the next message to display.
sub messageOutput_defineNext(message as object)
		
	if (message_isValid(message)) then
		&apos;if a message is already defined, it is displayed but only if this is an error message. Warning and info messages are ignored.
		if (_messageOutput_isNextAlreadyDefined()) then
			if (m_nextMessage.messageType = INT_MESSAGE_TYPE_ERROR) then
				messageOutput_showNext()
			end if
		end if
		m_nextMessage = message
	else
		messageOutput_showNext(message_new(&quot;messageOutput_defineNext : Invalid parameter : not a message object.&quot;, INT_MESSAGE_TYPE_ERROR))
	end if
	
end sub

&apos;displays the message previously defined with &apos;messageOutput_defineNext&apos; function or a message received as parameter.
&apos;if the message is received as parameter but that &apos;m_stuct_Message&apos; is already defined, it is first displayed.
&apos;param message (optionnal)	: object	: a message to display. if missing, &apos;m_stuct_Message&apos; is displayed instead.
sub messageOutput_showNext(optional message as object)

	if (not isMissing(message)) then
		messageOutput_defineNext(message)
	end if
	&apos;could happen if no parameter is received, and if &apos;m_stuct_Message&apos; is not yet defined
	if (not _messageOutput_isNextAlreadyDefined()) then
		m_nextMessage = message_new(&quot;messageOutput_showNext : No message to display, this should never happen!&quot;, INT_MESSAGE_TYPE_WARNING)
		messageOutput_log(m_nextMessage)
	end if
	msgbox(message_formatToString(m_nextMessage, string(2, chr(13))), m_nextMessage.messageType)
	m_nextMessage = createUnoStruct(&quot;&quot;)
	&apos;reset system error informations
	on local error resume next

end sub

&apos;log a message received as parameter
&apos;param message	: object	: the message to log.
sub messageOutput_log(message as object)

	dim messageToLog as object
	dim contentToLog as string
	
	messageToLog = message
	if (not message_isValid(messageToLog)) then
		messageToLog = message_new(&quot;messageOutput_log : Invalid &apos;message&apos; parameter. Unable to log initial message.&quot;, INT_MESSAGE_TYPE_ERROR)
	end if
	contentToLog = message_formatToString(messageToLog, chr(13))
	select case messageToLog.messageType
		case INT_MESSAGE_TYPE_ERROR
			logError(contentToLog)
		case INT_MESSAGE_TYPE_WARNING
			logWarning(contentToLog)
		case INT_MESSAGE_TYPE_INFO
			logDebug(contentToLog)
		case else
			&apos;level none or other : nothing to do
	end select

end sub

</script:module>