var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.mistral = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre=[1,2,3];
var sachet,champReponse,soluce,aGroupe,aTotalBonbon,aPrixQuestion,aBonbonQuestion,aPrix,conteneurReponse;

// Référencer les ressources de l'exercice (textes, image, son)
// exo.oRessources peut être soit un objet soit une fonction qui renvoie un objet

exo.oRessources = { 
    txt         : "mistral/textes/mistral_fr.json",
    Orangettes  : "mistral/images/boite-orangette.png",
    Colas       : "mistral/images/boite-cola.png",
    Fraises     : "mistral/images/boite-fraise.png",
    sachet      : "mistral/images/sachet.png",
    fraise      : "mistral/images/fraise.png",
    orange      : "mistral/images/orange.png",
    cola        : "mistral/images/cola.png",
    illustration: "mistral/images/illustration.png"
}

// Définir les options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:5,
        totalEssai:1,
        tempsExo:0,
        tempsQuestion:0,
        vitesse:75,
        totalBoite:3,
        intervalePrix:"2-9"
    }
    $.extend(exo.options,optionsParDefaut,oOptions);
}

// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
    aPrixQuestion = [];
    aBonbonQuestion = [];
    var nBoite = exo.options.totalBoite;
    var aPrix = util.getArrayNombre(exo.options.intervalePrix);
    if (aPrix.length >= nBoite) {
       for(var i=0;i<exo.options.totalQuestion;i++){
            aPrixQuestion[i] = util.shuffleArray(util.getArrayNombre(exo.options.intervalePrix));
            var aTemp=[];
            var total = 0;
            for(var j=0; j<nBoite; j++){
                aTemp[j] = 2 + Math.floor(Math.random()*(5-2+1));
                total+=aTemp[j]*aPrixQuestion[i][j];
            }
            if (total<100) {
                aBonbonQuestion.push(aTemp);
            }
            else {
                i--;
            }
            
        }
    }
}

//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    exo.blocIllustration.html(disp.createImageSprite(exo,"illustration"))
}

//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    // config du clavier
    exo.keyboard.config({
        arrow : "disabled",
        numeric : "disabled",
        large : "disabled"
    });
    // les disributeurs
    var aNomBoite = ["Orangettes","Fraises","Colas"];
    var aBoite = [];
    var nBoite = exo.options.totalBoite;
    aPrix=aPrixQuestion[exo.indiceQuestion]
    console.log("exo.indiceQuestion",exo.indiceQuestion);
    for(var i = 0; i < nBoite; i++){
        aBoite[i] = disp.createImageSprite(exo,aNomBoite[i]);
        exo.blocAnimation.append(aBoite[i]);
        posX = (735 - (nBoite*(aBoite[i].width())+20*(nBoite-1)))/2 + (aBoite[i].width()+20)*i;
        aBoite[i].css({left:posX,top:30});
        var label = disp.createTextLabel(aNomBoite[i]+"<br>"+aPrix[i]+" cts pièce");
        label.css({textAlign:"center",fontSize:16,width:100,fontWeight:"bold",lineHeight:1});
        label.css({left:(aBoite[i].width()-label.width())/2,top:80})
        aBoite[i].append(label);
    }
    
    // les bonbons
    var aNomBonbon = ["orange","fraise","cola"];
    aTotalBonbon = aBonbonQuestion[exo.indiceQuestion];
    aGroupe=[];
    soluce = 0;
    for(var i=0;i<nBoite; i++){
        soluce+=aPrix[i]*aTotalBonbon[i];
    }
    
    for(var i=0; i < nBoite; i++){
        var groupe = creerBonbon(aNomBonbon[i],aTotalBonbon[i]);
        exo.blocAnimation.append(groupe);
        groupe.css({
            left:aBoite[i].position().left+(aBoite[i].width()-groupe.width())/2,
            top:aBoite[i].position().top+(aBoite[i].height()-groupe.height())/2
        });
        exo.blocAnimation.append(aBoite[i]);
        groupe.show();
        aGroupe[i]=groupe;
    }
    
    // le sachet
    sachet = disp.createImageSprite(exo,"sachet")
    sachet.css({left:aBoite[0].position().left+20,top:250});
    exo.blocAnimation.append(sachet);
    
    // on lance l'animation
    distribuerBonbon(0);
    
    
    //
    function creerBonbon(sBonbon,nTotal){
        console.log(sBonbon,nTotal)
        var a=20;
        var aConfig = [[[a,a]],[[0,0],[2*a,2*a]],[[0,0],[a,a],[2*a,2*a]],[[0,0],[0,2*a],[2*a,0],[2*a,2*a]],[[0,0],[0,2*a],[2*a,0],[2*a,2*a],[a,a]]];
        var conteneur = disp.createEmptySprite();
        conteneur.css({width:60,height:60})
        for(var i=0; i<nTotal; i++){
            var bonbon = disp.createImageSprite(exo,sBonbon);
            bonbon.css({left:aConfig[nTotal-1][i][0],top:aConfig[nTotal-1][i][1]});
            conteneur.append(bonbon)
        }
        conteneur.hide();
        return conteneur
    }
    
    //
    function distribuerBonbon(index) {
        var groupe = aGroupe[index];
        groupe
            .transition({y:110},1000,"linear")
            .transition({y:200,delay:2000},1000,"linear",function(){
                groupe.hide();
                //anim.translateStop(sachet)
                sachet.transition({x:180*(index+1),delay:500},500,"linear",function(){
                    index++;
                    if (index < nBoite) {
                       distribuerBonbon(index);
                    }
                    else {
                        exo.keyboard.config({numeric : "enabled", large : "enabled"});
                        // afficher le champ réponse
                        conteneurReponse = disp.createEmptySprite();
                        exo.blocAnimation.append(conteneurReponse);
                        conteneurReponse.css({width:735})
                        var labelA = disp.createTextLabel("Ce sachet coûtera");
                        var labelB = disp.createTextLabel("centimes.");
                        champReponse = disp.createTextField(exo,4);
                        conteneurReponse.append(labelA,champReponse,labelB);
                        labelA.css({top:5})
                        champReponse.css({left:labelA.width()+10});
                        labelB.css({left:champReponse.position().left+champReponse.width()+20,top:5});
                        conteneurReponse.css({width:labelB.position().left+labelB.width()});
                        champReponse.focus();
                        conteneurReponse.css({left:(735-conteneurReponse.width())/2,top:280});
                        console.log(conteneurReponse.position().left);
                        anim.translateStop(sachet)
                        sachet.css({left:conteneurReponse.position().left+conteneurReponse.width()})
                    }
                });
        })
    }
    
    
}

// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {
    sachet.hide();
    if (champReponse.val() == "") {
        return "rien";
    }
    else {
        var repEleve = util.strToNum(champReponse.val());
        if (repEleve == soluce ) {
            return "juste";
        }
        else {
            return "faux";
        }
    }
   
}

// Correction (peut rester vide)
exo.corriger = function() {
    for(var i=0; i< aGroupe.length; i++){
        aGroupe[i].css({top:"-=100"})
        aGroupe[i].show();
        var subTotal = aTotalBonbon[i]*aPrix[i];
        var label = disp.createTextLabel(String(subTotal));
        exo.blocAnimation.append(label);
        label.css({
            left:aGroupe[i].position().left+20,
            top:230,
            color:"#ff0000"
        })
    }
    var correction = disp.createCorrectionLabel(soluce);
    exo.blocAnimation.append(correction).css({width:champReponse.width()})
    correction.position({
        my:"center top",
        at:"center bottom+5",
        of:champReponse
    })
    var barre = disp.drawBar(champReponse);
    conteneurReponse.append(barre);
}

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:exo.txt.opt1
    });
    exo.blocParametre.append(controle);
    //
    var controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"totalEssai",
        texte:exo.txt.opt2,
        aLabel:exo.txt.label2,
        aValeur:[1,2]
    });
    exo.blocParametre.append(controle);
    //
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"tempsQuestion",
        texte:exo.txt.opt3
    });
    exo.blocParametre.append(controle);
    
    var controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"totalBoite",
        texte:exo.txt.opt4,
        aLabel:exo.txt.label4,
        aValeur:[1,2,3]
    });
    exo.blocParametre.append(controle);
    
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:100,
        taille:10,
        nom:"intervalePrix",
        texte:exo.txt.opt5
    });
    exo.blocParametre.append(controle);
}

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
}
return clc;
}(CLC))