var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.oiseaumultiplication = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre=[1,2,3];
var aNombre,repEleve;

// Définir les options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:1,
        totalTentative:10,
        totalEssai:1,
        tempsExo:0,
        vitesse:3,
        plageA:"2-9",
        plageB:"0-10",
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Référencer les ressources de l'exercice (textes, image, son)
// exo.oRessources peut être soit un objet soit une fonction qui renvoie un objet
exo.oRessources = { 
    txt         :   "oiseaumultiplication/textes/oiseaumultiplication_fr.json",
    nuageA      :   "oiseaumultiplication/images/nuage-a.png",
    nuageNoirA  :   "oiseaumultiplication/images/nuage-a-noir.png",
    nuageNoirB  :   "oiseaumultiplication/images/nuage-b-noir.png",
    nuageB      :   "oiseaumultiplication/images/nuage-b.png",
    nuageC      :   "oiseaumultiplication/images/nuage-c.png",
    oiseau      :   "oiseaumultiplication/images/oiseau.png",
    oeuf        :   "oiseaumultiplication/images/oeuf.png",
    illustration:   "oiseaumultiplication/images/illustration.jpg"

    // illustration : "template/images/illustration.png"
};

// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
    aNombre = [];
    aPlageA = util.getArrayNombre(""+exo.options.plageA);
    aPlageB = util.getArrayNombre(""+exo.options.plageB);
    var lengthA = aPlageA.length;
    var lengthB = aPlageB.length;
    // on genere tous les couples possibles 
    for( var i = 0 ; i < lengthA ; i++ ) {
        var nA = aPlageA[i];
        for ( var j = 0 ; j < lengthB ; j++ ) {
            var nB = aPlageB[j];
            aNombre.push([nA,nB]);
        }
    }
    util.shuffleArray(aNombre);
    // on evite de manipuler un tableau trop long (100 couples maxi)
    if( aNombre.length > 100 ) {
        aNombre = aNombre.slice(0,exo.options.totalTentative);
    }
    // si les parametres ne génèrent pas assez de couples on répète
    else if ( aNombre.length < exo.options.totalTentative ) {
        var n = 0;
        while ( (exo.options.totalTentative - aNombre.length)>0 || n < 100) {
            n++;
            aNombre = aNombre.concat(aNombre.slice(0,exo.options.totalTentative - aNombre.length));
        }
    }
};

//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.append(illustration);
};

//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    exo.btnValider.hide();
    exo.keyboard.config({
        numeric : "disabled",
        arrow : "disabled",
        large : "disabled"
    });
    // le fond
    var fond = disp.createEmptySprite();
    fond.css({width:735,height:450,background:"#E9F2F5"});
    exo.blocAnimation.append(fond);
    // Les nugages
    var nuageA = disp.createImageSprite(exo,"nuageA");
    nuageA.css({left:-53,top:127});
    var nuageNoirA = disp.createImageSprite(exo,"nuageNoirA");
    nuageNoirA.css({display:"none",left:-53,top:127});
    var nuageB = disp.createImageSprite(exo,"nuageB");
    nuageB.css({left:-29,top:9});
    var nuageNoirB = disp.createImageSprite(exo,"nuageNoirB");
    nuageNoirB.css({display:"none",left:-29,top:9});
    var nuageD = disp.createImageSprite(exo,"nuageA");
    nuageD.css({left:587,top:47});
    var nuageNoirD = disp.createImageSprite(exo,"nuageNoirA");
    nuageNoirD.css({display:'none',left:587,top:47});
    var nuageC = disp.createImageSprite(exo,"nuageC");
    nuageC.css({left:300,top:105});
    
    // l'oiseau
    var conteneurOiseau = disp.createEmptySprite();
    conteneurOiseau.css({width:160,height:80,left:805,top:120});
    var oiseau = disp.createImageSprite(exo,"oiseau");
    oiseau.css({left:45});
    var nA = aNombre[exo.indiceTentative][0];
    var nB = aNombre[exo.indiceTentative][1];
    var etiquette = disp.createTextLabel(nA+" x "+nB+" = ?");
    etiquette.css({width:160,top:50,fontSize:18,fontWeight:"bold",textAlign:"center"});
    conteneurOiseau.append(oiseau,etiquette);
    exo.blocAnimation.append(nuageC,conteneurOiseau,nuageD,nuageA,nuageB,nuageNoirA,nuageNoirB,nuageNoirD);
    // le conteneur feedback juste
    var contFbJuste = disp.createTextLabel("Bravo !");
    exo.blocAnimation.append(contFbJuste);
    contFbJuste.css({display:'none',fontSize:24,padding:5,color:"#00C707",left:(750-contFbJuste.width())/2,top:250});
    
    // les propositions
    var conteneurPropos = disp.createEmptySprite();
    var aValeurPossible;
    console.log(nA,nB)
    if(nB > 2){
        aValeurPossible = [
            [nA*(nB-2),nA*(nB-1),nA*nB],
            [nA*(nB-1),nA*nB,nA*(nB+1)],
            [nA*nB,nA*(nB+1),nA*(nB+2)]
        ];
    }
    else {
        aValeurPossible = [
            [nA*nB,nA*(nB+1),nA*(nB+2)]
        ];
    }
    util.shuffleArray(aValeurPossible);
    var aValeur = aValeurPossible[0];
    console.log("aValeur",aValeur);
    for(var i=0;i<3;i++){
        var bouton =disp.createTextLabel(aValeur[i]);
        bouton.css({fontSize:18,fontWeight:"bold",textAlign:"center",width:70,height:40,lineHeight:"40px",background:"#FFD400"});
        conteneurPropos.append(bouton);
        bouton.css({left:i*80});
        bouton.on("mousedown.clc touchstart.clc",gererClickNombre);
    }
    conteneurPropos.css({left:257,top:300});
    exo.blocAnimation.append(conteneurPropos);
    conteneurPropos.hide();

    // le conteneur où l'on range les oeufs
    var boiteOeuf = disp.createEmptySprite();
    boiteOeuf.css({left:20,top:380,width:700,height:30});
    exo.blocAnimation.append(boiteOeuf);
    // demarrage de l'animation
    var testRep = false; // false si aucune réponse ; true si il y a eu une réponse
    repEleve = false;
    var angle=0;
    var t = 0;
    var vitesse=exo.options.vitesse*0.4;
    var vitesseCroisiere=vitesse;
    var vitesseRalenti=vitesse/2;
    var vitesseSortieFaux=vitesse+2;
    var vitesseSortieJuste=5*vitesse;
    var animId = requestAnimationFrame(bouclerAnimation);

    function bouclerAnimation(){
        animId = requestAnimationFrame(bouclerAnimation);
        angle-=Math.PI/360;
        conteneurOiseau.css({left:"-="+vitesse,top:120+(50*Math.sin(angle))});
        var posX = conteneurOiseau.position().left;
        // l'oiseau est passé     
        if(posX < 20 ){
            // on arrete l'animation
            cancelAnimationFrame(animId);
            // on cache les propositions
            conteneurPropos.hide();
            // l'élève n'a pas répondu
            if( testRep === false ) {
                // on lance quand même une évaluation
                ret = exo.poursuivreQuestion();
                // si c'est la derniere tentative on arrête l'exercice
                if(ret === false){
                    return;
                }
                // sinon 
                else{
                    // on affiche la réponse attendue
                    fond.css({background:"#999"});
                    var contFbFaux = disp.createTextLabel("Trop tard !<br>"+nA+" x "+nB+" = "+(nA*nB)+"<br>"+"Il fallait cliquer sur "+(nA*nB));
                    exo.blocAnimation.append(contFbFaux);
                    contFbFaux.css({fontSize:24,padding:5,color:"#FFEB2B",textAlign:"center",left:(750-contFbFaux.width())/2,top:220});
            
                    //on met à jour les valeurs
                    repEleve = false;
                    testRep = false;
                    updateValeurs();
                    //on replace l'oiseau
                    conteneurOiseau.css({left:805,top:120});
                    // on relance l'animation
                    exo.setTimeout(function(){
                        fond.css({background:"#E9F2F5"});
                        contFbFaux.remove();
                        bouclerAnimation();
                    },4000);
                }
            }
            // l'élève a répondu
            else {
                //on met à jour les valeurs
                repEleve = false;
                testRep = false;
                updateValeurs();
                conteneurPropos.find("*").on("mousedown.clc touchstart.clc",gererClickNombre);
                //on replace l'oiseau
                conteneurOiseau.css({left:805,top:120});
                // on relance l'animation
                bouclerAnimation();
            }
        }
        // l'oiseau est hors scene à droite;
        else if (posX > 550){
            conteneurPropos.hide();
            vitesse = vitesseCroisiere;
        }
        // l'oiseau est sur la scene
        else {
            conteneurPropos.show();
            if (posX > 317-35 && posX < 417-35 ) {
                vitesse = vitesseRalenti;
            } 
            else {
                vitesse = vitesseCroisiere;
            }

        }
    }

    function gererClickNombre(e){
        e.preventDefault();
        conteneurPropos.find("*").off(".clc");
        // on arrete l'animation
        cancelAnimationFrame(animId);
        // on recupère la reponse de l'élève
        repEleve = Number($(e.target).text());
        testRep = true;
        // on récupère les valeurs courantes de nA et nB
        var nA = aNombre[exo.indiceTentative][0];
        var nB = aNombre[exo.indiceTentative][1];
        // on evalue
        var ret = exo.poursuivreQuestion();
        // bonne réponse l'oeuf bonus
        var posX;
        if(exo.evaluer() == "juste"){
            var oeuf = disp.createImageSprite(exo,"oeuf");
            oeuf.css({left:conteneurOiseau.position().left+50,top:conteneurOiseau.position().top+80});
            exo.blocAnimation.append(oeuf);
            console.log(450-oeuf.position().top);
            var dy = 450-oeuf.position().top - 80;
            oeuf.transition({y:"+="+dy},400,rangerOeuf);
            //
            contFbJuste.show();
            // on fait sortir l'oiseau
            posX = -conteneurOiseau.position().left;
            conteneurOiseau.transition({x:posX},1500,"linear",function(){
                contFbJuste.hide();
                // on reprend l'animation
                if(ret === true)
                    bouclerAnimation();
            });
        }
        // mauvaise reponse l'orage
        else if(exo.evaluer() == "faux"){
            fond.css({background:"#999"});
            flasher.apply(nuageNoirA);
            flasher.apply(nuageNoirB);
            flasher.apply(nuageNoirD);
            var animNuageNoirA = anim.creerDemon(nuageNoirA,flasher,900,3);
            var animNuageNoirB = anim.creerDemon(nuageNoirB,flasher,910,3);
            var animNuageNoirD = anim.creerDemon(nuageNoirD,flasher,920,3);
            animNuageNoirA.start();
            animNuageNoirB.start();
            animNuageNoirD.start();
            // le conteneur feedback faux
            var contFbFaux = disp.createTextLabel("Erreur !<br>"+nA+" x "+nB+" = "+(nA*nB));
            exo.blocAnimation.append(contFbFaux);
            contFbFaux.css({fontSize:24,padding:5,color:"#FFEB2B",textAlign:"center",left:(750-contFbFaux.width())/2,top:220});
            
            posX = -conteneurOiseau.position().left;
            conteneurOiseau.transition({x:posX,delay:4000},400,"linear",function(){
                contFbFaux.remove();
                fond.transition({background:"#E9F2F5"},1000);
                // on reprend l'animation
                if(ret === true)
                    bouclerAnimation();
            });
        }
    }

    function updateValeurs(){
        nA = aNombre[exo.indiceTentative][0];
        nB = aNombre[exo.indiceTentative][1];
        var aValeurPossible;
        console.log(nA,nB);
        if(nB > 2){
            aValeurPossible = [
                [nA*(nB-2),nA*(nB-1),nA*nB],
                [nA*(nB-1),nA*nB,nA*(nB+1)],
                [nA*nB,nA*(nB+1),nA*(nB+2)]
            ];
        }
        else {
            aValeurPossible = [
                [nA*nB,nA*(nB+1),nA*(nB+2)]
            ];
        }
        util.shuffleArray(aValeurPossible);
        var aVal = aValeurPossible[0];
        etiquette.html(nA+" x "+nB+" = ?");
        vitesseCroisiere = exo.options.vitesse*0.4;
        conteneurPropos.children().each(function(index,value){
            $(value).html(aVal[index]);
        });
        conteneurOiseau.transition({x:0},0);
    }

    function rangerOeuf(){
        var oeuf = this;
        boiteOeuf.append(oeuf);
        oeuf.css({position:"static",float:"left",y:0});
    }

    function flasher(count){
        var duree = 850;
        var objet = this;
        objet.show();
        exo.setTimeout(function(){
            objet.hide();
        },duree);
    }


};

// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {
    var q = exo.indiceTentative-1;
    console.log("exo.indiceTentative",exo.indiceTentative);
    var soluce = aNombre[q][0]*aNombre[q][1];
    // l'élève n'a pas répondu ou répondu faux
    if(repEleve === false || repEleve !== soluce){
        console.log("faux");
        return "faux";
    }
    // bonne réponse
    else {
        console.log("juste");
        return "juste";
    }
    
};

// Correction (peut rester vide)
exo.corriger = function() {
    
};

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle;
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:300,
        nom:"plageA",
        texte:"Nombre A :"
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:300,
        nom:"plageB",
        texte:"Nombre B :"
    });
    exo.blocParametre.append(controle);

    controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"vitesse",
        texte:"Vitesse :",
        aValeur:[
            2,
            3,
            4,
            5
        ],
        aLabel:[
            "lente",
            "moyenne",
            "rapide",
            "très rapide"
        ]
    });
    exo.blocParametre.append(controle);
    
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));