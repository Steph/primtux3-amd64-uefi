﻿class CadreOptions{
	private var objExo:CalcExo;
	public var cadre_options:MovieClip;
	public var controles_option:Array;
	private var format:TextFormat;
	private var illustration:MovieClip;
	
	public function CadreOptions(obj){
		this.objExo=obj;
		this.controles_option=new Array();
		this.cadre_options = objExo.rootMovie.createEmptyMovieClip("cadre_options",objExo.rootMovie.getNextHighestDepth());
		var tf_titre=this.cadre_options.createTextField("tf_titre",this.cadre_options.getNextHighestDepth(),0,0,0,0);
		tf_titre.embedFonts=true;
		tf_titre.antiAliasType = "advanced";
		tf_titre.text="Options";
		this.format = new TextFormat();
		this.format.bold = true;
		this.format.font = "emb_tahoma";
		this.format.align = "center";
		this.format.size = 22;
		tf_titre.setTextFormat(this.format);
		tf_titre.autoSize=true;
		tf_titre._x=(objExo.rootMovie._width-tf_titre._width)/2;
		tf_titre._y=5;
		
	}
	
	public function CreeControleOption(nom_option:String, type_controle:String, aTextes:Array, aValeurs:Array, sOrientation:String) {
		var controle_existe = false;
		for (var i = 0; i<this.controles_option.length; i++) {
			if (nom_option == this.controles_option[i].nom) {
				controle_existe = true;
			}
		}
		if (!controle_existe) {
			if (type_controle == "input") {
				var controle = new OptionInput(this.cadre_options, nom_option, aTextes[0]);
				controle.SetValeur(String(objExo.options[nom_option]));
			} else if (type_controle == "radio") {
				var controle = new OptionRadio(this.cadre_options, nom_option, sOrientation);
				controle.AjouteBoutons(aTextes, aValeurs);
				controle.SetValeur(String(objExo.options[nom_option]));
			} else if (type_controle == "checkbox") {
				var controle = new OptionCheckBox(this.cadre_options, nom_option, sOrientation);
				controle.AjouteCases(aTextes, aValeurs);
				for(i=0;i<objExo.options[nom_option].length;i++){
					controle.SetValeur(String(objExo.options[nom_option][i]));
				}
			}
			this.controles_option.push(controle);
			
			//controle.SetValeur(String(objExo.options[nom_option]));
			return controle;
		}
	}
	
	public function CreeLabel(texte){
		var etiquette=this.cadre_options.createTextField("label"+getTimer(),this.cadre_options.getNextHighestDepth(),0,0,20,20);
		etiquette.embedFonts=true;
		etiquette.antiAliasType = "advanced";
		etiquette.text=texte;
		var format=new TextFormat();
		format.size=14;
		format.font="emb_tahoma";
		etiquette.setTextFormat(format);
		etiquette.autoSize=true;
		etiquette.align="left";
		return etiquette;
	}
	
	public function Hide(){
		this.cadre_options._visible=false;
	}
	public function Show(){
		this.cadre_options._visible=true;
	}
	public function AddIllustration(nom:String){
		var d = this.cadre_options.getNextHighestDepth();
		this.illustration=this.cadre_options.attachMovie(nom,"illustration_option",d);
		return this.illustration;
	}
}