var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.lebanquier = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre;
var i, j, cible, soluce, cartouche, caisse, tapis, monnaieClique, bonneReponse;
//Tableau de valeur de chaque pièce ou billet en centimes
var valeurMonnaie = [1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000];
//tableau stockant les 5 ou 10 solutions de l'exo
var tab_donnees_soluce = new Array();
//tableau stockant les quantités de pièces et de billets donnés par l'élève
var tab_monnaie_donnee = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
//Tableau stockant les nom des fichiers ressources pour l'affichage
var tab_ressources = ["unCent","deuxCent","cinqCent","dixCent","vingtCent","cinquanteCent","unEuro","deuxEuro","cinqEuro","dixEuro","vingtEuro","cinquanteEuro","centEuro","deuxcentsEuro","cinqcentsEuro"]
// Référencer les ressources de l'exercice (image, son)

exo.oRessources = { 
    //illustration : "monexo/images/illustration.png"
	illustration:"lebanquier/images/illustration.png",
	unCent:"lebanquier/images/1.png",
	deuxCent:"lebanquier/images/2.png",
	cinqCent:"lebanquier/images/5.png",
	dixCent:"lebanquier/images/10.png",
	vingtCent:"lebanquier/images/20.png",
	cinquanteCent:"lebanquier/images/50.png",
	unEuro:"lebanquier/images/100.png",
	deuxEuro:"lebanquier/images/200.png",
	cinqEuro:"lebanquier/images/500.png",
	dixEuro:"lebanquier/images/1000.png",
	vingtEuro:"lebanquier/images/2000.png",
	cinquanteEuro:"lebanquier/images/5000.png",
	centEuro:"lebanquier/images/10000.png",
	deuxcentsEuro:"lebanquier/images/20000.png",
	cinqcentsEuro:"lebanquier/images/50000.png"
	
}

// Options par défaut de l'exercice (définir au moins totalQuestion et tempsExo )

exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:10,
        tempsExo:0,
		tempsQuestion:0,
		totalEssai:1,
		niveau:3
    }
    $.extend(exo.options,optionsParDefaut,oOptions);
}

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
	//cible <= 10
	//especes 1 2 5 euros

	if (exo.options.niveau == 0) {
		exo.options.totalQuestion=5;
		tab_monnaie=[0,0,0,0,0,0,1,2,1,0,0,0,0,0,0];//1 pièce de 1, 2 de 2, et un billet de 5
		for(i=0;i<5;i++) {
			cible = 6 + i;
			soluce = calculer_soluce();
			tab_donnees_soluce.push(new Array(cible,soluce));
		}
	}
	//cible <= 20
	//especes 1 2 5 10 euros
	else if (exo.options.niveau == 1) {
		exo.options.totalQuestion = 10;
		tab_monnaie=[0,0,0,0,0,0,1,2,1,1,0,0,0,0,0];//1 pièce de 1, 2 de 2, un billet de 5 et 1 de 10
		tab_donnees_soluce = new Array();//On vide le tableau qui s'est rempli au lancement de l'exo avec les valeurs du niveau 0
		for(i=0;i<10;i++) {
			cible = 11 + i;
			soluce = calculer_soluce();
			tab_donnees_soluce.push(new Array(cible,soluce));
		}
	}
	//cible <= 50
	//especes 1 2 5 10 20 euros
	else if (exo.options.niveau == 2) {
		exo.options.totalQuestion = 10;
		tab_monnaie=[0,0,0,0,0,0,1,2,2,2,2,0,0,0,0];//1 pièce de 1, 2 de 2, 2 billets de 5, 2 de 10, 2 de 20
		tab_cible = new Array();
		tab_donnees_soluce = new Array();//On vide le tableau qui s'est rempli au lancement de l'exo avec les valeurs du niveau 0
		for(i=0;i<10;i++) {
			cible = 23 + Math.floor(Math.random()*(50-23+1));
			if( tab_cible.indexOf(cible)>-1 || cible == 25 ) {
				i--;
			} else {
				tab_cible.push(cible);
				soluce = calculer_soluce();
				tab_donnees_soluce.push(new Array(cible,soluce));
			}
		}
	}
	//cible < 100
	//especes 1 2 5 10 20 50 euros
	else if (exo.options.niveau == 3) {
		exo.options.totalQuestion = 10;
		tab_monnaie=[0,0,0,0,0,0,1,2,2,2,2,1,0,0,0];//1 pièce de 1, 2 de 2, 2 billets de 5, 2 de 10, 2 de 20 et 1 de 50
		tab_cible = new Array();
		tab_donnees_soluce = new Array();//On vide le tableau qui s'est rempli au lancement de l'exo avec les valeurs du niveau 0
		for(i=0;i<10;i++) {
			cible = 53 + Math.floor(Math.random()*(99-53+1));
			if( tab_cible.indexOf(cible)>-1 || cible == 55 ) {
				i--;
			} else {
				tab_cible.push(cible);
				soluce = calculer_soluce();
				tab_donnees_soluce.push(new Array(cible,soluce));
			}
		}
	}
	// pour test
	else if (exo.options.niveau == 4) {
		exo.options.totalQuestion = 10;
		tab_monnaie=[5,5,5,5,5,5,5,5,5,5,5,5,0,0,0];
		tab_cible = new Array();
		tab_donnees_soluce = new Array();//On vide le tableau qui s'est rempli au lancement de l'exo avec les valeurs du niveau 0
		for(i=0;i<10;i++) {
			cible = 53 + Math.floor(Math.random()*(99-53+1));
			if( tab_cible.indexOf(cible)>-1 || cible == 55 ) {
				i--;
			} else {
				tab_cible.push(cible);
				soluce = calculer_soluce();
				tab_donnees_soluce.push(new Array(cible,soluce));
			}
		}
	}

//

	function calculer_soluce() {/*
	Renvoit un tableau de la même structure que tab_monnaie, avec la solution 
	(quantité de pièces et billets de chaque valeur)	
	*/
		var total = cible *100;
		var tab_soluce = new Array();
		//calcul de la meilleure solution possible
		for ( j = tab_monnaie.length-1 ; j >= 0 ; j-- ) {
			var valeur = valeurMonnaie[j];
			if(tab_monnaie[j] == 0 ) {
				tab_soluce[j] =  0;
			} else {
				var nTemp = Math.floor(total/valeur);
				if(nTemp > tab_monnaie[j]) {
					tab_soluce[j] = tab_monnaie[j]
				} else {
					tab_soluce[j] =  nTemp;
				}
			}
			total -= tab_soluce[j] * valeur;
		}
		return tab_soluce;
	}
}
//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration

exo.creerPageTitre = function() {
    exo.blocTitre.html("Le Banquier");
    exo.blocConsigneGenerale.html("Donner la somme demandée. Cliquer sur les billets ou les pièces.");
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration)    
}

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
	
	exo.btnValider.position({
		my:"right top",
		at:"right-10 top+260",
		of:exo.blocAnimation
	});
	
	exo.keyboard.config({
		numeric : "disabled",
		float : "disabled",
		delete : "disabled",
		arrow : "disabled"
    });
	
	
	var q = exo.indiceQuestion;
    var texte = "Donne " + tab_donnees_soluce[q][0] + " euros.";//La solution est pour le moment stockée en €. On peut donc l'afficher telle quelle.
	
	// la consigne
	var svgConsigne = disp.createSvgContainer(300,60);
	var blocConsigne = svgConsigne.paper.rect(0,0,300,60,5);
	blocConsigne.attr("fill","90-#ddd-#eee");
	blocConsigne.attr("stroke","#ccc");
	//cartouche = disp.createImageSprite(exo,"cartouche");//Bloc orange
    exo.blocAnimation.append(svgConsigne);
    svgConsigne.position({//On le place centré à 25 px du haut de l'exo
        my:'center top',
        at:'center top+20',
        of:exo.blocAnimation
    });
	var etiquette = svgConsigne.paper.text(blocConsigne.getBBox().width/2,blocConsigne.getBBox().height/2,texte);
	etiquette.attr("font-size","24px");
	
	//La caisse
	var svgFond = disp.createSvgContainer(735,150);
	var rectFond = svgFond.paper.rect(0,0,735,150);
	rectFond.attr("fill","90-#7DD0FF-#4FBDFF");
	rectFond.attr("stroke","none");
	exo.blocAnimation.append(svgFond);
	svgFond.position({
		my:"center bottom",
		at:"center bottom",
		of:exo.blocAnimation
	})
	caisse = disp.createEmptySprite();
	caisse.css({height:130,overflow:"hidden"});
	exo.blocAnimation.append(caisse);
	caisse.position({
		my:'center bottom',
		at:'center bottom',
		of:exo.blocAnimation
	});
	console.log("caisse.position()",caisse.position())
	
	// le tapis
	tapis = disp.createEmptySprite();
	exo.blocAnimation.append(tapis);
	
	bonneReponse = tab_donnees_soluce[q][0]*100; //On récupère la valeur de la solution
	//et on la multiplie par 100 car on stocke les centimes et non les valeurs en €
	var count = 0;//Var servant à décaler les pièces et billets, pour éviter qu'ils ne se superposent
	var largeurCaisse = 0;
	var offsetLeft = 0;
	var aEspece = new Array();
	
	//calcul de la largeur de la caisse en fonction des especes choisies
	for (i = 0; i < 15; i++) {
		if (tab_monnaie[i]!=0) {//On vérifie s'il faut afficher ou non une "espèce", d'après les tableau créés plus haut
			for ( j = 0; j < tab_monnaie[i]; j++) {//On regarde combien il faut en créer
				var espece = disp.createImageSprite(exo,tab_ressources[i]);//On ajoute la bonne espèce
				espece.on("mousedown.clc",gestionClickEspece);
				espece.data("position","basse")
				espece.data("indice",i)
				aEspece.push(espece);
			}
			offsetLeft += espece.width()+5;
		}
	}
	// placement des espece dans la caisse
	if( offsetLeft > 700 ) {
		largeurCaisse=700;	
	} else {
		largeurCaisse=offsetLeft;
	}
	var reduc = largeurCaisse/offsetLeft;
	var count=0;
	var newLeft = 0;
	for (i = 0; i < 15; i++) {
		if (tab_monnaie[i]!=0) {
			for ( j = 0; j < tab_monnaie[i]; j++) {
				var newWidth=(aEspece[count].width())*reduc;
				aEspece[count].css({
					width:newWidth,
					left:newLeft,
					top:j*5
				});
				caisse.append(aEspece[count]);
				count++;
			}
			newLeft += newWidth+(5*reduc);
		}
	}
	caisse.css({width:largeurCaisse})
	caisse.position({
		my:'center bottom',
		at:'center bottom',
		of:exo.blocAnimation
	});


var largeurAbsolue=0;
function gestionClickEspece(e){
    especeClique = $(e.delegateTarget);//On récupère l'espèce sur laquelle on a cliqué 
	if (especeClique.data("position") == "basse") {//Si c'est en position basse, on la monte
		var y = 140;
		especeClique.data("caisseX",especeClique.position().left);
		especeClique.data("caisseY",especeClique.position().top);
		especeClique.data("caisseW",especeClique.width());
		especeClique.data("caisseH",especeClique.height());
		var posx=caisse.position().left+especeClique.position().left;
		var posy=caisse.position().top+especeClique.position().top;
		exo.blocAnimation.append(especeClique);//Remet au premier plan la pièce ou le billet
		especeClique.css({left:posx,top:posy})
		especeClique.transition({left:367,top:100},100,placerEspece);//Animation avec un easeInCubic
		tab_monnaie_donnee[especeClique.data("indice")]++;//On incrémente le tableau tab_monnaie_donnee (grâce à l'indice stocké);
		largeurAbsolue+=especeClique.width();
		
	}
	if (especeClique.data("position") == "haute") {//Si c'est en position haute, on la rabaisse
		var y = especeClique.data("y");//On récupère juste le y, il n'est pas modifié : pas besoin de le restocker.
		var posx=tapis.position().left+especeClique.position().left;
		var posy=tapis.position().top+especeClique.position().top;
		exo.blocAnimation.append(especeClique);//Remet au premier plan la pièce ou le billet
		especeClique.css({position:"absolute",margin:"none",left:posx,top:posy});
		especeClique.transition({left:367,top:320},100,placerEspece);//Animation avec un easeInCubic
		tab_monnaie_donnee[especeClique.data("indice")]--;//On décrémente le tableau tab_monnaie_donnee (grâce à l'indice stocké)
		//puisqu'on a retiré une espèce
		largeurAbsolue-=especeClique.width();
		
	}
}

function placerEspece(e){
	if (especeClique.data("position") == "basse") {
		tapis.append(especeClique);
		especeClique.css({position:"static",display:"inline-block",verticalAlign:"middle",margin:0});
		console.log(largeurAbsolue)
		if(largeurAbsolue>500){
			var d=500/largeurAbsolue;
			tapis.children().each(function(index,value){
				$(value).css({width:$(value).data("caisseW")*d});
				$(value).css({height:$(value).data("caisseH")*d})
			})
		}
		//especeClique.css({width:especeClique.width()})
		tapis.position({
			my:"center top",
			at:"center top+110",
			of:exo.blocAnimation
		});
		especeClique.data("position","haute");//On stocke le nouveau positionnement (haut ou bas) de l'espèce
		
	}
	else if (especeClique.data("position") == "haute") {
		var marge = 0 || tapis.children().length ;
		caisse.append(especeClique);
		var caisseX = especeClique.data("caisseX");
		var caisseY = especeClique.data("caisseY");
		especeClique.css({left:caisseX,top:caisseY,margin:0})
		especeClique.css({
			width:especeClique.data("caisseW"),
			height:especeClique.data("caisseH")
		})
		if(largeurAbsolue>500){
			var d=500/largeurAbsolue;
			tapis.children().each(function(index,value){
				$(value).css({width:$(value).data("caisseW")*d});
				$(value).css({height:$(value).data("caisseH")*d})
			})
		}
		//especeClique.css({width:especeClique.width()})
		tapis.position({
			my:"center top",
			at:"center top+110",
			of:exo.blocAnimation
		});
		especeClique.data("position","basse");//On stocke le nouveau positionnement (haut ou bas) de l'espèce
	}
	
	//exo.blocAnimation.append(especeClique)
}

}
    

// Evaluation doit toujours retourner "juste" "faux" ou "rien"


exo.evaluer = function() {
	var repEleve = 0;
	for (i = 0; i < 15; i++) {//On parcoure le tableau tab_monnaie_donnee pour voir ce que l'élève a sorti de la caisse
		repEleve = repEleve + (tab_monnaie_donnee[i] * valeurMonnaie[i]);//On multiplie la quantité d'espèces par la valeur de chacune
		tab_monnaie_donnee[i] = 0; // Permet de réinitialiser le tableau stockant la monnaie fournie par l'élève pour la suite de l'exercice
	}
	if (repEleve == 0) { 
		return "rien";
	}
	if (repEleve == bonneReponse) {
		
		return "juste"
	} else {
		
		return "faux";
	}
	
}

// Correction (peut rester vide)

exo.corriger = function() {
	var q = exo.indiceQuestion;
	exo.blocInfo.position({
		my:"right bottom",
		at:"right-10 bottom-50",
		of:exo.blocAnimation
	});
	
	var largeur = 300;
	var d = largeur/tapis.width();
	console.log(d)
	if(d<1){
		tapis.children().each(function(index,value){
			// on reduit de d le contenu du tapis
			var newWidth = $(value).width()*d
			$(value).css({
				width:$(value).width()*d,
				height:$(value).height()*d,
				marginLeft:3*d
			})
		})
	};
	tapis.transition({left:20},500,function(e){
		tapis.css({opacity:0.5})
		var tapisCorrection = disp.createEmptySprite();
		exo.blocAnimation.append(tapisCorrection);
		var tab_sol = tab_donnees_soluce[q][1];//On récupère la solution au format tableau 
		for (i = 14; i >= 0 ; i--) {
			if (tab_sol[i]!=0) {
				for (j = 0; j < tab_sol[i]; j++) {
					var espece = disp.createImageSprite(exo,tab_ressources[i]);
					tapisCorrection.append(espece);
					espece.css({position:"static",display:"inline-block",verticalAlign:"middle",margin:0});
				}
			}
		}
		var largeur = 300;
		var d = largeur/tapisCorrection.width();
		
		if(d<1){
			tapisCorrection.children().each(function(index,value){
				// on reduit de d le contenu du tapis
				var newWidth = $(value).width()*d
				$(value).css({
					width:$(value).width()*d,
					marginLeft:3*d
				})
			})
		}
		
		tapisCorrection.position({
			my: 'left top',
			at: 'center+20 top+100',
			of: exo.blocAnimation
		});
		
	})
	
	
	
	
	
	
}

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:"Nombre de questions : "
    });
    exo.blocParametre.append(controle);
    //
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"tempsExo",
        texte:"Temps pour réaliser l'exercice (en secondes)  : "
    });
	
    exo.blocParametre.append(controle);
    var controle = disp.createOptControl(exo,{
        type:"radio",
		nom:"niveau",
		texte:"Somme à donner",
		aLabel : [
			"inférieure ou égale à 10",
			"inférieure ou égale à 20",
			"inférieure ou égale à 50",
			"inférieure à 100"
		],
		aValeur : [0,1,2,3]
    });
    exo.blocParametre.append(controle);

}

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
}
return clc;
}(CLC))