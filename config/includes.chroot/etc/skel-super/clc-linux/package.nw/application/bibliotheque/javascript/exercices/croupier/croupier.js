var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.croupier = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre;
var aCarte,champReponse,soluce,animationTitre;
var creerCarte = function(valeur,couleur,couleurDos,paper) {
    var l=40,h=75;
    var aPositionA = [//les positions pour les cartes de 1 à 8
        [0,0],[l,0],[2*l,0],[l,h/2],[0,h],[l,h],
        [2*l,h],[l,3*h/2],[0,2*h],[l,2*h],[2*l,2*h]
    ];
    var aConstellationA=[//les constellations pour les cartes de 1 à 8
        [5],[1,9],[1,5,9],[0,2,8,10],[0,2,5,8,10],
        [0,2,4,6,8,10],[0,2,3,4,6,8,10],[0,2,3,4,6,7,8,10]
    ];
    var aPositionB = [//les positions pour les cartes 9 et 10
        [0,0],[2*l,0],[l,h/3],[0,2*h/3],[2*l,2*h/3],
        [l,h],[0,4*h/3],[2*l,4*h/3],[l,5*h/3],[0,2*h],[2*l,2*h]
    ];
    var aConstellationB=[//les constellations pour les cartes 9 et 10
        [0,1,3,4,5,6,7,9,10],[0,1,2,3,4,6,7,8,9,10]
    ];  
    var stCarte = paper.set();
    var fond = paper.rect(0,0,(3*l)+60,(3*h)+40,10).attr({fill:"#fff",stroke:"#666"});
    fond.data("valeur",valeur)
    stCarte.push(fond);
    if (valeur>8) {
        var constellation = aConstellationB[valeur-9];
        var aPosition = aPositionB;
    } else {
        constellation = aConstellationA[valeur-1];
        aPosition = aPositionA;
    }
    
    var stGroupe = paper.set();
    var st = disp.createRaphSet(exo,couleur,paper);
    var bbox = st.getBBox();
    var x  = bbox.width/2;
    var y = bbox.height/2;
    st.transform("T"+aPosition[constellation[0]].join(","));
    stGroupe.push(st);
    for (var i=1;i<constellation.length;i++) {
        st = st.clone();
        st.transform("T"+aPosition[constellation[i]].join(","));
        if (i>Math.floor((valeur-1)/2)) {
            st.transform("...r180,"+x+","+y)
        }
        stGroupe.push(st);
    }
    
    stGroupe.transform("...T30,40");
    stCarte.push(stGroupe);
    var fillColor = "#000";
    if (couleur=="carreau" || couleur=="coeur") {
        fillColor="#E6180A"
    }
    for (var i =0; i< 4 ; i++) {
        var x = 25 + (((i%2)*3*(l+2)));
        var y = 25 + (Math.floor(i/2)*3*(h-3));
        var lbl = paper.text(x,y,valeur).attr({"font-size":32,"font-weight":"bold",fill:fillColor})
        stCarte.push(lbl);
        if (i > 1) {
           lbl.transform("...R180")
        }
    }
    var dos = paper.rect(0,0,(3*l)+60,(3*h)+40,10).attr({fill:couleurDos,stroke:"#CCC"});
    var bbox = dos.getBBox();
    var x = bbox.width/2;
    var y = bbox.height/2;
    var cercle = paper.circle(x,y,1.5*l).attr({fill:"#ccc",stroke:"none",opacity:0.2})
    stCarte.push(dos,cercle);
    stCarte.attr("cursor","pointer");
    stCarte.estVisible = false;
    
    stCarte.retourner = function(){
        //var offset = this.getBBox().width+10;
        if (stCarte.estVisible) {
            dos.attr("opacity",1);
            cercle.attr("opacity",0.2);
            stCarte.estVisible = false;
            //stCarte.transform("...T,-"+offset+",0")
        }else{
            dos.attr("opacity",0);
            cercle.attr("opacity",0);
            stCarte.estVisible = true;
            //stCarte.transform("...T,"+offset+",0")
        }
    }
    return stCarte;
}

// Référencer les ressources de l'exercice (image, son)

exo.oRessources = { 
    //illustration : "monexo/images/illustration.png"
    carreau : "croupier/images/carreau.svg",
    coeur : "croupier/images/coeur.svg",
    pique : "croupier/images/pique.svg",
    trefle : "croupier/images/trefle.svg"
}

// Options par défaut de l'exercice (définir au moins totalQuestion et tempsExo )

exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:3,
        totalEssai:1,
        tempsExo:0,
        tempsQuestion:0,
        maxCartes:5
    }
    $.extend(exo.options,optionsParDefaut,oOptions);
}

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
    var aCouleur = ["carreau","coeur","pique","trefle"];
    // aCarte reference toutes les cartes possibles (de 2 à 10 et dans les 4 couleurs)
    aCarte=[];
    for (var i=0;i<4; i++) {
        for (var j = 2 ; j<=10 ; j++) {
            aCarte.push([j,aCouleur[i]])
        }
    }
   
}

//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration

exo.creerPageTitre = function() {
    exo.blocTitre.html("Le croupier");
    exo.blocConsigneGenerale.html("Comparez le total de points des trois tas de cartes.")
    var cnt = disp.createSvgContainer(600,280);
    exo.blocIllustration.append(cnt);
    aCarte.sort(function(){return -1 + Math.floor(Math.random()*3)});
    var paquet = cnt.paper.set();
    var aLabel = [];
    var tg = 0, tc = 0, td = 0;
    for (var i = 0 ; i < 9 ; i++) {
        var carte = creerCarte(aCarte[i][0],aCarte[i][1],"#662D91",cnt.paper);
        carte.transform("...S0.4,0.4,0,0");
        x = (200*Math.floor(i/3))+(i%3)*2;
        y = Math.floor(i/3)+(i%3)*2;
        carte.transform("...T"+x+","+y);
        paquet.push(carte);
        if (i%3==0) {
            var label = disp.createTextLabel("Total = ?").appendTo(exo.blocIllustration);
            label.css({left:x+80,top:250,fontSize:16});
            aLabel.push(label);
        }
    }
    var oContext = {pqt:paquet,lbl:aLabel,ttG:0,ttC:0,ttD:0}
    animationTitre = anim.creerDemon(oContext,animer,1200,22);
    console.log(animationTitre.INDEX)
    animationTitre.start();
    
    function animer(index) {
        index=(index-1)%12;
        //console.log(index);
        var paquet = this.pqt;
        if (index == 11) {
            // on range
            for (var i = 0 ; i < 9 ; i++) {
                paquet[i].retourner();
                paquet[i].transform("...T,-"+(paquet[i].getBBox().width+10)+",0");
            }
            for (var i = 0;i<3;i++) {
                aLabel[i].html("Total = ?");
            }
            this.ttG=this.ttC=this.ttD=0;
        } else if (index >8 && index<11) {
            // on ne fait rien 
        }
        else {
            var carte = paquet[index];
            var valeur = carte[0].data("valeur");
            carte.retourner();
            var offset = carte.getBBox().width+10;
            carte.transform("...T,"+offset+",0");
            if (index < 3) {
                this.ttG+=valeur;
                aLabel[0].html("Total = "+this.ttG)
            } else if (index>2 && index<6) {
                this.ttC+=valeur;
                aLabel[1].html("Total = "+this.ttC)
            } else if (index>5 && index<9) {
                this.ttD+=valeur;
                aLabel[2].html("Total = "+this.ttD)
            }
        }
        
        
    }
}

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
    
    animationTitre.pause();
    var cnt = disp.createSvgContainer(735,450);
    exo.blocAnimation.append(cnt);
    
    //la consigne
    var label = cnt.paper.text(367.5,44,"Clique sur les cartes pour les retourner.\nTrouve quel tas totalise le plus grand nombre de points.")
    label.attr({"font-size":18,"font-weight":"normal"});
    // determination du nombre de carte de chaque tas en fonction des options et de l'indice de la question;
    var nbreCarteTas = exo.options.maxCartes-exo.options.totalQuestion+exo.indiceQuestion+1;
    if (nbreCarteTas<2) nbreCarteTas=2;
    // on s'assure que les trois tas ont un total different
    totalTasGauche=0;
    totalTasCentre=0;
    totalTasDroite=0;
    while (totalTasCentre==totalTasDroite || totalTasCentre == totalTasGauche || totalTasGauche == totalTasDroite) {
        aCarte.sort(function(){return -1 + Math.floor(Math.random()*3)});
        for (var i = 0 ; i < 3*nbreCarteTas ; i++ ) {
            if (i<nbreCarteTas) {
                totalTasGauche+=aCarte[i][0];
            }else if (i>=nbreCarteTas && i<2*nbreCarteTas) {
                totalTasCentre+=aCarte[i][0];
            }else if (i>=2*nbreCarteTas && i<3*nbreCarteTas) {
                totalTasDroite+=aCarte[i][0];
            }
        }   
    }
    aChoix = [totalTasGauche,totalTasCentre,totalTasDroite];
    soluce=aChoix.indexOf(Math.max(totalTasCentre,totalTasDroite,totalTasGauche))+1;
    // on cree les cartes
    for (var i = 0 ; i < 3*nbreCarteTas ; i++ ) {
        var carte = creerCarte(aCarte[i][0],aCarte[i][1],"#662D91",cnt.paper);
        carte.transform("...S0.5,0.5,0,0");
        x = 20+(250*Math.floor(i/nbreCarteTas))+(i%nbreCarteTas)*2;
        y =100+Math.floor(i/nbreCarteTas)+(i%nbreCarteTas)*2;
        carte.transform("...T"+x+","+y);
        carte.click(function(e){
            this.toFront();
            this.retourner();
            var offset = this.getBBox().width+10;
            this.estVisible ? this.transform("...T,"+offset+",0") : this.transform("...T,-"+offset+",0");
            champReponse.focus();
        },carte);
    }
    
    // les etiquettes sous chaque tas
    for (var i=0; i < 3 ; i++) {
        label = cnt.paper.text((70+250*i),270,"Tas n°"+(i+1));
        label.attr({"font-size":18,"font-weight":"bold"})
    }
    // la phrase reponse :
    label = cnt.paper.text(367.5,320,"Le tas qui totalise le plus de points\nest le tas n°");
    label.attr({"font-size":18,"font-weight":"bold"});
    //l'étiquette réponse
    champReponse = disp.createTextField(exo,1);
    exo.blocAnimation.append(champReponse);
    champReponse.position({
        my:"center bottom",
        at:"center bottom-60",
        of:exo.blocAnimation
    });
    champReponse.focus();
}

// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
    if (champReponse.val() == "") {
        return "rien"
    }
    else if ( Number(champReponse.val()) == soluce ) {
        return "juste"
    }
    else {
        return "faux"
    }
    
}

// Correction (peut rester vide)

exo.corriger = function() {
    var correction = disp.createCorrectionLabel(soluce);
    exo.blocAnimation.append(correction)
    correction.position({
        my:"left center",
        at:"right center",
        of:champReponse,
        offset:"20px 0px"
    })
    var barre = disp.drawBar(champReponse);
    exo.blocAnimation.append(barre);
}

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:"Nombre de questions : "
    });
    exo.blocParametre.append(controle);
    //
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"tempsExo",
        texte:"Temps pour réaliser l'exercice (en secondes)  : "
    });
    exo.blocParametre.append(controle);
    //
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"maxCartes",
        texte:"Nombre maximum de cartes dans un paquet  : "
    });
    exo.blocParametre.append(controle);
    
}

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
}
return clc;
}(CLC))