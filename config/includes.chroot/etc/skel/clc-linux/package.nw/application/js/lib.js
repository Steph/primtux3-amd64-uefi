var DB = function(){

//Connexion à la base
	var sqlite3 = require("sqlite3");
	var chemin = "application/calculaticev2.db";
	var bdd = new sqlite3.Database(chemin);
	
//Propriétés
	this.bdd = bdd;
	//return bdd;

//Méthodes

//Méthode _requete
	this._requete = function(sql,type){
		//Variable différée
		var deferred = $.Deferred();
		if(type=="select"){
			//Traitement pour les selects
			//...
			console.log("SELECT");		
			this.bdd.all(sql, function(err, rows){
				if(err) {
					console.log(err.errno + " : " + err.code + " : " + err.message);
					deferred.resolve("erreur");
				}
				else {
					deferred.resolve(rows);
				}
			});
		} else if(type=="insert") {
			//Traitement pour les inserts
			// Retourne un objet différé contenant soit l'uid de la ligne insérée, soit un objet erreur
			console.log("Insert");
			this.bdd.run(sql, function(err){
				if(err) {
					console.log(err.errno + " : " + err.code + " : " + err.message);
					deferred.resolve("erreur");
				} else {
					deferred.resolve(this.lastID);
				}
			});
		} else if(type=="update") {
			//Traitement pour les update
			// Retourne un objet différé contenant soit le nombre de lignes modifiées, soit un objet erreur
			console.log("Update");
			this.bdd.run(sql, function(err){
				if(err) {
					console.log(err.errno + " : " + err.code + " : " + err.message);
					deferred.resolve("erreur");
				} else {
					deferred.resolve(this.changes);
				}
			});
		} else if(type=="delete") {
			//Traitement pour les delete
			// Retourne un objet différé contenant soit le nombre de lignes modifiées, soit un objet erreur
			console.log("Delete");
			this.bdd.run(sql, function(err){
				if(err) {
					console.log(err.errno + " : " + err.code + " : " + err.message);
					deferred.resolve("erreur");
				} else {
					deferred.resolve(this.changes);
				}
			});
		}
		return deferred;
	}


//Méthode _requeteUpdate
	this._requeteUpdate = function(sql) {
		return this._requete(sql,"update");
	}

	 //Méthode _requeteDelete
	 this._requeteDelete = function(sql) {
	 	return this._requete(sql,"delete");
	 }

	 //Méthode _requeteInsert
	 // exécute ._requete
	 this._requeteInsert = function(sql) {
	 	return this._requete(sql,"insert");
	 }

	 //Méthode _requeteSelect
	 this._requeteSelect = function(sql) {
	 	return this._requete(sql,"select");
	 }

	 //Méthode _selectPresence
	 this._selectPresence = function(sql) {
		//Variable différée
		var deferred = $.Deferred();
		console.log("SELECT-PRESENCE");		
		this.bdd.each(sql, function(err, row) {},function(err, nbRow){
			if(err) {
				console.log(err.errno + " : " + err.code + " : " + err.message);
				deferred.resolve("erreur");
			}
			else {
				if(nbRow==0) {deferred.resolve(false);}
				else {deferred.resolve(true);}
			}
		});
		return deferred;
	}
}

