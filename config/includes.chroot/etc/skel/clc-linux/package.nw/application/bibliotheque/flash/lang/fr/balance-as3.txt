p1=La balance
p2=Cliquer sur les paquets pour les peser. \rCalculer l'écart entre le paquet le plus lourd et le plus léger.
p7=Cliquer sur les paquets pour les peser. \rCalculer la masse totale de ces paquets.
p3=Masse totale de ces paquets : $var g
p4=Calcule l'écart entre le plus lourd et le plus léger : $var g
p5=Masse totale de ces paquets : $var kg
p6=Calcule l'écart entre le plus lourd et le plus léger : $var kg
