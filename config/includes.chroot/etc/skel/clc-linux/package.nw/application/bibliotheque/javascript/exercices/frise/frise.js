var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.frise = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre=[1,2,3];
var aDonnees,frise,borneInf,pasPixel,texteMobile,repereMobile,repEleve,soluce,gererClavier;// prenom, annee de naissance, age, annee anniversaire


// Définir les options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:5,
        totalEssai:1,
        tempsExo:0,
        niveau:1
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
    if(exo.options.totalQuestion>10)
            exo.options.totalQuestion = 10;
};

// Référencer les ressources de l'exercice (textes, image, son)
// exo.oRessources peut être soit un objet soit une fonction qui renvoie un objet
exo.oRessources = { 
    txt : "frise/textes/frise_fr.json",
    frise:"frise/images/frise.png",
    repere:"frise/images/repere.png",
    repereM:"frise/images/repere-mobile.png",
    berceau:"frise/images/berceau.png",
    berceauR:"frise/images/berceau-rouge.png",
    gateau:"frise/images/gateau.png",
    gateauR:"frise/images/gateau-rouge.png",
    illustration : "frise/images/illustration.jpg"
};

// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
    // niveau 1 trouver annee anniversaire, age multiple de 10
    // niveau 2 trouver annee naissance, age multiple de 10
    // niveau 3 trouver annee anniversaire, age quelconque
    // niveau 4 trouver annee naissance, age quelconque
    aDonnees = [];
    var aPrenom = ["Mina","Nathan","Lola","Lucas","Chloé","Léo","Lucie","Kevin","Léa","Mehdi"];
    util.shuffleArray(aPrenom);
    var i,count,prenom,borneInf,borneSup,age,annee;
    if(exo.options.niveau == 1  || exo.options.niveau == 2){
        var aAges = [10,20,30,40,50,60,40,70,50,80];
        count=0;
        for( i=0; i<10; i++ ){
            age = aAges[i];
            borneInf = age<50 ? 1970 : 1920;
            borneSup = 2020;
            annee = borneInf+10 + Math.floor(Math.random()*(borneSup-10-borneInf+1));
            prenom = aPrenom[i];
            if(annee+age<2030 && util.find([prenom,annee,age,annee+age],aDonnees)<0){
                aDonnees[i]=[prenom,annee,age,annee+age];
            }
            else if(count<1000) {
                i--;
            }
            count++;
        }
    }
    else if(exo.options.niveau == 3  || exo.options.niveau == 4){
        aNaissances = [];
        count=0;
        for( i=0; i<10; i++ ){
            age = 11 + Math.floor(Math.random()*(79-11+1));
            borneInf = age<50 ? 1970 : 1920;
            borneSup = 2020;
            annee = borneInf+10 + Math.floor(Math.random()*(borneSup-10-borneInf+1));
            prenom = aPrenom[i];
            if(age%10 !== 0 && annee+age<2030 && util.find([prenom, annee,age,annee+age],aDonnees)<0){
                aDonnees[i]=[prenom,annee,age,annee+age];
            }
            else if(count<1000) {
                i--;
            }
            count++;
        }
    }
};

//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    exo.keyboard.config({
        numeric : "disabled",
        arrow : "enabled",
        large : "enabled"
    });
    // les données propres à chaque question
    var q = exo.indiceQuestion;
    var prenom = aDonnees[q][0];
    var naissance = aDonnees[q][1];
    var age = aDonnees[q][2];
    var anniversaire = aDonnees[q][3];
    var gradPixel = 60; //valeur en pixels d'une graduation
    var borneSup = 2030;
    pasPixel = age < 50 ? 12 : 6; // valeur en pixels d'une année
    borneInf = age < 50 ? 1980 : 1930;
    repEleve = 0;
    var texteConsigne,texteFixe,xFixe,xMobile,imageFixe,imageMobile,dateFixe,dateMobile;
    if(exo.options.niveau == 1 || exo.options.niveau == 3 ){
        if(String("Mina,Lola,Chloé,Lucie,Léa").indexOf(prenom)>-1){
            texteConsigne = prenom +" est née en "+naissance+"<br>Place le repère qui marque l'année de ses "+age+" ans.";
        }
        else {
            texteConsigne = prenom +" est né en "+naissance+"<br>Place le repère qui marque l'année de ses "+age+" ans.";
        }
        texteFixe = "Naissance de <br>"+prenom;
        xFixe = (naissance-borneInf)*pasPixel;
        texteMobile = prenom+" a<br>"+age+" ans";
        xMobile = 607;
        imageFixe = disp.createImageSprite(exo,"berceau");
        imageMobile = disp.createImageSprite(exo,"gateau");
        dateFixe = naissance;
        dateMobile = age < 50 ? 2031 : 2032;
        soluce = anniversaire;
        xFrise = 20;
    }
    else if(exo.options.niveau == 2 || exo.options.niveau == 4){
        texteConsigne = prenom +" a "+age+" ans en "+anniversaire+"<br>Place le repère qui marque l'année de sa naissance.";
        texteFixe = prenom+" a<br>"+age+" ans";
        xFixe = ((anniversaire)-borneInf)*pasPixel;
        texteMobile = "Naissance de<br>"+prenom;
        xMobile = 49-pasPixel-54.5;
        imageMobile = disp.createImageSprite(exo,"berceau");
        imageFixe = disp.createImageSprite(exo,"gateau");
        dateFixe = anniversaire;
        dateMobile = age < 50 ? 1979 : 1929;
        soluce = naissance;
        xFrise = 25;
    }

    // La consigne
    texteConsigne+="<br><small>( Utilise les flèches \"gauche\" et \"droite\" du clavier pour déplacer le repère. )</small>";
    var labelConsigne = disp.createTextLabel(texteConsigne);
    labelConsigne.children("small").css({fontSize:14,lineHeight:'28px'});
    exo.blocAnimation.append(labelConsigne);
    labelConsigne.css({width:450,fontSize:18,fontWeight:200,padding:10,textAlign:"center",background:"#9c27b0",color:"#fff"});
    labelConsigne.css({left:(735-labelConsigne.width())/2,top:20});
    
    // la frise
    frise = disp.createImageSprite(exo,"frise");
    exo.blocAnimation.append(frise);
    frise.css({left:xFrise,top:260});
    var gradAnnee = age < 50 ? 5 : 10;
    for(var i=0; i<= 10; i++){
        var label = disp.createTextLabel(borneInf+i*gradAnnee);
        frise.append(label);
        label.css({fontSize:14,textAlign:"center"});
        label.css({top:45,left:50+i*gradPixel-label.width()/2});
    }

    // le repere fixe
    var repereFixe = disp.createImageSprite(exo,"repere");
    frise.append(repereFixe);
    repereFixe.css({top:-125,left:xFixe});
    
    repereFixe.append(imageFixe);
    imageFixe.css({left:25,top:5});
    var labelFixeA = disp.createTextLabel(texteFixe);
    repereFixe.append(labelFixeA);
    labelFixeA.css({width:100,fontSize:14,textAlign:"center",top:60,left:0});
    
    var labelFixeB = disp.createTextLabel(""+dateFixe);
    repereFixe.append(labelFixeB);
    labelFixeB.css({width:100,fontSize:18,fontWeight:"bold",textAlign:"center",top:90,left:0});

    // le repere mobile
    repereMobile = disp.createImageSprite(exo,"repereM");
    frise.append(repereMobile);
    repereMobile.css({top:-130,left:xMobile});
    repereMobile.append(imageMobile);
    imageMobile.css({left:25,top:10});
    var labelMobileA = disp.createTextLabel(texteMobile);
    repereMobile.append(labelMobileA);
    labelMobileA.css({width:109,fontSize:14,textAlign:"center",top:67,left:0});
    var labelMobileB = disp.createTextLabel("????");
    repereMobile.append(labelMobileB);
    labelMobileB.css({width:109,fontSize:18,fontWeight:"bold",textAlign:"center",top:97,left:0});

    // Pour que gererClavier ne soit lié qu'une seule fois aux evenements clavier
    // dans le cas ou on recommence l'exo sans recharger la page
    $(document).off("keydown.clc");
    // la gestion du clavier
    gererClavier = function(e){
        //droite
        if(e.which == 39 && repereMobile.position().left<650){
            repereMobile.css({left:"+="+pasPixel+"px"});
            dateMobile++;
            labelMobileB.text(dateMobile);
            repEleve = dateMobile;
        }
        //gauche
        else if(e.which == 37 && repereMobile.position().left>-50){
            repereMobile.css({left:"-="+pasPixel+"px"});
            dateMobile--;
            labelMobileB.text(dateMobile);
            repEleve = dateMobile;
        }
    };

    $(document).on("keydown.clc",gererClavier);
};

// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {
    exo.blocInfo.css({top:330});
    if(repEleve === 0){
        return "rien";
    }
    else if(repEleve == soluce) {
        $(document).off("keydown.clc",gererClavier);
        return "juste";
    }
    else {
        $(document).off("keydown.clc",gererClavier);
        return "faux";
    }
    
};

// Correction (peut rester vide)
exo.corriger = function() {
    shake(repereMobile,6,10);
    function shake(elem,dX,n){
        if( n > 0 ){
            elem.transition({x:dX},50,"linear",function(){
                dX=-dX;
                n--;
                shake(elem,dX,n);
            });
        }
        else if (n===0){
            elem.transition({x:0},50, function(){
                elem.transition({opacity:0.2},function(){
                    var repereCorrection = disp.createImageSprite(exo,"repere");
                    frise.append(repereCorrection);
                    var xCorrection = 50+(soluce-borneInf)*pasPixel - repereCorrection.width()/2;
                    repereCorrection.css({top:-125,left:xCorrection});
                    var imageCorrection;
                    if(exo.options.niveau == 2 || exo.options.niveau == 4){
                        imageCorrection = disp.createImageSprite(exo,"berceauR");
                    }
                    else if(exo.options.niveau == 1 || exo.options.niveau == 3){
                        imageCorrection = disp.createImageSprite(exo,"gateauR");
                    }
                    repereCorrection.append(imageCorrection);
                    imageCorrection.css({left:25,top:5});
                    var labelMobileA = disp.createTextLabel(texteMobile);
                    repereCorrection.append(labelMobileA);
                    labelMobileA.css({width:100,fontSize:14,textAlign:"center",top:60,left:0});
                    var labelMobileB = disp.createTextLabel(soluce);
                    repereCorrection.append(labelMobileB);
                    labelMobileB.css({width:100,fontSize:18,fontWeight:"bold",textAlign:"center",top:90,left:0});
                });
            });
        }
    }
};

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle;

    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:32,
        taille:2,
        nom:"totalQuestion",
        texte:exo.txt.opt1,
        texteApres:"(Maximum 10)"
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"totalEssai",
        texte:exo.txt.opt2,
        aLabel:exo.txt.label2,
        aValeur:[1,2]
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:48,
        taille:3,
        nom:"tempsQuestion",
        texte:exo.txt.opt3
    });
    exo.blocParametre.append(controle);

    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"niveau",
        texte:exo.txt.opt4,
        aLabel:exo.txt.label4,
        aValeur:[1,2,3,4]
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));