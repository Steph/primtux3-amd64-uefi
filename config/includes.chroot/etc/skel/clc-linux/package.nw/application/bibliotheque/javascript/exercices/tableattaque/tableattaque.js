var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.modele par clc.nom-de-votre-exercice
clc.tableattaque = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci

// Déclarer ici les variables globales à l'exercice.

var repEleve, soluce, flash, fondrouge, champReponse, indexParkingFaux, indexParkingJuste, colGauche, colDroite, arNombre , arInvader, arTimeout, arReqAnim;

// Chargement des fichiers ressource de l'exercice (image, son)

exo.oRessources = {
    illustration : "tableattaque/images/illustration.png",
    txt:"tableattaque/textes/tableattaque_fr.json",
    fond : "tableattaque/images/fond.png",
    fondrouge : "tableattaque/images/fond-rouge.png",
    invader : "tableattaque/images/invader.png",
    redinvader : "tableattaque/images/redinvader.png",
    greeninvader : "tableattaque/images/greeninvader.png",
    flash : "tableattaque/images/flash.png"
};

// Options par défaut de l'exercice

exo.creerOptions = function() {
    var optionsParDefaut = {
        exoName:"table-attaque",
        totalQuestion:1,
        tentative:10,
        tempsExo:0,
        plageA:"2-9",
        plageB:"2-9",
        operateur:[10],
        defilement:3
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
    exo.options.totalTentative = exo.options.tentative;// pour compatiblite avec exo flash
};

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
    var arNombreA = util.getArrayNombre(String(exo.options.plageA));
    util.shuffleArray(arNombreA);
    var arNombreB = util.getArrayNombre(String(exo.options.plageB));
    util.shuffleArray(arNombreB);
    var i,j;
    arNombre=[];
    for (i=0;i<arNombreA.length;i++) {
        for (j=0;j<arNombreB.length;j++) {
            if (util.find([arNombreA[i],arNombreB[j]],arNombre) == -1) {
                arNombre.push([arNombreA[i],arNombreB[j]]);
            }
        }
    }
    util.shuffleArray(arNombre);
    var dif=arNombre.length - (exo.options.totalTentative+1);
    if(dif < 0) {
        //pas assez de couples on complete
        for(i=0;i<-dif;i++) {
            arNombre.push(arNombre[i%arNombre.length]);
        }
    }
    else if (dif > 0) {
        //trop de couples on retire
        arNombre.splice(exo.options.totaltotalTentative,dif);
    }
    console.log(arNombre);
};

// Création de la page titre

exo.creerPageTitre = function() {
    console.log(exo.txt);
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
    exo.keyboard.config({
        arrow:"disabled"
    });
    // on cache le bouton valider;
    exo.btnValider.hide();
    exo.blocScore.css({color:"#fff"});
    // le fond noir
    var fond = disp.createImageSprite(exo,"fond");
    fondrouge = disp.createImageSprite(exo,"fondrouge");
    exo.blocAnimation.append(fond);
    exo.blocAnimation.append(fondrouge);
    fondrouge.hide();
    
    fond.css({left:1,top:1});
    // le conteneur qui accueillera les petis envahisseurs rouges en cas de réponse fausse
    colGauche = disp.createEmptySprite();
    colGauche.css({
        width:50,
        height:430,
        left:5,
        top:20
    });
    exo.blocAnimation.append(colGauche);
    // le conteneur qui accueillera les petis envahisseurs verts en cas de réponse exacte
    colDroite = disp.createEmptySprite();
    colDroite.css({
        width:50,
        height:430,
        left:680,
        top:20
    });
    exo.blocAnimation.append(colDroite);
    //
    var cadence, vitesse;
    if(exo.options.defilement === 3 ){
        cadence = 3000;
        vitesse = 40000;
    }
    else if(exo.options.defilement === 4 ){
        cadence = 2800;
        vitesse = 30000;
    }
    else if (exo.options.defilement === 6 ){
        cadence = 2600;
        vitesse = 20000;
    }
    else if (exo.options.defilement === 8 ){
        cadence = 2400;
        vitesse = 15000;
    }
    
    indexParkingJuste = 0;
    indexParkingFaux = 0;
    // les envahisseurs
    arInvader = [];
    var arLeft = [30,130,230,330,430,530,630];//position de départ des envahisseurs
    util.shuffleArray(arLeft);
    
    for ( var i = 0; i < exo.options.totalTentative; i++ ) {
        var indiceOperateur = i%exo.options.operateur.length;
        creerInvader(i,exo.options.operateur[indiceOperateur]);
    }
    
     
    arTimeout = [];
    for ( var i = 0; i < exo.options.totalTentative; i++ ) {
        (function delayedAnimation(index){
            arTimeout.push(exo.setTimeout(function(){
                arInvader[index].css( { visibility:'visible' } );
                //anim.translate( arInvader[index], 0, 370, 0, vitesse, "linear", arreterInvaderFaux );
                arInvader[index].transition({y:290},vitesse,"linear",arreterInvaderFaux);
            },
            index*cadence));
        }(i));
    }
    // le champ réponse et le flash rouge qui se trouve derrière
    flash = disp.createImageSprite(exo,"flash");
    exo.blocAnimation.append(flash);
    champReponse = disp.createTextField(exo,5);
    champReponse.focus();
    exo.blocAnimation.append(champReponse);
    var posX = (735-champReponse.outerWidth())/2;
    champReponse.css({left:posX,top:400});
    
    flash.position({
        my:'center center',
        at:'center center',
        of:champReponse
    });
    flash.hide();
    champReponse.on("keydown",gestionClavier);
    //
    // comportement de la touche "Valider" du clavier virtuel.
    var toucheValider = exo.keyboard.baseContainer.find('.key.large');
    toucheValider.data("valeur","exo");// permet de redefinir le comportement de la touche "Valider"
    toucheValider.on("touchstart", gestionValiderClavierVirtuel);
    //
    //
    function creerInvader(index,sOperateur){
		var invader = disp.createImageSprite(exo,"invader");
        //var invader = disp.createImageSprite(exo,"invaderc");
        exo.blocAnimation.append(invader);
        var texte,valeur;
        console.log(index);
        var nA = Big(arNombre[index][0]);
        var nB = Big(arNombre[index][1]);
        if (sOperateur == 1) {
            texte = nA.toFrench() + " + " + nB.toFrench();
            valeur= nA.plus(nB);
        } else if (sOperateur ==  10) {
            //texte = arNombre[index][0] + " x " + arNombre[index][1];
            texte = nA.toFrench() + " x " + nB.toFrench();
            //valeur= arNombre[index][0] * arNombre[index][1];
            valeur = nA.times(nB);
        } else if (sOperateur ==  1000) {
            texte = nA.times(nB).toFrench() +" : "+nB.toFrench();
            valeur= nA;
        } else if (sOperateur ==  100) {
            var max = Big(Math.max(arNombre[index][0],arNombre[index][1]));
            var min = Big(Math.min(arNombre[index][0],arNombre[index][1]));
            texte = max.toFrench() + " - " +min.toFrench();
            valeur= max.minus(min);
        }
        
        var labelInvader = disp.createTextLabel(texte);
        invader.data({
            index:index,
            valeur:valeur
        });
        labelInvader.css({
            color:"#fff",
            fontSize:18,
            fontWeight:'bold',
            width:100,
            textAlign:'center'
        });
        invader.append(labelInvader);
        labelInvader.position({
            my:'center top',
            at:'center bottom',
            of:invader
        });
        invader.css({
            visibility:'hidden',
            top:50,
            left:arLeft[index%7]
        });
        
        arInvader.push(invader);
        return invader;
    }
    
    
    function gestionClavier(e) {
        //console.log(e)
        if(e.which == 13) {
            if(champReponse.val()==="") {
                return false;
            }
            exo.poursuivreQuestion();
            champReponse.val("");
            return false;
        }
    }
    
    function gestionValiderClavierVirtuel() {
            if(champReponse.val()==="") {
                return false;
            }
            exo.poursuivreQuestion();
            champReponse.val("");
            return false;
    }
    
    function arreterInvaderFaux(e) {
        var elem = $(this);
        elem.effect("pulsate",100,function(){
            elem.remove();
            var spriteRouge = disp.createImageSprite(exo,"redinvader");
            var n = indexParkingFaux;
            var posX = parseInt(n/20)*20;
            var posY = 390 - (n%20)*20;
            colGauche.append(spriteRouge);
            spriteRouge.css({left:posX,top:posY});
            spriteRouge.css({width:'50%'});
            indexParkingFaux++;
        });
        exo.indiceTentative++;
        //console.log(elem.css('y'),exo.indiceTentative);
        //console.log()
        if(exo.indiceTentative == exo.options.tentative ) {
            for(var i = 0 ; i < arInvader.length; i++ ) {
                clearTimeout(arTimeout[i]);
                //anim.translateStop(arInvader[i]);
                arInvader[i].transitionStop();
                arInvader[i].transition({opacity:0},1000,"linear",function(){$(this).remove();});
            }
            exo.poursuivreQuestion();
        }
    }
    
};

// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
    //console.log("exo.indiceTentative",exo.indiceTentative);
    if( exo.indiceTentative === 0 ) {
        console.log("terminé")
        for(var i = 0, max = arInvader.length ; i < max; i++ ) {
            clearTimeout(arTimeout[i]);
            arInvader[i].transitionStop();
        }
    }
    var resEval = "";
    if(repEleve === "") {
        resEval = "rien";
    } 
    else {
        repEleve = Big(util.strToNum(champReponse.val()));
        var max=arInvader.length;
        for ( var index=0;index<max;index++) {
            if ( arInvader[index].data("valeur") !== undefined && arInvader[index].data("valeur").eq(repEleve) && arInvader[index].css( 'visibility') == 'visible' ) {
                resEval = "juste";
                arInvader[index].data("valeur",null);
                arInvader[index].transitionStop(true);
                arreterInvaderJuste(arInvader[index]);
                break;
            } else {
                resEval = "faux";
            }
        }
    }
    if (resEval == "faux") {
        
        
        flash.fadeIn(100,function(){flash.fadeOut(100); fondrouge.fadeIn(100,function(){fondrouge.fadeOut(100);})});
        
        
        var spriteRouge = disp.createImageSprite(exo,"redinvader");
        var n = indexParkingFaux;
        var posX = parseInt(n/20)*20;
        var posY = 410 - (n%20)*20;
        colGauche.append(spriteRouge);
        spriteRouge.css({left:posX,top:posY});
        indexParkingFaux++;
    }
    return resEval;
};

// Correction (peut rester vide)

exo.corriger = function() {
    //console.log("corriger");
    
};

exo.creerPageParametre = function() {
	var controle;
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:48,
        taille:4,
        nom:"tentative",
        texte:"Nombre de calculs : "
    });
    exo.blocParametre.append(controle);
    
    controle = disp.createOptControl(exo,{
        type:"text",
        nom:"plageA",
        texte:"Plage nombre A : "
    });
    exo.blocParametre.append(controle);
    
    controle = disp.createOptControl(exo,{
        type:"text",
        nom:"plageB",
        texte:"Plage nombre B : "
    });
    exo.blocParametre.append(controle);
    
    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"defilement",
        texte:"Vitesse : ",
        aLabel:["lente","normale","rapide","très rapide"],
        aValeur:[3,4,6,8]
    });
    exo.blocParametre.append(controle);
    
    controle = disp.createOptControl(exo,{
        type:"checkbox",
        nom:"operateur",
        texte:"Type d'opération : ",
        aValeur:[1,10,100,1000],
        aLabel:["addition","multiplication","soustraction","division"]
    });
    exo.blocParametre.append(controle);
};

function arreterInvaderJuste(invader) {
    invader.transition({opacity:0},1000,"linear",function(){$(this).remove();});
    
    var spriteVert = disp.createImageSprite(exo,"greeninvader");
    var n = indexParkingJuste;
    var posX = 28 - parseInt(n/20)*20;
    var posY = 390 - (n%20)*20;
    
    colDroite.append(spriteVert);
    spriteVert.css({left:posX,top:posY});
    spriteVert.show();
    indexParkingJuste++;   
}

function effacer(){
    //console.log("effacer");
}


/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));