p1=La frise
p2=Place un repère sur une frise en utilisant les flèches du clavier.
p3=Utilise la flèche gauche ou la flèche droite du clavier pour déplacer ce repère.
p4=Emilie est née en $date. \r\rPlace le repère qui marque l'année de ses $duree ans.
p5=Naissance d'Emilie
p6=Emilie a $duree ans
