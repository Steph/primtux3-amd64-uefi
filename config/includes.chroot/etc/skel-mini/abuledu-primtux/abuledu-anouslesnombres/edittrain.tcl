############################################################################
# Copyright (C) 2003 Eric Seigne
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : editeur.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: edittrain.tcl,v 1.1.1.1 2004/04/16 11:45:47 erics Exp $
# @author     David Lucardi
# @project
# @copyright  Eric Seigne
#
#
#########################################################################
#!/bin/sh
#Editeur.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

##################################"sourcing
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source fonts.tcl
source path.tcl
source utils.tcl
source eval.tcl
source msg.tcl

inithome
initlog $plateforme $ident
changehome

set fichier [file join $basedir aide index.htm]
bind . <F1> "exec $progaide \042$fichier\042 &"

####################### Creation des menus
#menu .menu -tearoff 0
#menu .menu.fichier -tearoff 0
#.menu add cascade -label "Activit�" -menu .menu.fichier
#.menu.fichier add command -label "Calapa" -command "interface calapa"
#. configure -menu .menu
#######################################################################"
global arg tabcoulwagon tabcouldispo
set arg [lindex $argv 0]
array set tabcoulwagon {0 8 1 8 2 8 3 8 4 8}
array set tabcouldispo {0 #FF0303 1 #13FF13 2 #0000FF 3 #FFFF00 4 #FF00FF 5 #11CFFF 6 #B7770D 7 #BFBFBF}

. configure -background white
frame .geneframe -background white -height 300 -width 400
pack .geneframe -side left

#variables
#set arg train
#################################################g�n�ration interface
proc interface { what} {
global sysFont listanimaux listhabitat arg compteligne typexo

set compteligne 5
set ext .gif
for {set i 0} {$i <= 8} {incr i 1} {
set ind 0
image create photo wagon$i$ind -file [file join images wagon$i$ind$ext]
set ind 1
image create photo wagon$i$ind -file [file join images wagon$i$ind$ext]
}

catch {destroy .geneframe}

catch {destroy .leftframe}

frame .leftframe -background white -height 480 -width 100 
pack .leftframe -side left -anchor n

frame .geneframe -background white -height 300 -width 200
pack .geneframe -side left -anchor n
label .leftframe.lignevert1 -bg red -text "" -font {Arial 1}
pack .leftframe.lignevert1 -side right -expand 1 -fill y

######################leftframe.frame1
frame .leftframe.frame1 -background white -width 100 
pack .leftframe.frame1 -side top 

label .leftframe.frame1.lab1 -foreground red -bg white -text [mc $arg] -font $sysFont(l)
pack .leftframe.frame1.lab1 -side top -fill x -pady 5

label .leftframe.frame1.lab2 -bg blue -foreground white -text [mc {Scenario}] -font $sysFont(l)
pack .leftframe.frame1.lab2 -side top -fill x -expand 1 -pady 5

listbox .leftframe.frame1.listsce -yscrollcommand ".leftframe.frame1.scrollpage set" -width 15 -height 6
scrollbar .leftframe.frame1.scrollpage -command ".leftframe.frame1.listsce yview" -width 7
pack .leftframe.frame1.listsce .leftframe.frame1.scrollpage -side left -fill y -expand 1 -pady 10
bind .leftframe.frame1.listsce <ButtonRelease-1> "changelistsce %x %y"

############################leftframe.frame2
frame .leftframe.frame2 -background white -width 100 
pack .leftframe.frame2 -side top 
button .leftframe.frame2.but0 -text [mc {Nouveau}] -command "additem" -activebackground white
pack .leftframe.frame2.but0 -pady 5 -side top
button .leftframe.frame2.but1 -text [mc {Supprimer}] -command "delitem" -activebackground white
pack .leftframe.frame2.but1 -pady 5 -side top


########################leftframe.frame3
frame .leftframe.frame3 -background white -width 100
pack .leftframe.frame3 -side top 
label .leftframe.frame3.lab0 -foreground white -bg blue -text [mc {    Type    }] -font $sysFont(l)
pack .leftframe.frame3.lab0 -pady 5 -side top -fill x -expand 1

label .leftframe.frame3.lbcommande -bg white -text [mc {Commande des wagons :}] -font $sysFont(s)
pack .leftframe.frame3.lbcommande -side top
radiobutton .leftframe.frame3.rbcommande0 -text [mc {Un � un ou ecriture usuelle}] -bg white  -variable typeexo -value 0 -font $sysFont(s)
pack .leftframe.frame3.rbcommande0 -side top
radiobutton .leftframe.frame3.rbcommande1 -text [mc {Ecriture usuelle ou complexe}] -bg white  -variable typeexo -value 1 -font $sysFont(s)
pack .leftframe.frame3.rbcommande1 -side top


button .leftframe.frame3.but0 -text [mc {ok}] -command "fin" -activebackground white
pack .leftframe.frame3.but0 -side bottom

##########################################################################################################
set tmp 3
for {set i 0} {$i < $compteligne} {incr i 1} {
  label .geneframe.labll$i -bg red -text "" -font {Arial 1}
  grid .geneframe.labll$i -column 0 -row $tmp -columnspan 6 -sticky "w e"
  incr tmp 2 

}

#############colonne wagons
  label .geneframe.lab0_0 -bg white -text [mc {Wagons}] -font $sysFont(l)
  grid .geneframe.lab0_0 -column 0 -row 0 -sticky "w e"
  label .geneframe.labl1 -bg red -text "" -font {Arial 1}
  grid .geneframe.labl1 -column 0 -row 1 -columnspan 6 -sticky "w e"
set tmp 2
for {set i 0} {$i < $compteligne} {incr i 1} {
  menubutton .geneframe.btnwagon$i -image wagon80 -direction right -menu .geneframe.btnwagon$i.coul -relief raised -activebackground grey 
  menu .geneframe.btnwagon$i.coul -tearoff 0

 bind .geneframe.btnwagon$i <1>  "menuwagon $i"

 grid .geneframe.btnwagon$i -column 0 -row $tmp
  incr tmp 2
}

################colonne nombre
  label .geneframe.lab3_0 -bg #78F8FF -text [mc {Nombre (Min Max)}] -font $sysFont(l)
  grid .geneframe.lab3_0 -column 1 -row 0 -sticky "w e" -columnspan 2

set tmp 2
for {set i 0} {$i < $compteligne} {incr i 1} {
  scale .geneframe.scale$i -orient horizontal -length 80 -from 0 -to 20 -tickinterval 20 -borderwidth 0 -bg #78F8FF -activebackground #78F8FF -highlightcolor #78F8FF -highlightbackground #78F8FF -font $sysFont(s)
  grid .geneframe.scale$i -column 1 -row $tmp -sticky "w e"
  bind .geneframe.scale$i <ButtonRelease-1> "changescalemin $i"
  scale .geneframe.scale$i$i -orient horizontal -length 80 -from 0 -to 20 -tickinterval 20 -borderwidth 0 -bg #78F8FF -activebackground #78F8FF -highlightcolor #78F8FF -highlightbackground #78F8FF -font $sysFont(s)
  grid .geneframe.scale$i$i -column 2 -row $tmp -sticky "w e"
  bind .geneframe.scale$i$i <ButtonRelease-1> "changescalemax $i" 
  incr tmp 2
}


###############colonne D�j� present
  label .geneframe.lab1_0 -bg white -text [mc {Deja presents}] -font $sysFont(l)
  grid .geneframe.lab1_0 -column 3 -row 0 -columnspan 2 -sticky "w e"
set tmp 2
for {set i 0} {$i < $compteligne} {incr i 1} {
  checkbutton .geneframe.rb$i -text [mc {Deja presents}] -bg white -variable dejap$i 
  grid .geneframe.rb$i -column 3 -row $tmp  -sticky "w e"
  checkbutton .geneframe.rbb$i -text [mc {Pres des bornes}] -bg white -variable borne$i  
  grid .geneframe.rbb$i -column 4 -row $tmp  -sticky "w e"
incr tmp 2
}

#################################sp�cifiques trains

  #label .geneframe.lbcommande -bg grey -text [mc {Commande des wagons :}] -font $sysFont(s)
  #grid .geneframe.lbcommande  -column 0 -row $tmp -sticky "w e n s"
  #radiobutton .geneframe.rbcommande0 -text [mc {Un � un ou ecriture usuelle}] -bg grey  -variable typeexo -value 0 -font $sysFont(s)
  #grid .geneframe.rbcommande0 -column 1 -row $tmp  -sticky "w e n s" -columnspan 2
  #radiobutton .geneframe.rbcommande1 -text [mc {Ecriture usuelle ou complexe}] -bg grey  -variable typeexo -value 1 -font $sysFont(s)
  #grid .geneframe.rbcommande1 -column 3 -row $tmp  -sticky "w e n s" -columnspan 2
  #incr tmp 2
  #label .geneframe.labll6 -bg red -text "" -font {Arial 1}
  #grid .geneframe.labll6 -column 0 -row $tmp -columnspan 6 -sticky "w e"
  #incr tmp

  checkbutton .geneframe.ckfenetre -bg grey  -variable commandefenetre -text [mc {Commande fenetres}] -command "changefenetre"
  grid .geneframe.ckfenetre -column 0 -row $tmp  -sticky "w n s e" 
  checkbutton .geneframe.ckcorrection -bg grey -variable correction -text [mc {Correction commande}]
  grid .geneframe.ckcorrection -column 1 -row $tmp  -sticky "w n e s" -columnspan 2 
  checkbutton .geneframe.ckaccroche -bg grey  -variable pauseauto -text [mc {Accrochage automatique des wagons}]
  grid .geneframe.ckaccroche -column 3 -row $tmp  -sticky "w n s e" -columnspan 2
##################################################"
 charge 0
}


####################################################################"""
proc charge {index} {
global listdata activelist indexpage totalpage arg compteligne tabcoulwagon Home
variable typeexo
variable correction
variable commandefenetre
variable pauseauto


array unset tabcoulwagon
array set tabcoulwagon {0 8 1 8 2 8 3 8 4 8}


set ext .conf
for {set i 0} {$i < $compteligne} {incr i 1} {
variable dejap$i
variable borne$i
set dejap$i 0
set borne$i 0
.geneframe.scale$i set 0
.geneframe.scale$i$i set 0
}

set f [open [file join $Home reglages $arg$ext] "r"]
set listdata [gets $f]
close $f


set totalpage [llength $listdata] 
set indexpage $index
	.leftframe.frame1.listsce delete 0 end
	for {set k 0} {$k < [llength $listdata]} {incr k 1} {
	.leftframe.frame1.listsce insert end [lindex [lindex [lindex $listdata $k] 0] 0]
	}
set activelist [lindex $listdata $indexpage]
.leftframe.frame1.listsce selection set $indexpage
	set typeexo [lindex [lindex $activelist 0] 1]
	set correction [lindex [lindex $activelist 0] 2]
	set commandefenetre [lindex [lindex $activelist 0] 3]
	set pauseauto [lindex [lindex $activelist 0] 4]
for {set i 0} {$i < $compteligne} {incr i 1} {
 .geneframe.btnwagon$i configure -image wagon8$commandefenetre   
}

################################################################################modifier

	for {set i 1} {$i < [llength $activelist]} {incr i 1} {
      .geneframe.btnwagon[expr $i - 1] configure -image wagon[lindex [lindex $activelist $i] 0]$commandefenetre
	.geneframe.scale[expr $i - 1][expr $i - 1] set [lindex [lindex $activelist $i] 2]
	.geneframe.scale[expr $i - 1] set [lindex [lindex $activelist $i] 1]
	set dejap[expr $i - 1] [lindex [lindex $activelist $i] 3]
	set borne[expr $i - 1] [lindex [lindex $activelist $i] 4]
	set tabcoulwagon([expr $i - 1]) [lindex [lindex $activelist $i] 0]
	.geneframe.scale[expr $i - 1] configure -to [lindex [lindex $activelist $i] 2] -tickinterval [lindex [lindex $activelist $i] 2]
	}
######################################################################
}



###################################################################################"
proc enregistre_sce {} {
global indexpage listdata activelist totalpage arg compteligne tabcoulwagon Home
set ext .conf
variable typeexo
variable correction
variable commandefenetre
variable pauseauto
for {set i 0} {$i < $compteligne} {incr i 1} {
variable dejap$i
variable borne$i
}


set activelist \173[.leftframe.frame1.listsce get $indexpage]\040$typeexo\040$correction\040$commandefenetre\040$pauseauto\175
##########################################################################modifier
foreach item [array names tabcoulwagon] {
if {$tabcoulwagon($item) != 8} {
lappend activelist $tabcoulwagon($item)\040[.geneframe.scale$item get]\040[.geneframe.scale$item$item get]\040[expr \$dejap$item]\040[expr \$borne$item]
}
}
#####################################################################################################"


set listdata [lreplace $listdata $indexpage $indexpage $activelist]
set f [open [file join $Home reglages $arg$ext] "w"]
puts $f $listdata
close $f
}

###################################################################
proc changescalemin {min} {
if {[.geneframe.scale$min get] > [.geneframe.scale$min$min get] } { 
.geneframe.scale$min$min set [.geneframe.scale$min get] 
}
}

proc changescalemax {max} {
global tabcoulwagon
set total 0
	foreach item [array names tabcoulwagon] {
		if {$tabcoulwagon($item) != 8} {
		set total [expr $total + [.geneframe.scale$item$item get]]
		}
	}
if {$total > 30} {
.geneframe.scale$max$max set 0
tk_messageBox -message [mc {30 wagons autorises au total}] -type ok -title [mc {Train}]
}
.geneframe.scale$max configure -to [.geneframe.scale$max$max get]
#.geneframe.scale$max set [.geneframe.scale$max$max get]

if {[.geneframe.scale$max$max get]!=0} {
.geneframe.scale$max configure -tickinterval [expr [.geneframe.scale$max$max get]]
} else {
#.geneframe.scale$max configure -tickinterval 0
}
}

proc changelistsce {x y} {
enregistre_sce
set ind [.leftframe.frame1.listsce index @$x,$y]
charge $ind
}

proc fin {} {
enregistre_sce
destroy .
}

#######################################################################
proc delitem {} {
global listdata indexpage totalpage arg Home
set ext .conf
	if {$totalpage < 2} {
	tk_messageBox -message [mc {Impossible de supprimer la fiche}] -type ok -title [mc $arg]
	return
	}
set response [tk_messageBox -message [format [mc {Voulez-vous vraiment supprimer la fiche %1$s ?}] [lindex [lindex [lindex $listdata $indexpage] 0] 0]] -type yesno -title [mc $arg]]

#set response [tk_messageBox -message "Voulez-vous vraiment supprimer la fiche [lindex [lindex [lindex $listdata $indexpage] 0] 0]?" -type yesno -title $arg]
	if {$response == "yes"} {
	set totalpage [expr $totalpage - 1]
		set listdata [lreplace $listdata $indexpage $indexpage]
     		if {$indexpage > 0} {
     		set indexpage [expr $indexpage - 1]						
		} 
	set f [open [file join $Home reglages $arg$ext] "w"]
	puts $f $listdata
	close $f
	charge $indexpage

	} 

}


proc additem {} {
global listdata indexpage totalpage 
enregistre_sce
catch {destroy .nomobj}
toplevel .nomobj -background grey -width 250 -height 100
wm geometry .nomobj +50+50
frame .nomobj.frame -background grey -width 250 -height 100
pack .nomobj.frame -side top
label .nomobj.frame.labobj -font {Helvetica 10} -text [mc {Nom du scenario :}] -background grey
pack .nomobj.frame.labobj -side top 
entry .nomobj.frame.entobj -font {Helvetica 10} -width 10
pack .nomobj.frame.entobj -side top 
button .nomobj.frame.ok -background gray75 -text [mc {Ok}] -command "verifnomobj"
pack .nomobj.frame.ok -side top -pady 10
}

proc verifnomobj {} {
global nom listdata indexpage totalpage arg tabcoulwagon compteligne Home
set ext .conf
set nom [join [.nomobj.frame.entobj get] ""]
if {$nom !=""} {
set indexpage $totalpage
incr totalpage
lappend listdata \173$nom\0400\0400\0401\0400\175
set f [open [file join $Home reglages $arg$ext] "w"]
puts $f $listdata
close $f
charge $indexpage
}
catch {destroy .nomobj}
}

proc menuwagon {index} {
global tabcoulwagon tabcouldispo
array set tabtmp [array get tabcouldispo]
#.leftframe.frame1.listsce insert end [array get tabcoulwagon]

foreach item [array names tabcoulwagon] {
if {$tabcoulwagon($item) != 8} {
array unset tabtmp $tabcoulwagon($item)
}
}
catch {.geneframe.btnwagon$index.coul delete 0 end}
	foreach coul [array names tabtmp] {
    	.geneframe.btnwagon$index.coul add command -label " " -background $tabtmp($coul) -activebackground Black -command "geremenuwagon $index $coul"
	}
    	.geneframe.btnwagon$index.coul add command -label [mc {Desactive}] -background white -command "geremenuwagon $index 8"

}

proc geremenuwagon {index coul} {
global tabcoulwagon tabcouldispo commandefenetre
set tabcoulwagon($index) $coul
#set indfenetre 0
.geneframe.btnwagon$index configure -image wagon$coul$commandefenetre
if {$coul != 8} {
.geneframe.scale$index$index set 0
.geneframe.scale$index set 0
.geneframe.scale$index configure -to 0
}
}

proc changefenetre {} {
global compteligne tabcoulwagon
variable commandefenetre
for {set i 0} {$i < $compteligne} {incr i 1} {
 .geneframe.btnwagon$i configure -image wagon$tabcoulwagon($i)$commandefenetre   
}
}

interface $arg
