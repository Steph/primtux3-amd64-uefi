############################################################################
# Copyright (C) 2003 ????
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : calapa.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 08/02/2003
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: constellation.tcl,v 1.1.1.1 2004/04/16 11:45:42 erics Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi
#
#
#########################################################################
#!/bin/sh
#calapa.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}
global classe erreurselection erreurhabitat serieincomplete idimg
set erreurselection 0
set erreurhabitat 0
set serieincomplete 0
set classe 0
set idimg 0
#gestion du jeu et des déplacements
proc itemStartDrag {c x y} {
    global lastX lastY sourcecoord flag
    set sourcecoord [$c coords current]
    set lastX [$c canvasx $x]
    set lastY [$c canvasy $y]
    $c raise current
    .bframe.tete configure -image neutre
    }

proc highlight {c image gamma} {
$image configure -gamma $gamma
$c itemconfigure current -image $image
}


proc itemStopDrag {c x y} {
global indens serieencours classe listdata erreurselection 
	if {[lindex [lindex $listdata 0] 1] == "2"} {
		foreach imag [image names] {
		$imag configure -gamma 1.0
	}	}

set tmp [$c itemcget current -image]
set id [lindex [$c gettags current] [lsearch -regexp [$c gettags current] uid*]]
highlight $c $tmp 2.5
	if {$id== "uidens(0)" || [lsearch [$c gettags current] compte] != -1 || ($serieencours!= "" && [string range $id 3 end] != $serieencours)} {
	highlight $c $tmp 0.01
	foreach imag [image names] {
			$imag configure -gamma 1.0
			}

	$c dtag uid$serieencours compte
	set serieencours ""
	set classe 0
	incr erreurselection
      .bframe.tete configure -image mal
	return
	}
$c addtag compte withtag current
incr classe
set serieencours [string range $id 3 end]
}

proc itemDrag {c x y} {
}

proc verif {c lieu} {
global nbens indens serieencours listdata nbcat classe classens erreurselection erreurhabitat serieincomplete user idimg reussite
#classe : classens(1) ou classens(2)
	foreach imag [image names] {
	$imag configure -gamma 1.0
	}
if {$serieencours == ""} {
return
}

if {$serieencours != $lieu} {
incr erreurhabitat
}
if {$classe != [expr \$class$serieencours]} {
incr serieincomplete
}

if {$serieencours != $lieu || $classe != [expr \$class$serieencours]} {
$c dtag uid$serieencours compte
set serieencours ""
set classe 0
.bframe.tete configure -image mal
return
}

if {$serieencours !="" && $classe == [expr \$class$serieencours]} {
	switch $classe {
	2 { set constellation {{0 0} {40 40}} }
	3 { set constellation {{0 0} {20 20} {40 40}} }
	4 { set constellation {{0 0} {40 40} {0 40} {40 0}} }
	5 { set constellation {{0 0} {40 40} {20 20} {0 40} {40 0}} }
	}
	if {$nbcat == 1} {
	set lpos { {180 220} {280 220} {380 220} {180 300} {280 300} {380 300} }

	} else {
	switch $serieencours {
	ens(1) {
	set lpos { {30 220} {130 220} {230 220} {30 300} {130 300} {230 300} }
	}
	ens(2) {
	set lpos { {350 220} {450 220} {550 220} {350 300} {450 300} {550 300} }
	}
	}
	}
set tmpcompt 0
		foreach item [$c find withtag uid$serieencours] {
		if {[lsearch [$c gettags $item] compte] != -1} {
#########################################################################
		set tmp [$c itemcget $item -image]
		image create photo tmpdst$serieencours$idimg
		tmpdst$serieencours$idimg copy $tmp -subsample 2 2 
		$c itemconfigure $item -image tmpdst$serieencours$idimg
		incr idimg
#########################################################################"
		$c coords $item [expr [lindex [lindex $lpos [expr int(\$ind$serieencours/$classe)]] 0] + [lindex [lindex $constellation $tmpcompt] 0]]  [expr [lindex [lindex $lpos [expr int(\$ind$serieencours/$classe)]] 1] + [lindex [lindex $constellation $tmpcompt] 1]]
		incr ind$serieencours
		incr tmpcompt
		$c dtag $item compte
		$c dtag $item drag
		$c dtag $item anim1
		}
		}
set classe 0
.bframe.tete configure -image bien
}
if {$indens(1) == $nbens(1) && $indens(2) == $nbens(2)} {
$c dtag all drag
.bframe.tete configure -image bien
appendeval "\173[mc {Nombre d'erreurs de selection :}]$erreurselection\175\040\173[mc {Nombre d'erreurs d'habitats :}]$erreurhabitat\175\040\173[mc {Erreurs sur mauvais groupement :}]$serieincomplete\175" $user
set reussite [expr $erreurselection + $erreurhabitat +$serieincomplete]
geresuite $c
}
set serieencours ""
}

