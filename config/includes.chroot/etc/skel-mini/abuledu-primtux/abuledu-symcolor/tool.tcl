#!/bin/sh
#kidistb.tcl
#Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}
############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  :
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version $Id: tool.tcl,v 1.2 2006/01/18 14:12:11 david Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#
#
#########################################################################

proc majmenu {c} {
global Home Home_data basedir plateforme progaide baseHome
catch {destroy .wmenu.menu}
menu .wmenu.menu -tearoff 0
    menu .wmenu.menu.fichier -tearoff 0
    .wmenu.menu add cascade -label [mc {Fichier}] -menu .wmenu.menu.fichier
    .wmenu.menu.fichier add command -label [mc {Nouveau}] -command "nouveau $c"
    .wmenu.menu.fichier add command -label [mc {Ouvrir}] -command "charge $c"
    .wmenu.menu.fichier add command -label [mc {Enregistrer}] -command "sauve $c old"
    .wmenu.menu.fichier add command -label [mc {Enregistrer sous}] -command "sauve $c new"
    .wmenu.menu.fichier add command -label [mc {Bilans}] -command "exec wish bilan.tcl 1"
    .wmenu.menu.fichier add sep
    .wmenu.menu.fichier add command -label [mc {Quitter}] -command "destroy ."

   menu .wmenu.menu.images -tearoff 0
    .wmenu.menu add cascade -label [mc {Images}] -menu .wmenu.menu.images
	set ext .kdb
	if {[winfo screenwidth .] == "800"} {
	set ext1 _lr
	} else {
	set ext1 _hr
	}
    	foreach i [lsort [glob [file join  $Home_data *$ext1$ext]]] {
    	set imge [string map {.kdb ""} [file tail $i]]
 .wmenu.menu.images add command -label $imge -command "ouvre $c $imge$ext"
    }

    menu .wmenu.menu.options -tearoff 0
    .wmenu.menu add cascade -label [mc {Options}] -menu .wmenu.menu.options
    

    set ext .msg
menu .wmenu.menu.options.lang -tearoff 0 
.wmenu.menu.options add cascade -label "[mc {Langue}]" -menu .wmenu.menu.options.lang
    foreach i [glob [file join  $basedir msgs *$ext]] {
    set langue [string map {.msg ""} [file tail $i]]
    .wmenu.menu.options.lang add radio -label $langue -variable langue -command "setlang $langue"
    }
####################################
menu .wmenu.menu.options.repconf -tearoff 0 
.wmenu.menu.options add cascade -label [mc {Dossier de travail}] -menu .wmenu.menu.options.repconf
.wmenu.menu.options.repconf add radio -label [mc {Commun}] -variable repertconf -value 1 -command "changehome ; majmenu $c"
.wmenu.menu.options.repconf add radio -label [mc {Individuel}] -variable repertconf -value 0 -command "changehome ; majmenu $c"

if {$plateforme == "windows"} {
.wmenu.menu add command -label [mc {Utilisateur}] -command "setwindowsusername"
}
###################################
    menu .wmenu.menu.aide -tearoff 0
    .wmenu.menu add cascade -label [mc {?}] -menu .wmenu.menu.aide

	if {[file exists [file join $baseHome reglages lang.conf]] == 1} {
	set f [open [file join $baseHome reglages lang.conf] "r"]
  	gets $f lang
  	close $f
	set fich "_index.htm"
	set fich $lang$fich
	set fichier [file join [pwd] aide $fich]
	} else {
	set fichier [file join [pwd] aide fr_index.htm]
	}
  


if {[file exists $fichier] != 1} {
set fichier [file join [pwd] aide fr_index.htm]
}
    
    .wmenu.menu.aide add command -label [mc {Aide}] -command "exec firefox file:$fichier &"
    .wmenu.menu.aide add command -label [mc {A propos ...}] -command "source apropos.tcl"

. configure -menu .wmenu.menu

}

#######################################################################
proc interface {c} {
global canvw canvh bgn bgl
variable repertconf
set suf ".gif"
set widthf 200
if {[winfo screenwidth .] == "800"} {
set suf "_low.gif"
set widthf 140
}
frame .wcorner  -width 30 -height 200
pack .wcorner -side left -anchor n -fill both

#la barre d'outils
frame .wcorner.wtool -width 20 -height 80
pack .wcorner.wtool -side top -anchor n -fill both
button .wcorner.wtool.image -image [image create photo -file [file join sysdata image$suf]] -command "forme $c poly1" -activebackground grey
button .wcorner.wtool.texte -image [image create photo -file [file join sysdata texte$suf]] -command "forme $c poly2" -activebackground grey
button .wcorner.wtool.forme -image [image create photo -file [file join sysdata libre$suf]] -command "tracepoly $c" -activebackground grey
button .wcorner.wtool.symh -image [image create photo -file [file join sysdata symh$suf]] -command "symh $c" -activebackground grey
button .wcorner.wtool.symv -image [image create photo -file [file join sysdata symv$suf]] -command "symv $c" -activebackground grey

pack .wcorner.wtool.image .wcorner.wtool.texte .wcorner.wtool.forme .wcorner.wtool.symh .wcorner.wtool.symv -side top -anchor n -fill x


frame .wmenu 
pack .wmenu -side top -padx 100

############################## la barre de menus
majmenu $c 
#######################################
frame .frame -width [expr [winfo screenwidth .]] -height [expr [winfo screenheight .]]
pack .frame -side top -fill both -expand yes

wm geometry . +0+0
wm title . [mc {Symcolor - La symetrie en couleur}]



set canvh [expr ([winfo screenheight .] -$widthf)/2]
set canvw [expr ([winfo screenwidth .] -$widthf)/2]

canvas $c -width $canvw -height $canvh 
pack $c



frame .bframe 
pack .bframe -side bottom -anchor w

}

####################################################################

proc creetool {itm c} {
global canvw canvh taillefont tabpage tid msound tabobj iwish progaide basedir nompage plateforme
set ind [lindex [$c gettags cible] [lsearch -regexp [$c gettags cible] uident*]]
catch {destroy .waction}
set widthc "19"
if {[winfo screenwidth .] == "800"} {
set widthc 13
}



if {$itm=="canvas"} {
return
} else {
frame .waction -width [expr $canvw] -height 32
pack .waction -side left -anchor w -fill both
button .waction.charge -image [image create photo -file [file join sysdata devant.gif]] -command "devant $c" -activebackground grey
pack .waction.charge -side left -anchor w -fill y
button .waction.sauve -image [image create photo -file [file join sysdata derriere.gif]] -command "derriere $c" -activebackground grey
pack .waction.sauve -side left -anchor w -fill y
button .waction.sup -image [image create photo -file [file join sysdata supprimer.gif]] -command "delitem $c" -activebackground grey
pack .waction.sup -side left -anchor w -fill y
}

set ftrans2 0

foreach coul {Black DarkGreen Red Green Blue Yellow Cyan Magenta White Brown DarkSeaGreen DarkViolet} {
button .waction.c$coul -image [image create photo -file [file join sysdata [string tolower $coul].gif]] -background $coul -activebackground $coul -width $widthc -command "changefond1 $c $coul"
pack .waction.c$coul -side left -fill y
}
	
}


#############################################################################






proc forme {c nom} {
     if {$nom !=""} {
	createpolygon $c $nom.pol Blue Black 0 new 
	}

}





proc setcolor {type c} {
	global tid
    	set color [tk_chooseColor -title [mc {Choisir une couleur}]]

    if {$color != ""} {
	switch $type {
		canvas {$c configure -background $color}
		coultext {$c itemconfigure cible -fill $color}
		coulfond {changefond $c $color}
		coultour {changetrait $c $color}
		coulfond1 {changefond1 $c $color}
		coultrait1 {changetrait1 $c $color}
		bouton2 {changetextebouton2 $c $color}
	}
    }
}



proc setlang {lang} {
global env plateforme baseHome c
set env(LANG) $lang
set f [open [file join $baseHome reglages lang.conf] "w"]
puts $f $lang
close $f

::msgcat::mclocale $lang
::msgcat::mcload [file join [file dirname [info script]] msgs]
tk_messageBox -message [mc {Les changements prendront effets au redemarrage de l'application.}] -type ok -title "Symcolor"
}


proc quitte {c} {
set answer [tk_messageBox -message [mc {Enregistrer les modifications?}] -type yesno -icon info -title "Symcolor"]
if {$answer == "yes"} {
sauve $c old
}
destroy .
}

interface $c
