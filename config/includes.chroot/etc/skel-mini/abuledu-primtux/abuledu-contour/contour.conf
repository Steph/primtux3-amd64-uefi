# $id$
# contour.tcl configuration file

#*************************************************************************
#  Copyright (C) 2002 André Connes <andre.connes@wanadoo.fr>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : contour.tcl
#  Author  : André Connes <andre.connes@wanadoo.fr>
#  Modifier:
#  Date    : 19/05/2004
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version    $Id: contour.conf,v 1.24 2007/01/18 08:51:11 abuledu_andre Exp $
#  @author     André Connes
#  @modifier
#  @project    Le terrier
#  @copyright  André Connes
#
#***********************************************************************

set glob(tcl_version) $tcl_version

set glob(bgcolor) #cf9e28
set glob(charSize) 9
set glob(couleurs) [list noir jaune rouge vert bleu]
set glob(colors) [list black yellow red green blue]
set glob(width) 609 ;#800
set glob(height) 609 ;#600
set glob(attente) 5  ;#temps d'attente entre deux sessions en seconde(s)
set glob(org) 3       ;#translation de l'origine des coordonnées pour ajustement
set glob(ordre) 1     ;# presentation aleatoire (0) ou ordonnee (1) des images

set glob(autorise) [file writable [file join [pwd] images]]

set glob(passwd) "terrieradmin"

# ##############################
# ne pas modifier ci-dessous svp
# ##############################

# gestion des traces-élèves suivant l'OS
#   user=répertoire+identifiant-élève
if { $tcl_platform(platform) == "unix" } {
  set glob(platform) unix
  set glob(trace_dir) [file join $env(HOME) leterrier contour log]
  set glob(trace_user) $glob(trace_dir)/$tcl_platform(user)
  set glob(home) [file join $env(HOME)]
  set glob(home_contour) [file join $env(HOME) leterrier contour]
  set glob(home_reglages) [file join $glob(home_contour) reglages]
  set glob(home_msgs) [file join $glob(home_contour) msgs]
  set glob(progaide) ./runbrowser
  set glob(wish) wish
  package require Img
} elseif { $tcl_platform(platform) == "windows" } {
  set glob(platform) windows
  set glob(home) [file dir $argv0]
  set glob(home_contour) [file dir $argv0] ;# cad .
  set glob(trace_dir) [file join [file dir $argv0] contour log]
  set glob(trace_user) "$glob(trace_dir)/eleve" ;#forcer le nom (cf contour.tcl)
  set glob(home_reglages) [file join $glob(home_contour) reglages]
  set glob(home_msgs) [file join $glob(home_contour) msgs]
  set a ""
  catch {set a $env(USERNAME) }
  if { $a != "" } {
  # répertoire pour tous les utilisateurs
  #     catch { set glob(home_contour) [file join $env(ALLUSERSPROFILE) leterrier contour] }
  # ou répertoire pour l'utilisateur loggé
        catch { set glob(home_contour) [file join $env(USERPROFILE) leterrier contour] }
        set glob(trace_dir) [file join $glob(home_contour) log]
        set glob(trace_user) $glob(trace_dir)/$env(USERNAME)
        }
  set glob(home) $glob(home_contour)
  set glob(home_msgs) [file join $glob(home_contour) msgs]
  set glob(home_reglages) [file join $glob(home_contour) reglages]
  set glob(progaide) shellexec.exe
  set glob(wish) wish
  package require Img
} else {
  set glob(platform) undef
  set glob(trace_dir) undef"
  set glob(trace_user) undef
  set glob(home) undef
  set glob(home_contour) undef
  set glob(progaide) undef
}

