var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.calculdiffere = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.

var cartouche, champReponse;
var aNombres, param_operateur;
var reponse_juste;

// Référencer les ressources de l'exercice (images, sons)
// {
//      nom-de-la-ressource : chemin-du-fichier
// }

exo.oRessources = { 
    illustration:"calculdiffere/images/illustration.png",
    cartouche:"calculdiffere/images/cartouche.png"
};

// Options par défaut de l'exercice (définir au moins exoName, totalQuestion, tempsExo et tempsQuestion)

exo.creerOptions = function() {
    var optionsParDefaut = {
        tempsExo:0,
    	totalQuestion:10,
    	totalEssai:1,
    	temps_question:0,
    	temps_exo:0,
    	nombre_a:"400-500",
    	nDecimalesA:0,
    	nombre_b:"100;200;300",
    	nDecimalesB:0,
    	operateur:[1,10],
    	temps_expo:40
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
//Creation des donnees numeriques de l'exo
//Tirage des nombres en fonction des paramètres nombre_a et nombre_b
//
    param_operateur=0;
    var i;
    var aTemp = [];

    for(i=0;i<exo.options.operateur.length;i++){
	   param_operateur+=exo.options.operateur[i];
    }
    var pNa=String(exo.options.nombre_a);
    var pNb=String(exo.options.nombre_b);
    var pDa=String(exo.options.nDecimalesA);
    var pDb=String(exo.options.nDecimalesB);
    aNombres=creer_couples(pNa,pNb,pDa,pDb);

};
/**************
 * Fonctions Utiles
 *****************/ 
function choisir_operateur(paramOperateur,indiceQuestion){
    var type_operation=0;
    //choix de l'operateur
    //Selon le numéro de question, on fait varier les opérateurs
    switch(paramOperateur){
    	case 1 :
    	case 10 :
	case 100 :
	case 1000 :
	//un seul operateur coché
            type_operation=paramOperateur;
            break;
	case 11 ://addition soustraction
            if(indiceQuestion%2===0){type_operation=1;}else{type_operation=10;}
            break;
	case 101 ://addition multiplication
            if(indiceQuestion%2===0){type_operation=1;}else{type_operation=100;}
            break;
	case 1001 ://addition division
            if(indiceQuestion%2===0){type_operation=1;}else{type_operation=1000;}
            break;
	case 110 ://multiplication soustraction
            if(indiceQuestion%2===0){type_operation=100;}else{type_operation=10;}
            break;
	case 1100 ://multiplication division
            if(indiceQuestion%2===0){type_operation=100;}else{type_operation=1000;}
		break;
        case 1010 ://division soustraction
            if(indiceQuestion%2===0){type_operation=1000;}else{type_operation=10;}
            break;
	case 1011 ://division soustraction addition
            if(indiceQuestion%3===0){type_operation=1000;}else if(indiceQuestion%3==1){type_operation=10;}else{type_operation=1;}
            break;
        case 1101 ://division multiplication addition
            if(indiceQuestion%3===0){type_operation=1000;}else if(indiceQuestion%3==1){type_operation=100;}else{type_operation=1;}
            break;
	case 1111 ://division mutiplication soustraction addition
            if(indiceQuestion%4===0){type_operation=1000;}else if(indiceQuestion%3==1){type_operation=100;}else if(indiceQuestion%3==2){type_operation=10;}else{type_operation=1;}
            break;
    }
    return type_operation;
}
function creer_couples(param_nombre_a,param_nombre_b,param_dec_a,param_dec_b){
    var param_a=param_nombre_a;
    var param_b=param_nombre_b;
    var aNombresA=[];
    var aNombresB=[];
    //si il n'y a pas d'option permettant de choisir le nombre
    // de chiffres après la virgule on travaille sur des entiers
    if(param_dec_a===undefined){param_dec_a=0;}
    if(param_dec_b===undefined){param_dec_b=0;}
    var aTemp,i,j;
    if(param_a.indexOf("-")<0){
        //il s'agit d'une liste de 1 ou plusieurs nombres
        aNombresA=param_a.split(";");
    	for (i=0;i< aNombresA.length;i++) {
            aNombresA[i] = virgToPoint(aNombresA[i]);
    	}
    } 
    else {
	//il s'agit d'un intervale
    	aTemp = [];
    	aTemp=param_a.split("-");
    	//generation des decimaux
    	var nMiniA=Number(virgToPoint(aTemp[0]))*Math.pow(10,param_dec_a);
    	var nMaxiA=Number(virgToPoint(aTemp[1]))*Math.pow(10,param_dec_a);
    	var nRangeA=nMaxiA-nMiniA;
    	for(i=0;i<=nRangeA;i++){
             aNombresA[i]=(nMiniA+i)/Math.pow(10,param_dec_a);
    	}
    }
    if(param_b.indexOf("-")<0){
    	//il s'agit d'une liste de 1 ou plusieurs nombres
    	aNombresB=param_b.split(";");
    	for (i=0;i< aNombresB.length;i++) {
                aNombresB[i] = virgToPoint(aNombresB[i]);
    	}
    } 
    else {
    	//il s'agit d'un intervale
    	aTemp=param_b.split("-");
    	//generation des decimaux
    	var nMiniB=Number(virgToPoint(aTemp[0]))*Math.pow(10,param_dec_b);
    	var nMaxiB=Number(virgToPoint(aTemp[1]))*Math.pow(10,param_dec_b);
    	var nRangeB=nMaxiB-nMiniB;
    	for(i=0;i<=nRangeB;i++) {
            aNombresB[i]=(nMiniB+i)/Math.pow(10,param_dec_b);
    	}
    }
    var aPossibles=[];
    for(i=0;i<aNombresA.length;i++){
    	for( j=0; j<aNombresB.length; j++ ){
            aPossibles.push([Number(aNombresA[i]),Number(aNombresB[j])]);
    	}
    }
    //aPossibles.sort(function(A,B){return Math.floor(Math.random()*3)-1});
    util.shuffleArray(aPossibles);
	//on evite de travailler sur un tableau trop grand (100 maxi)
    aPossibles = aPossibles.slice(0,100);
	
	//on supprime les doublons dus à la commutativité des opérations
    var aCouples=[];
    aCouples[0]=aPossibles[0];
    for(i=1;i<aPossibles.length;i++){
        var existe_deja=false;
        for (j=0;j<aCouples.length;j++){
            if(aPossibles[i][0]==aCouples[j][1] && aPossibles[i][1]==aCouples[j][0] ){
        		existe_deja=true;
        		break;
            }
        }
    	if(!existe_deja){
                aCouples.push(aPossibles[i]);
    	}
    }
	//on Mélange le tableau
    //aCouples.sort(function(A,B){return Math.floor(Math.random()*3)-1});
    util.shuffleArray(aCouples);
	//on ne laisse sortir le zero qu'une seule fois
    var supprime_zero=false;
    for(i=0;i<aCouples.length;i++){
    	if(aCouples[i][0]===0 || aCouples[i][1]===0 ){
            if(supprime_zero){
    		  aCouples.splice(i,1);
    		  i--;
            }
            supprime_zero=true;
    	}
    }
//on ajoute des couples si le nombre de couples obtenus est inférieur au nombre de questions
    var n=aCouples.length;
    var dif=exo.options.totalQuestion-aCouples.length;
    if(dif>0){
    	for(i=n;i<options.totalQuestion;i++){
                aCouples.push(aCouples[i%n]);
    	}
    }
    return aCouples;
}
//réécrit un nombre en fonction des normes françaises (virgule et séparateur des milliers)
//renvoie un string
function numToString(nNumber){
    var string=String(nNumber);
    var temp=string.split(".");
    var partie_entiere;
    var partie_decimale;	
    if(temp.length>1){
        partie_entiere=temp[0];
	partie_decimale=temp[1];		
    }else{
	partie_entiere=string;
    }
    if(partie_entiere.length>3){
	partie_entiere= partie_entiere.substr(0,partie_entiere.length-3) + " " + partie_entiere.substr(-3,3);
    }
    if(temp.length>1){
	return partie_entiere+","+partie_decimale;
    }else{
        return partie_entiere;
    }
}
//remplace la virgule par un point
function virgToPoint(sExpre) {
	return sExpre.split(",").join(".");
}
//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration

exo.creerPageTitre = function() {
    exo.blocTitre.html("Calcul différé");
    exo.blocConsigneGenerale.html("Donner le résultat du calcul affiché après qu'il a disparu.");
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
    var q = exo.indiceQuestion;
    var operateur=choisir_operateur(param_operateur,q);
    var texte;
    var temps_expo = exo.options.temps_expo *100;
    if (operateur=="1") {
    	reponse_juste=aNombres[q][0]+aNombres[q][1];
    	if (q%2===0) {
            texte = numToString(aNombres[q][0])+" + "+numToString(aNombres[q][1])+" = ?";
    	}
        else{
            texte = numToString(aNombres[q][1])+" + "+numToString(aNombres[q][0])+" = ?";
    	}
    } 
    else if (operateur == 10) {
	   if(aNombres[q][0]>=aNombres[q][1]){
            reponse_juste=aNombres[q][0]-aNombres[q][1];
            texte = numToString(aNombres[q][0])+" - "+numToString(aNombres[q][1])+" = ?";
        }
        else{
            reponse_juste=aNombres[q][1]-aNombres[q][0];
            texte = numToString(aNombres[q][1])+" - "+numToString(aNombres[q][0])+" = ?";
        }
    } 
    else if (operateur == 100) {
        reponse_juste=(aNombres[q][0]*aNombres[q][1]);
        //on travaille au millieme
        reponse_juste = Math.round(reponse_juste*1000)/1000;
        if (q%2===0) {
            texte = numToString(aNombres[q][0])+" x "+numToString(aNombres[q][1])+" = ?";
    	}
        else{
            texte = numToString(aNombres[q][1])+" x "+numToString(aNombres[q][0])+" = ?";
    	}
    }
    else if (operateur == 1000) {
        reponse_juste=aNombres[q][1];
        texte = numToString(aNombres[q][0]*aNombres[q][1])+" : "+numToString(aNombres[q][0])+" = ?";
    }

//bouton valider cache

    exo.btnValider.hide();

    //
    //
    cartouche = disp.createImageSprite(exo,"cartouche");
    exo.blocAnimation.append(cartouche);
    cartouche.position({
        my:'center center',
        at:'center center',
        of:exo.blocAnimation
    });
    //
    var etiquette = disp.createTextLabel(texte);
    etiquette.css({
        fontSize:32,
        fontWeight:'bold',
        color:'#fff'
    });
    cartouche.append(etiquette);
    etiquette.position({
        my:'center center',
        at:'center center',
        of:cartouche
    });
    //
    champReponse = disp.createTextField(exo,6);
    //
    var to = setTimeout(degageCartouche,temps_expo);
    
    function degageCartouche() {
        cartouche.animate({top:-100},800,'easeInCubic',function(e){
            exo.blocAnimation.append(champReponse);
            champReponse.position({
                my:'center center',
                at:'center center',
                of:exo.blocAnimation
            });
            champReponse.focus();
            exo.btnValider.afficher();
        });
    }
};

// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
    if (champReponse.val() === "") {
       return "rien" ;
    } else if( Number(champReponse.val()) == reponse_juste ) {
        return "juste";
    } else {
        return "faux";
    }
};

// Correction (peut rester vide)

exo.corriger = function() {
    cartouche.position({
        my:'center top',
        at:'center top+100',
        of:exo.blocAnimation,
    });
    //
    var correction = disp.createCorrectionLabel(reponse_juste);
    exo.blocAnimation.append(correction);
    correction.position({
        my:"left center",
        at:"right+20 center",
        of:champReponse,
    });
    var barre = disp.drawBar(champReponse);
    exo.blocAnimation.append(barre);
};

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
    var conteneur = disp.createOptConteneur();
    exo.blocParametre.append(conteneur);
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:"Nombre de questions : "
    });
    conteneur.append(controle);
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"temps_expo",
        texte:"Durée d'exposition (en ds) : "
    });
    conteneur.append(controle);
    
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"temps_exo",
        texte:"Temps pour réaliser l'exercice (en secondes)  : "
    });
    exo.blocParametre.append(controle);
    controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"totalEssai",
        texte:"Nombre d'essais : ",
        aValeur:[1,2],
        aLabel:["Un seul essai","Deux essais"]
    });
    exo.blocParametre.append(controle);
	
    /*
     * Création du conteneur contenant Nombre A et Décimale A
     */
    conteneur = disp.createOptConteneur();
    exo.blocParametre.append(conteneur);
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:200,
        taille:50,
        nom:"nombre_a",
        texte:"Nombre A : "
    });
    conteneur.append(controle);
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"nDecimalesA",
        texte:"Décimales : "
    });
    conteneur.append(controle);
	
    /*
     * Création du conteneur contenant Nombre B et Décimale B
     */
    conteneur = disp.createOptConteneur();
    exo.blocParametre.append(conteneur);
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:200,
        taille:50,
        nom:"nombre_b",
        texte:"Nombre B : "
    });
    conteneur.append(controle);
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"nDecimalesB",
        texte:"Décimales : "
    });
    conteneur.append(controle);
	
    controle = disp.createOptControl(exo,{
        type:"checkbox",
        largeur:24,
        taille:2,
        nom:"operateur",
        texte:"Opérateur : ",
        aValeur:[1,10,100,1000],
        aLabel:["Addition","Soustraction","Multiplication","Division"]
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));