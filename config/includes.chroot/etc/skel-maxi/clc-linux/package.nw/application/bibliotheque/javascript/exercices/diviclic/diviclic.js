var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.diviclic = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre=[1,2,3];
var aNombre,dividende,diviseur,repEleve,cntConseil,cellC,cellF,aReste,aQuot;

// Définir les options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:5,
        totalEssai:1,
        tempsExo:0,
        typeDivision:2,
        afficherAide:true
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Référencer les ressources de l'exercice (textes, image, son)
// exo.oRessources peut être soit un objet soit une fonction qui renvoie un objet
exo.oRessources = { 
    txt : "diviclic/textes/diviclic_fr.json",
    egal: "diviclic/images/egal.png",
    inf : "diviclic/images/inf.png",
    illustration : "diviclic/images/illustration.png"
};

// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
    var i,j,k,nA,nB,niveau,temp;
    niveau = exo.options.typeDivision;
    if (niveau === 0) {
        //tables x2 x3
        aNombre = [];
        while(aNombre.length<exo.options.totalQuestion) {
            nA = 3 + Math.floor(Math.random()*(35-3+1));
            nB = 2 + Math.floor(Math.random()*(3-2+1));
            temp=[nA,nB];
            if(aNombre.indexOf(temp)<0 && Math.floor(nA/nB)<10)
                aNombre.push(temp);
        }
        
    }
    else if (niveau == 1) {
        //tables x2 x3 x 4
        aNombre = [];
        while(aNombre.length<exo.options.totalQuestion) {
            nA = 4 + Math.floor(Math.random()*(44-4+1));
            nB = 2 + Math.floor(Math.random()*(4-2+1));
            temp=[nA,nB];
            if(aNombre.indexOf(temp)<0 && Math.floor(nA/nB)<10)
                aNombre.push(temp);
        }
        
    }
    else if (niveau == 2) {
        //tables x3 x4 x5
        aNombre = [];
        while(aNombre.length<exo.options.totalQuestion) {
            nA = 9 + Math.floor(Math.random()*(53-4+1));
            nB = 3 + Math.floor(Math.random()*(5-3+1));
            temp=[nA,nB];
            if(aNombre.indexOf(temp)<0 && Math.floor(nA/nB)<10)
                aNombre.push(temp);
        }
    }
    else if (niveau == 3) {
        //tables x4 x5 x6
        aNombre = [];
        while(aNombre.length<exo.options.totalQuestion) {
            nA = 16 + Math.floor(Math.random()*(62-16+1));
            nB = 4 + Math.floor(Math.random()*(6-4+1));
            temp=[nA,nB];
            if(aNombre.indexOf(temp)<0 && Math.floor(nA/nB)<10)
                aNombre.push(temp);
        }
    }
    else if (niveau == 4) {
        //tables x5 x6 x7
        aNombre = [];
        while(aNombre.length<exo.options.totalQuestion) {
            nA = 25 + Math.floor(Math.random()*(71-25+1));
            nB = 5 + Math.floor(Math.random()*(7-5+1));
            temp=[nA,nB];
            if(aNombre.indexOf(temp)<0 && Math.floor(nA/nB)<10)
                aNombre.push(temp);
        }
    }
    else if (niveau == 5) {
        //tables x6 x7 x8
        aNombre = [];
        while(aNombre.length<exo.options.totalQuestion) {
            nA = 36 + Math.floor(Math.random()*(80-36+1));
            nB = 6 + Math.floor(Math.random()*(8-6+1));
            temp=[nA,nB];
            if(aNombre.indexOf(temp)<0 && Math.floor(nA/nB)<10)
                aNombre.push(temp);
        }
    }
    else if (niveau == 6) {
        //tables x7 x8 x9
        aNombre = [];
        while(aNombre.length<exo.options.totalQuestion) {
            nA = 49 + Math.floor(Math.random()*(89-49+1));
            nB = 6 + Math.floor(Math.random()*(9-6+1));
            temp=[nA,nB];
            if(aNombre.indexOf(temp)<0 && Math.floor(nA/nB)<10)
                aNombre.push(temp);
        }
    }
    util.shuffleArray(aNombre);
};

//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    exo.keyboard.config({
        numeric:"disabled",
        arrow:"disabled"
    });
    // la consigne
    var q = exo.indiceQuestion;
    dividende = aNombre[q][0];
    diviseur = aNombre[q][1];
    repEleve = [];
    var bleu = "#00bcd4";
    var vert = "#8bc34a";
    var orange = "#ffc107";
    var texte = exo.txt.p1.replace("%1%",dividende).replace("%2%",diviseur);
    var consigne = disp.createTextLabel(texte);
    consigne.css({padding:10,border:"1px solid #ccc",backgroundColor:bleu,color:"#fff",fontWeight:"bold",fontSize:32, letterSpacing:"0.05em"});
    
    exo.blocAnimation.append(consigne);
    consigne.css({left:30+(735-consigne.width())/2,top:30});

    // les quotients
    aQuot = [];
    var cntQuotient = disp.createEmptySprite().css({width:"100%",height:60,left:20,top:130});
    exo.blocAnimation.append(cntQuotient);
    var lblQuotient = disp.createTextLabel("Quotient").css({top:8,color:vert});
    cntQuotient.append(lblQuotient);
    var lblNombre;
    for(var i=0; i <10; i++){
        lblNombre = disp.createTextLabel(String(i));
        lblNombre.data({valeur:i});
        lblNombre.css({width:40, height:40, left:120 + 55*i, border:"1px solid #ccc",textAlign:"center", backgroundColor:vert,color:"#fff",lineHeight:"40px"});
        cntQuotient.append(lblNombre);
        lblNombre.on("touchstart.clcl, mousedown.clc",gererClickQuotient);
        aQuot[i]=lblNombre;
    }

    // les restes
    aReste = [];
    var cntReste = disp.createEmptySprite().css({width:"100%",height:60,left:20,top:200});
    exo.blocAnimation.append(cntReste);
    cntReste.hide();
    var lblReste = disp.createTextLabel("Reste").css({top:8,color:orange});
    cntReste.append(lblReste);
    for(var j=0; j <10; j++){
        lblNombre = disp.createTextLabel(String(j));
        lblNombre.data({valeur:j});
        lblNombre.css({width:40, height:40, left:120 + 55*j, border:"1px solid #ccc",textAlign:"center",backgroundColor:orange,color:"#fff",lineHeight:"40px"});
        cntReste.append(lblNombre);
        lblNombre.on("touchstart.clcl, mousedown.clc",gererClickReste);
        aReste[j]=lblNombre;
    }

    // Les conseils
    cntConseil = disp.createEmptySprite();
    cntConseil.css({width:"100%",height:150,top:260});
    exo.blocAnimation.append(cntConseil);
    var cellA= disp.createTextLabel(exo.txt.p2);
    cellA.css({width:"100%",textAlign:"center"});
    cntConseil.append(cellA);
    var cellB = disp.createTextLabel(util.numToStr(dividende)).css({color:bleu,fontWeight:"bold"});
    cellB.css({left:280,top:50});
    cntConseil.append(cellB);
    cellC = disp.createMovieClip(exo,"egal",30,30,500);
    cntConseil.append(cellC.conteneur);
    cellC.conteneur.css({left:310,top:50});
    cellC.play();
    var sTexte = "( <span class='diviseur'>"+diviseur+"</span> x <span class='quotient'></span> ) + <span class='reste'></span>";
    var cellD = disp.createTextLabel(sTexte).css({color:bleu,fontWeight:"bold"});
    cntConseil.append(cellD);
    cellD.css({left:340,top:50});
    var cellE = disp.createTextLabel("<span class='reste'>r</span>").css({color:bleu});
    cellE.css({left:330,top:100});
    cntConseil.append(cellE);
    cellF = disp.createMovieClip(exo,"inf",30,60,500);
    cellF.conteneur.css({left:350,top:100});
    cntConseil.append(cellF.conteneur);
    cellF.play();
    var cellG = disp.createTextLabel(util.numToStr(diviseur)).css({color:bleu,fontWeight:"bold"});
    cellG.css({left:390,top:100});
    cntConseil.append(cellG);
    cntConseil.hide();
    //
    function gererClickQuotient(e){
        e.preventDefault();
        exo.blocInfo.hide();
        repEleve[0] = $(e.target).data("valeur");
        for(var i=0;i<aQuot.length;i++){
            aQuot[i].css({opacity:0.3});
        }
        $(e.target).css({opacity:1});
        cntReste.show();
        if(repEleve[1] !== undefined )
            afficherConseil(repEleve[0],repEleve[1]);
    }
    //
    function gererClickReste(e){
        e.preventDefault();
        exo.blocInfo.hide();
        repEleve[1] = $(e.target).data("valeur");
        for(var i=0;i<aReste.length; i++){
            aReste[i].css({opacity:0.3});
        }
        $(e.target).css({opacity:1});
        afficherConseil(repEleve[0],repEleve[1]);
    }

    function afficherConseil(q,r){
        cntConseil.find('.quotient').html(q);
        cntConseil.find('.reste').html(r);
        cntConseil.find(".quotient").css({color:vert});
        cntConseil.find(".reste").css({color:orange});
        if(exo.options.afficherAide)
            cntConseil.show();
    }
};

// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {
    if(repEleve[1] === undefined){
        return "rien";
    }
    else if(dividende == (diviseur * repEleve[0]) + repEleve[1]){
        cntConseil.find(".conseil").hide();
        if(exo.options.afficherAide){
            cellC.pause();
            cellC.gotoAndStop(1);
            cellF.pause();
            cellF.gotoAndStop(1);
        }
        
        return "juste";
    }
    else {
        return "faux";
    }
};

// Correction (peut rester vide)
exo.corriger = function() {
    // on cache le conseil
    cntConseil.find(".conseil").hide();
    // on corrige le quotient
    if(Math.floor(dividende/diviseur) != repEleve[0]){
        cntConseil.find(".quotient").html(Math.floor(dividende/diviseur)).css({color:"#ff0000"});
        for(var i=0;i<10;i++){
            if(aQuot[i].data().valeur == Math.floor(dividende/diviseur)){
                aQuot[i].css({backgroundColor:"#ff0000",opacity:1});
            }
        }
    }
    // on corrige le reste
    if(dividende%diviseur != repEleve[1]){
        cntConseil.find(".reste").html(dividende%diviseur).css({color:"#ff0000"});
        for(var j=0;j<10;j++){
            if(aReste[j].data().valeur == dividende%diviseur){
                aReste[j].css({backgroundColor:"#ff0000",opacity:1});
            }
        }
    }
    cntConseil.show();
    cellC.pause();
    cellC.gotoAndStop(1);
    cellF.pause();
    cellF.gotoAndStop(1);
    
};

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle;
    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"typeDivision",
        texte:exo.txt.opt1,
        aLabel:exo.txt.label1,
        aValeur:[0,1,2,3,4,5,6]
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"afficherAide",
        texte:exo.txt.opt2,
        aLabel:exo.txt.label2,
        aValeur:[true,false]
    });
    exo.blocParametre.append(controle);
   
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:48,
        taille:3,
        nom:"tempsExo",
        texte:exo.txt.opt3
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));