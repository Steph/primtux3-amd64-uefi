var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.planeteaddition = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre;

var aNombre,aValeurPlanete,arrondiA,arrondiB,repEleve,soluce,fusee,carteCalcul, 
planeteSoluce,haloOrange,haloRouge,haloVert;

// Référencer les ressources de l'exercice (image, son)
exo.oRessources = { 
	txt:"planeteaddition/textes/planeteaddition_fr.json",
	fusee:"planeteaddition/images/fusee.png",
	boule:"planeteaddition/images/boule.png",
	haloOrange:"planeteaddition/images/halo-orange.png",
	haloRouge:"planeteaddition/images/halo-rouge.png",
	haloVert:"planeteaddition/images/halo-vert.png",
	fleche:"planeteaddition/images/fleche.png",
	illustration:"planeteaddition/images/illustration.png"
};
// Options par défaut de l'exercice (définir au moins totalQuestion et tempsExo )

exo.creerOptions = function() {
    var optionsParDefaut = {
		totalQuestion:10,
		totalEssai:1,
		tempsExo:0,
		typeOperation:"addition",
		niveau:1
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
	aNombre=[];
	aValeurPlanete=[];
	creerNombre(exo.options.niveau,exo.options.totalQuestion);
	console.log(aValeurPlanete);
	console.log(aNombre);
	
	function creerNombre(niveau,nbreQuestion)
	{
		var nCentaineInf = 0;
		var soluceTemp,leurre1,leurre2,leurre3,leurre4,leurre5,leurre6,leurre7,leurre8,leurre9;
		var nA,nB,i,arrondiA,arrondiB;
		if ( niveau==1 )
		{
			// addition 3 chiffres + 3 chiffres
			// arrondir à la dizaine superieure
			// pas de retenue dans l'ajout des centaines et des dizaines
			// planetes 100, 200, ..., 1 000
			aValeurPlanete=[100,200,300,400,500,600,700,800,900,1000];
			
			for (i=0;i<nbreQuestion;i++)
			{
				cA = 0 ;
				cB = 1 + Math.floor(Math.random()*(9-1+1));
				dA = 1 + Math.floor(Math.random()*(9-1+1));
				dB = 1 + Math.floor(Math.random()*(9-1+1));
				uA = 1 + Math.floor(Math.random()*(9-1+1));
				uB = 1 + Math.floor(Math.random()*(9-1+1));
				nA = 100*cA+10*dA+uA;
				arrondiA = Math.round((10*dA+uA)/10)*10;
				nB = 100*cB+10*dB+uB;
				arrondiB = Math.round((10*dB+uB)/10)*10;
				//soluceTemp = Math.round((nA+nB)/100)*100;
				// centaine inférieure
				if(i<5){
					if (
						nA%5 !== 0 && nB % 5 !== 0 && cA+cB<10 && arrondiA+arrondiB<50 && util.find([nA,nB],aNombre) < 0
					) 
					{
						aNombre.push([nA,nB]);
					} 
					else {
						i--;
					}
				}
				// centaine supérieure
				else {
					if (
						nA%5 !== 0 && nB % 5 !== 0 && cA+cB<10 && arrondiA+arrondiB> 50 && arrondiA+arrondiB<100 && util.find([nA,nB],aNombre) < 0
					) 
					{
						aNombre.push([nA,nB]);
					} 
					else {
						i--;
					}
				}
				
				if(arrondiA+arrondiB<50){
					nCentaineInf++;
				}
			}
			
		} 
		else if ( niveau==2 )
		{
			// addition 3 chiffres + 3 chiffres
			// arrondir à la dizaine superieure
			// pas de retenue dans l'ajout des centaines et des dizaines
			// planetes 100, 200, ..., 1 000
			aValeurPlanete=[100,200,300,400,500,600,700,800,900,1000];
			for (i=0;i<nbreQuestion;i++)
			{
				cA = 1 + Math.floor(Math.random()*(9-1+1));
				cB = 1 + Math.floor(Math.random()*(9-1+1));
				dA = 1 + Math.floor(Math.random()*(9-1+1));
				dB = 1 + Math.floor(Math.random()*(9-1+1));
				uA = 1 + Math.floor(Math.random()*(9-1+1));
				uB = 1 + Math.floor(Math.random()*(9-1+1));
				nA = 100*cA+10*dA+uA;
				arrondiA = Math.round((10*dA+uA)/10)*10;
				nB = 100*cB+10*dB+uB;
				arrondiB = Math.round((10*dB+uB)/10)*10;
				soluceTemp = Math.round((nA+nB)/100)*100;
				if (
					nA%5 !== 0 && nB % 5 !== 0 && cA+cB<10 && arrondiA+arrondiB<100 && arrondiA+arrondiB!=50 && util.find([nA,nB,soluceTemp],aNombre) < 0
				) 
				{
					aNombre.push([nA,nB,soluceTemp]);
				} 
				else {
					i--;
				}
				if(arrondiA+arrondiB<50){
					nCentaineInf++;
				}
			}
			
		}
		else if ( niveau==3 )
		{
			
			
		}
		util.shuffleArray(aNombre);
		console.log("arrondiInf",nCentaineInf+"/"+exo.options.totalQuestion);
	}
};

//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration

exo.creerPageTitre = function() {    
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
	exo.keyboard.config({
        numeric:"disabled",
        arrow:"disabled",
        large:"disabled"
    });
    
	exo.blocScore.css({color:"#fefefe"});
	var angle=0;
	var rayon = 70;
	var q = exo.indiceQuestion;
	var arrayPlanete = [];
	soluce = Math.round((aNombre[q][0]+aNombre[q][1])/100)*100;
	exo.btnValider.hide();
	//
	var fond = disp.createEmptySprite().css({width:735,height:430,background:"#000"}).css({top:20});
	exo.blocAnimation.append(fond);
	//
	carteCalcul  = disp.createEmptySprite().css({width:160,height:90,background:"#ffffcc"});
	exo.blocAnimation.append(carteCalcul);
	carteCalcul.css({left:10,top:310});
	var consigne = disp.createTextLabel(exo.txt.p1);
	carteCalcul.append(consigne);
	consigne.css({fontSize:14,textAlign:"center",padding:5,fontWeight:"bold"});
	var operation = q%2===1 ? disp.createTextLabel(aNombre[q][0]+" + "+aNombre[q][1]) : disp.createTextLabel(aNombre[q][1]+" + "+aNombre[q][0]);
	carteCalcul.append(operation);
	operation.css({width:"100%", fontWeight:"bold",textAlign:"center",top:50});
	carteCalcul.hide();
	//
	haloOrange = disp.createImageSprite(exo,"haloOrange");
	haloOrange.hide();
	haloRouge = disp.createImageSprite(exo,"haloRouge");
	haloRouge.hide();
	haloVert = disp.createImageSprite(exo,"haloVert");
	haloVert.hide();
	exo.blocAnimation.append(haloOrange,haloRouge,haloVert);
	//
	var aPosition=[[111,60],[310,50],[478,60],[616,62],[320,153],[574,144],[443,210],[603,255],[460,300],[374,367]];
	//util.shuffleArray(aPosition);
	var valeurPlanete = aValeurPlanete;
	//util.shuffleArray(valeurPlanete);
	var posX,posY;
	for ( i = 0 ; i < valeurPlanete.length ; i++ ) {
		posX = aPosition[i][0];
		posY = aPosition[i][1];
		var valeur = valeurPlanete[i];
		var planete = creerPlanete(posX,posY,30+i*3,valeur);
		arrayPlanete[i]=planete;
		if ( valeur == soluce ) {
			planeteSoluce = planete;
		}
	}
	//
	fusee = disp.createImageSprite(exo,"fusee");
	exo.blocAnimation.append(fusee);
	posX = 10 + (carteCalcul.width()-fusee.width())/2;
	posY = 450;
	fusee.css({left:posX,top:posY});
	// on amene la fusee sur la scene
	fusee.transition({y:-240},400,"linear",function(){carteCalcul.show();});
	
	function creerPlanete(x,y,diametre,valeur){
		var planete = disp.createImageSprite(exo,"boule");
		var dW = planete.width()/diametre;
		planete.width(diametre);
		planete.height(diametre);
		exo.blocAnimation.append(planete);
		planete.data({
			valeur:valeur
		});
		var etiquette = disp.createTextLabel(util.numToStr(valeur));
		etiquette.css({
			fontSize:18, width:70,
			textAlign:"center",color:"#fff"
		});
		planete.append(etiquette);
		planete.css({left:x,top:y});
		etiquette.position({my:"center",at:"center",of:planete});
		
		planete.on("mousedown.clc touchstart.clc",gestionClickPlanete);
		return planete;
	}
	
	function gestionClickPlanete(evt) {
		evt.preventDefault();
		var planete =  $(evt.delegateTarget);
		haloOrange.show();
		haloOrange.position({my:"center",at:"center",of:planete});
		//anim.glow(planete.children("img"),"#fff",10,10)
		desactiverClickPlanete();
		repEleve = planete.data().valeur;
		if(soluce == repEleve){
			moveCalculToPlanete(planete);
			
		}
		else if(soluce != repEleve){
			fauxDepart(planete);
		}
		else {
			trace("fin");
		}
	}
	
	function desactiverClickPlanete() {
		for (var i=0;i<arrayPlanete.length; i++) {
			arrayPlanete[i].off("click touchstart",gestionClickPlanete);
		}
	}
	function activerClickPlanete() {
		for (var i=0;i<arrayPlanete.length; i++) {
			arrayPlanete[i].on("click touchstart",gestionClickPlanete);
		}
	}
	
	function moveCalculToPlanete(planete) {
		var origX = fusee.position().left;
		var destX = planete.position().left;
		var origY = 210;
		var destY = planete.position().top;
		//var origZ = fusee.z;
		//carteCalcul.remove();
		//commenter(spInfo.msgJuste,"vert");
		var a = origX - destX;
		var b = origY - destY;
		var rot = - Math.atan2(a,b)* 180 / Math.PI;
		fusee.transition({scale:0.1,rotate:"+="+rot+"deg",x:"+="+(-a)+"px",y:"+="+(-b)+"px"},
			2000,
			function(){
				
				fusee.remove();
				repEleve = planete.data().valeur;
				exo.avancer();
		});
	}
	
	function fauxDepart(planete) {
		console.log("faux depart");
		fusee.transition({y:"-=50px"},1000,"easeInCubic");
		fusee.transition({y:"+=50px"},500,"easeInCubic",function(){
			repEleve = planete.data(valeur);
			exo.avancer();
		});
	}
};

// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
	console.log(soluce,repEleve);
    if(repEleve == soluce) {
		haloOrange.remove();
		haloVert.show();
		haloVert.position({my:"center",at:"center",of:planeteSoluce});
		//planeteSoluce.children(".text-sprite").css({color:"#8ABE2B",fontWeight:"bold"})
		return "juste";
	}
	else {
		return "faux";
	}
};

// Correction (peut rester vide)
exo.corriger = function() {
	exo.cacherBtnSuite();
	var q = exo.indiceQuestion;
	var aCellule = [];
	var nA,nB;
	var tableau = $("<table></table>");
	for ( var i=0; i< 5; i++ ) {
		aCellule[i] = [];
		var ligne = $("<tr></tr>");
		for ( var j=0; j< 5; j++ ) {
			var cell = $("<td></td>").css({textAlign:"center",height:21,fontSize:18,fontWeight:"bold"});
			var largeur = j % 2 === 0 ? 40 : 20;
			if (j == 4)
				largeur = 50;
			cell.css({width:largeur});
			if (i==1)
				cell.css({textAlign:"left",verticalAlign:"top"});
			ligne.append(cell);
			aCellule[i][j] = cell;
		}
		tableau.append(ligne);
	}
	exo.blocAnimation.append(tableau);
	tableau.css({position:"absolute",padding:5,left:100,top:250,color:"#fff"});
	
	var monDemon = anim.creerDemon(aCellule,afficherCorrection,1200,8);
	monDemon.start();
	
	//
	function afficherCorrection(nEtape){
		if ( nEtape == 1 ) {
			// on fait de la place
			fusee.hide();
			carteCalcul.hide();
		}
		else if ( nEtape == 2 ) {
			//on réaffiche le calcul
			
			this[0][0].html(aNombre[q][0]);
			this[0][1].html("+");
			this[0][2].html(aNombre[q][1]);
		}
		else if ( nEtape == 3 ) {
			// on affiche le point d'interrogation
			this[0][3].html("?");
		}
		else if ( nEtape == 4 ) {
			// on affiche les fleches
			var fleche;
			fleche = disp.createImageSprite(exo,"fleche");
			this[1][0].html(fleche);
			fleche = disp.createImageSprite(exo,"fleche");
			this[1][2].html(fleche);
		}
		else if ( nEtape == 5 ) {
			// on affiche l'arrondi
			if (aNombre[q][0] < 10 ) {
				nA = aNombre[q][0];
			}
			else if (aNombre[q][0] > 10 && aNombre[q][0] <100) {
				nA = Math.round(aNombre[q][0]/10)*10;
				this[2][0].html(nA);
			} else if (aNombre[q][0] > 100 && aNombre[q][0] <1000) {
				nA = Math.round(aNombre[q][0]/10)*10;
				this[2][0].html(nA);
			}
			if (aNombre[q][1] < 10 ) {
				nB = aNombre[q][1];
			}
			else if (aNombre[q][1] > 10 && aNombre[q][1] <100) {
				nB = Math.round(aNombre[q][1]/10)*10;
				this[2][2].html(nB);
			} else if (aNombre[q][1] > 100 && aNombre[q][1] <1000) {
				nB = Math.round(aNombre[q][1]/10)*10;
				this[2][2].html(nB);
			}
		}
		else if ( nEtape == 6 ) {
			//on affiche les autres chiffres signes
			if ( aNombre[q][0] < 10 ) {
				this[2][0].html(aNombre[q][0]);
			} else if (aNombre[q][1]  < 10 ) {
				this[2][2].html(aNombre[q][1]);
			}
			this[2][1].html("+");
			this[2][3].html("=");
		}
		else if ( nEtape == 7 ) {
			// on affiche le resultat
			console.log(nA,nB);
			this[2][4].html(util.numToStr(nA+nB));
		}
		else if ( nEtape == 8 ) {
			// on met le calcul en orbite
			haloOrange.remove();
			haloRouge.show();
			haloRouge.position({my:"center",at:"center",of:planeteSoluce});
			var etiquette = disp.createTextLabel(aNombre[q][0]+" + "+aNombre[q][1]);
			etiquette.css({width:100});
			exo.blocAnimation.append(etiquette);
			console.log(planeteSoluce.width(),etiquette.width());
			var posX = planeteSoluce.position().left + (planeteSoluce.width()-etiquette.width())/2;
			var posY = planeteSoluce.position().top - etiquette.height();
			etiquette.css({fontSize:16,fontWeight:"bold",color:"#ff0000",left:posX,top:posY,textAlign:"center"});
			//planeteSoluce.children(".text-sprite").css({color:"#ff0000",fontWeight:"bold"})
			var oX = planeteSoluce.position().left+(planeteSoluce.width())/2;
			var oY = planeteSoluce.position().top+(planeteSoluce.height())/2;
			requestAnimationFrame(bouclerAnim);
			exo.afficherBtnSuite();
		}
		
		var angle=0;//(servira à faire tourner le calcul autour de la planete);
		function bouclerAnim(dt){
			requestAnimationFrame(bouclerAnim);
			angle += Math.PI/360;
			var posX = oX + 50*Math.cos(angle);
			var posY = oY + 50*Math.sin(angle);
			etiquette.css({left:posX-etiquette.width()/2,top:posY-etiquette.height()/2});
		}
	}
};

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
	var controle;
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:48,
        taille:3,
        nom:"tempsExo",
        texte:"Temps pour réaliser l'exercice (en secondes)  : "
    });
    exo.blocParametre.append(controle);
    //
    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"niveau",
        texte:exo.txt.option2,
        aLabel:exo.txt.label2,
        aValeur:[1,2]
    });
    exo.blocParametre.append(controle);
    
};


/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));
