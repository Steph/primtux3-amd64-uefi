var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.numbercrushdecimaux = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre=[1,2,3];
var aNombre,aCouleur,cible;


// Définir les options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:1,
        totalTentative:10,
        totalEssai:1,
        tempsExo:0,
        cible:5,
        cote:4
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Référencer les ressources de l'exercice (textes, image, son)
// exo.oRessources peut être soit un objet soit une fonction qui renvoie un objet
exo.oRessources = { 
    txt : "numbercrushdecimaux/textes/numbercrushdecimaux_fr.json",
    illustration : "numbercrushdecimaux/images/illustration.png"
    // illustration : "template/images/illustration.png"
};

// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
    aCouleur = ["#8CC63F","#42C4C6","#3A65C6","#9341C6","#C63290","#C65346","#C67642","#C6B040","#5D3BC6"];
    util.shuffleArray(aCouleur);
    
    
};

//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    exo.keyboard.config({
        numeric : "disabled",
        arrow : "disabled",
        large : "disabled"
    });
    var aSelect = []; // tableau des cases selectionees par l'utilisateur
    var aGrille = []; // tableau des valeurs des cases
    var aCase = []; // tableau des cases HTML
    var cote = exo.options.cote; // nombre de cases sur un côté;
    var largeur=65;
    var hauteur=55;
    
    //var min=1,max=cible-2;
    cible = exo.options.cible;
    if (cible == 2 ) {
        aNombre =[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,1];
    } else if (cible == 5) {
        aNombre =[0.5,1,1.5,2,2.5,3,3.5,4];
    } else if (cible == 6) {
        aNombre =[0.5,1,1.5,2,2.5,3,3.5,4,4.5];
    }
    
    exo.btnValider.hide();
    
    //creation de la consigne
    var consigne = disp.createTextLabel(exo.txt.p1+" "+cible+" "+exo.txt.p2);
    exo.blocAnimation.append(consigne);
    consigne.css({fontSize:20,fontWeight:"bold"});
    
    if(exo.options.cote == 6){
        consigne.css({width:100,textAlign:"center"});
        consigne.position({my:"right middle",at:"right-30 middle",of:exo.blocAnimation});
    }else{
        consigne.position({my:"center top",at:"center top+25",of:exo.blocAnimation});
    }
    
    // creation du tableau de reference des valeurs
    while(faireTroisH(aGrille,cible)<cote-1 || faireTroisV(aGrille,cible) < cote-1){
        creerGrille();
    }
    var conteneur = disp.createEmptySprite();
    var marge = (cote+1)*2.5;
    conteneur.css({width:cote*(largeur)+marge,height:cote*(largeur)+marge,border:'2px solid #aaa',overflow:'hidden'});
    conteneur.css({left:(735-conteneur.width())/2,top:(450-conteneur.height())/2});
    exo.blocAnimation.append(conteneur);
    for(var i=0; i < cote; i++){
        for(var j=0; j < cote; j++){
            var valeur = aGrille[i][j];
            var label=disp.createTextLabel( util.numToStr(valeur) );
            label.data({uid:(i*cote)+j,l:i,c:j,val:valeur,color:aCouleur[((valeur*10)-1)%9],selected:false});
            conteneur.append(label);
            label.css({
                width:largeur,
                height:largeur,
                left:j*(largeur+2.5)+2.5,
                top:i*(largeur+2.5)+2.5,
                lineHeight:largeur+'px',
                textAlign:"center",
                background:aCouleur[((valeur*10)-1)%9],
                color:"#fff"
            });
            label.on("mousedown.clc touchstart.clc",gererClick);
            aCase.push(label);
        }
        
    }
    
    function gererClick(e){
        e.preventDefault();
        var nombre = $(e.target);
        var i;
        //on retire le nombre du tableau on deselectionne
        if (nombre.data().selected) {
            for( i=0;i<aSelect.length;i++){
                if (nombre.data().uid == aSelect[i].data().uid) {
                    aSelect.splice(i,1);
                }
            }
            nombre.data().selected=false;
            nombre.css({background:nombre.data().color});
        }
        //on ajoute le nombre au tableau et on selectionne
        else {
            // les cases doivent être contigues
            if (
                aSelect.length == 1 && Math.abs(nombre.data().l-aSelect[0].data().l) + Math.abs(nombre.data().c-aSelect[0].data().c) > 1
            )
            {
                return;
            }
            else if ( aSelect.length == 2 && aSelect[0].data().c == aSelect[1].data().c )
            {
                var aL = [aSelect[0].data().l,aSelect[1].data().l,nombre.data().l];
                var maxL = Math.max.apply(Math,aL);
                var minL = Math.min.apply(Math,aL);
                if (maxL-minL>2 || aSelect[0].data().c != nombre.data().c) {
                    return;
                }
                
            }
            else if (
                aSelect.length == 2 && aSelect[0].data().l == aSelect[1].data().l
            )
            {
                var aC = [aSelect[0].data().c,aSelect[1].data().c,nombre.data().c];
                var maxC = Math.max.apply(Math,aC);
                var minC = Math.min.apply(Math,aC);
                if (maxC-minC>2 || aSelect[0].data().l != nombre.data().l) {
                    return;
                }
            }
            // elles sont contigues
            aSelect.push(nombre);
            nombre.data().selected=true;
            nombre.css({background:"#ccc"});
            if ( aSelect.length == 3) {
                var total = 0;
                var aVide;
                for( i=0; i<aSelect.length; i++){
                    total+=aSelect[i].data().val;
                }
                // c'est gagné
                if (total == cible) {
                    console.log("gagné");
                    //on reference les cases qui vont devoir etre deplacées vers le bas
                    var aToMove = [];
                    var aCol = [aSelect[0].data().c,aSelect[1].data().c,aSelect[2].data().c];
                    var aLin = [aSelect[0].data().l,aSelect[1].data().l,aSelect[2].data().l];
                    var cMin = Math.min.apply(Math,aCol);
                    var cMax = Math.max.apply(Math,aCol);
                    var lMin = Math.min.apply(Math,aLin);
                    var lMax = Math.max.apply(Math,aLin);
                    for( i=cMin; i<=(lMin-1)*cote+cMax; i++){
                        if (aCase[i].data().c >= cMin && aCase[i].data().c <= cMax) {
                            aToMove.push(aCase[i]);
                        }
                    }
                    // on retire les cases sélectionnées du DOM, et du tableau des valeurs
                    for( i=0;i<aSelect.length;i++){
                         var uid = aSelect[i].data().uid;
                         var l = aSelect[i].data().l;
                         var c = aSelect[i].data().c;
                         aGrille[l][c]=0;
                         aCase[uid].remove();
                         aCase[uid]=null;
                    }
                    aSelect=[];
                    // s'il n'y a pas de case à déplacer on rajoute de nouvelles cases
                    if (aToMove.length === 0) {
                        aVide = donnerCaseVide(aGrille);
                        rajouterNouvelleCase(aVide,aGrille);
                    }
                    // on "deplace" ces cases dans le tableau des cases et dans le tableau des valeurs
                    else {
                        var nLigne = lMax-lMin+1;
                        for( i=aToMove.length-1;i>=0;i--){
                            var elt = aToMove[i];
                            aCase[elt.data().uid] = null;
                            aGrille[elt.data().l][elt.data().c] = 0;
                            var newUid = elt.data().uid+cote*nLigne;
                            elt.data().l+=nLigne;
                            elt.data().uid=newUid;
                            aCase[newUid] = elt;
                            aGrille[elt.data().l][elt.data().c] = elt.data().val;
                        }
                        //on repere les cases vides
                        aVide = donnerCaseVide(aGrille);
                        
                        //on deplace les cases sur la page
                        for( i=0;i<aToMove.length;i++){
                            //var dx = aToMove[i].position().top+((largeur+2.5)*nLigne);
                            var dx = (largeur+2.5)*nLigne;
                            aToMove[i].transition({top:"+="+dx},500,"linear");
                        }
                        //on remet de nouvelles cases
                        rajouterNouvelleCase(aVide,aGrille);
                    }
                }
                // c'est perdu on agite les cases
                else {
                    console.log("perdu");
                    aSelect[0].effect('shake',{distance:5,times:6},500);
                    aSelect[1].effect('shake',{distance:5,times:6},500);
                    aSelect[2].effect('shake',{distance:5,times:6},500,function(){
                        for(var i=0;i<aSelect.length;i++){
                            aSelect[i].css({background:aSelect[i].data().color});
                            aSelect[i].data().selected=false;
                        }
                        aSelect=[];
                    });
                }
            }
        }
        
    }
    
    // calcule combien de possibilités il y a de faire "valeur"
    // avec trois cases qui se suivent horizontalement
    function faireTroisH(grille,valeur){
        var reussite=0;
        for(var i=0; i<grille.length; i++){
            var ligne = grille[i];
            for(var j=0; j<ligne.length-2; j++){
                var somme = ligne[j]+ligne[j+1]+ligne[j+2];
                if (somme == valeur) {
                    reussite++;
                    j+=2;
                }
            }
        }
        return reussite;
    }
    
    // calcule combien de possibilités il y a de faire "valeur"
    // avec trois cases qui se suivent verticalement
    function  faireTroisV(grille,valeur){
        var reussite=0;
        for(var i=0; i<grille.length; i++){
            for(var j=0; j<grille.length-2; j++){
                var somme = grille[j][i]+grille[j+1][i]+grille[j+2][i];
                if (somme == valeur) {
                    reussite++;
                    j+=2;
                }
            }
        }
        return reussite;
    }
    
    function faireTrois(rangee,valeur){
        //c'est une ligne
        if (rangee.length==1) {
           return faireTroisH(rangee,valeur);
        }
        // c'est une colonne
        else if (rangee.length == cote){
            return faireTroisV(rangee,valeur);
        }
        else {
            console.log("Erreur fonction faireTrois");
            return false;
        }
    }

    function creerGrille(){
        aGrille=[];
        for(var i=0; i < cote ;i++){
            var aLigne=[];
            for(var j=0;j<cote;j++){
                var index = 0 + Math.floor(Math.random()*(7-0+1));
                aLigne[j] = aNombre[index];
            }
            aGrille[i]=aLigne;
        }
    }
    
    function rajouterNouvelleCase(aCaseVide,grille) {
        var newGrille=[];
        var rangee = [];
        var count = 0;
        while(
          (faireTroisH(newGrille,cible)<1 && faireTroisV(newGrille,cible) < 1) || (faireTrois(rangee,cible)>0 && count<50)
        )
        {
            newGrille = raffraichirGrille(aCaseVide,grille);
            rangee = [];
            if (aCaseVide[0][0] == aCaseVide[1][0]) {
                rangee = [newGrille[aCaseVide[0][0]]];
            } else {
                for(var n=0;n<cote;n++){
                    //var valeur = newGrille[n][aCaseVide[0][1]];
                    //rangee.push([valeur]);
                    rangee.push(newGrille[n][aCaseVide[0][1]]);
                }
            }
            count++;
        }
        if (count>=50){
            //on recree une nouvelle grille
            aGrille=[];
            while(faireTroisH(aGrille,cible)<cote-1 || faireTroisV(aGrille,cible) < cote-1){
                creerGrille();
            }
            for( var i=0; i < cote; i++){
                for(var j=0; j < cote; j++){
                    var valeur = aGrille[i][j];
                    if (aCase[(i*cote)+j]) {
                        aCase[(i*cote)+j].remove();
                    }
                    
                    var label=disp.createTextLabel( util.numToStr(valeur) );
                    label.data({uid:(i*cote)+j,l:i,c:j,val:valeur,color:aCouleur[((valeur*10)-1)%9],selected:false});
                    conteneur.append(label);
                    label.css({
                        width:largeur,
                        height:largeur,
                        left:j*(largeur+2.5)+2.5,
                        top:i*(largeur+2.5)+2.5,
                        lineHeight:largeur+'px',
                        textAlign:"center",
                        background:aCouleur[((valeur*10)-1)%9],
                        color:"#fff",
                        opacity:0
                    });
                    label.on("mousedown.clc touchstart.clc",gererClick);
                    aCase[(i*cote)+j] = label;
                    label.transition({opacity:1},1000);
                }
                
            }
            
        }
        else {
            // on insere dans les cases vides de l'ancienne grille les valeurs de la nouvelle
            for(var k=0;k<aCaseVide.length;k++){
                var index=aCaseVide[k];
                var l = index[0];
                var c = index[1];
                var newVal = newGrille[l][c];
                aGrille[l][c]= newVal;
                var newLabel=disp.createTextLabel( util.numToStr(newVal) );
                newLabel.data({uid:l*cote+c,l:l,c:c,val:newVal,color:aCouleur[((newVal*10)-1)%9],selected:false});
                newLabel.css({
                    width:largeur,
                    height:largeur,
                    left:c*(largeur+2.5)+2.5,
                    top:(l-3)*(largeur+2.5)+2.5,
                    lineHeight:largeur+'px',
                    textAlign:"center",
                    background:aCouleur[((newVal*10)-1)%9],
                    color:"#fff",
                    opacity:1
                });
                newLabel.on("mousedown.clc touchstart.clc",gererClick);
                aCase[l*cote+c] = newLabel;
                conteneur.append(newLabel);
                var delta = 3*(largeur+2.5);
                newLabel.transition({y:delta},800,"linear");
                
            }
        }
        exo.poursuivreQuestion();
    }
    
    function donnerCaseVide(grille){
        var vide = [];
        for(var i=0;i<grille.length;i++){
            for(var j=0;j<grille.length;j++){
                if (grille[i][j] === 0) {
                    vide.push([i,j]);
                }
            }
        }
        return vide;
    }
    
    function donnerCaseABouger(grille, selection){
        var aToMove = [];
        var aCol = [selection[0].data().c,selection[1].data().c,selection[2].data().c];
        var aLin = [selection[0].data().l,selection[1].data().l,selection[2].data().l];
        var cMin = Math.min.apply(Math,aCol);
        var cMax = Math.max.apply(Math,aCol);
        var lMin = Math.min.apply(Math,aLin);
        var lMax = Math.max.apply(Math,aLin);
        for(var i=cMin; i<=(lMin-1)*cote+cMax; i++){
             if (aCase[i].data().c >= cMin && aCase[i].data().c <= cMax) {
                 aToMove.push(aCase[i]);
             }
        }
    }
    
    function raffraichirGrille(aCaseVide,grille){
        //on fait une copie independante de la grille
        var cloneGrille = [];
        for(var i=0;i<grille.length;i++){
            cloneGrille[i]=[];
            for(var j=0;j<grille[i].length;j++){
                cloneGrille[i][j] = grille[i][j];
            }
        }
        // on rajoute des nouvelles valeurs dans cette copie
        for(var k=0;k<aCaseVide.length;k++){
            var index=aCaseVide[k];
            var alea = 0 + Math.floor(Math.random()*(7-0+1));
            var valeur = aNombre[alea];
            cloneGrille[index[0]][index[1]] = valeur;
            
        }
        return cloneGrille;
    }

};

// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {
    return "juste";
};

// Correction (peut rester vide)
exo.corriger = function() {
    
};

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:48,
        taille:3,
        nom:"tempsExo",
        texte:exo.txt.opt1
    });
    exo.blocParametre.append(controle);
    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"cote",
        texte:exo.txt.opt2,
        aLabel:exo.txt.label2,
        aValeur:[3,4,5,6]
    });
    exo.blocParametre.append(controle);
    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"cible",
        texte:exo.txt.opt3,
        aLabel:exo.txt.label3,
        aValeur:[2,5,6]
    });
    exo.blocParametre.append(controle);
};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));