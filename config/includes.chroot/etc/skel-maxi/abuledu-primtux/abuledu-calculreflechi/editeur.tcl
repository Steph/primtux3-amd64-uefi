############################################################################
# Copyright (C) 2006 Eric Seigne
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : editeur.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: editeur.tcl,v 1.5 2006/08/26 08:30:03 david Exp $
# @author     David Lucardi
# @project
# @copyright  Eric Seigne
#
#
#########################################################################
#!/bin/sh
#Editeur.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

##################################"sourcing
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source fonts.tcl
source path.tcl
source msg.tcl

initapp $plateforme
loadlib $plateforme
inithome

#set fichier [file join $basedir aide index.htm]
#bind . <F1> "exec $progaide \042$fichier\042 &"

#######################################################################"
global arg defaultset

set arg [lindex $argv 0]
switch $arg {
	a+b.conf { set defaultset {+ {0 1 9 1} {0 1 9 1} {0 10 20 1} {10 200 3000 10} 0 {1} {1}} }
	aa+b.conf { set defaultset {+ {10 20 99 1} {0 1 9 1} {0 10 20 1} {10 200 3000 10} 0 {1 10} {1}} }
	aa+bb.conf { set defaultset {+ {10 20 99 1} {10 20 99 1} {0 10 20 1} {10 200 3000 10} 0 {1 10} {1 10}} }
	aaa+b.conf { set defaultset {+ {100 200 999 1} {0 1 9 1} {0 10 20 1} {10 200 3000 10} 0 {1 10 100} {1}} }
	aaa+bb.conf { set defaultset {+ {100 200 999 1} {10 20 99 1} {0 10 20 1} {10 200 3000 10} 0 {1 10 100} {1 10}} }
	aaa+bbb.conf { set defaultset {+ {100 200 999 1} {100 200 999 1} {0 10 20 1} {10 200 3000 10} 0 {1 10 100} {1 10 100}} }
	a-b.conf { set defaultset {- {0 1 9 1} {0 1 9 1} {0 10 20 1} {10 200 3000 10} 0 {1} {1}} }
	aa-b.conf { set defaultset {- {10 20 99 1} {0 1 9 1} {0 10 20 1} {10 200 3000 10} 0 {1 10} {1}} }
	aa-bb.conf { set defaultset {- {10 20 99 1} {10 20 99 1} {0 10 20 1} {10 200 3000 10} 0 {1 10} {1 10}} }
	aaa-b.conf { set defaultset {- {100 200 999 1} {0 1 9 1} {0 10 20 1} {10 200 3000 10} 0 {1 10 100} {1}} }
	aaa-bb.conf { set defaultset {- {100 200 999 1} {10 20 99 1} {0 10 20 1} {10 200 3000 10} 0 {1 10 100} {1 10}} }
	aaa-bbb.conf { set defaultset {- {100 200 999 1} {100 200 999 1} {0 10 20 1} {10 200 3000 10} 0 {1 10 100} {1 10 100}} }
	axb.conf { set defaultset {* {0 1 9 1} {0 1 9 1} {0 10 20 1} {10 200 3000 10} 0 {1} {1}} }
	aaxb.conf { set defaultset {* {10 20 99 1} {0 1 9 1} {0 10 20 1} {10 200 3000 10} 0 {1 10} {1}} }
	aaxbb.conf { set defaultset {* {10 20 99 1} {10 20 99 1} {0 10 20 1} {10 200 3000 10} 0 {1 10} {1 10}} }
	aaaxb.conf { set defaultset {* {100 200 999 1} {0 1 9 1} {0 10 20 1} {10 200 3000 10} 0 {1 10 100} {1}} }
	aaaxbb.conf { set defaultset {* {100 200 999 1} {10 20 99 1} {0 10 20 1} {10 200 3000 10} 0 {1 10 100} {1 10}} }
	aaaxbbb.conf { set defaultset {* {100 200 999 1} {100 200 999 1} {0 10 20 1} {10 200 3000 10} 0 {1 10 100} {1 10 100}} }
	comp1.conf { set defaultset {+ {0 1 9 1} {0 10 10 1} {0 3 20 1} {10 200 3000 10} 1 {1} {1}} }
	comp2.conf { set defaultset {+ {0 15 19 1} {0 20 20 1} {0 3 20 1} {10 200 3000 10} 1 {1} {1}} }
	comp3.conf { set defaultset {+ {0 50 99 1} {0 100 100 1} {0 3 20 1} {10 200 3000 10} 1 {1 10} {1}} }
	comp4.conf { set defaultset {+ {0 20 999 1} {0 50 999 1} {0 3 20 1} {10 200 3000 10} 1 {1} {1}} }
	mult2.conf { set defaultset {* {0 1 999 1} {0 2 20 1} {0 3 20 1} {10 200 3000 10} 1 {1} {1}} }
	mult3.conf { set defaultset {* {0 1 999 1} {0 3 20 1} {0 3 20 1} {10 200 3000 10} 1 {1} {1}} }
	mult4.conf { set defaultset {* {0 1 999 1} {0 4 20 1} {0 3 20 1} {10 200 3000 10} 1 {1} {1}} }
	mult5.conf { set defaultset {* {0 1 999 1} {0 5 20 1} {0 3 20 1} {10 200 3000 10} 1 {1} {1}} }
	mult6.conf { set defaultset {* {0 1 999 1} {0 6 20 1} {0 3 20 1} {10 200 3000 10} 1 {1} {1}} }
	mult7.conf { set defaultset {* {0 1 999 1} {0 7 20 1} {0 3 20 1} {10 200 3000 10} 1 {1} {1}} }
	mult8.conf { set defaultset {* {0 1 999 1} {0 8 20 1} {0 3 20 1} {10 200 3000 10} 1 {1} {1}} }
	mult9.conf { set defaultset {* {0 1 999 1} {0 9 20 1} {0 3 20 1} {10 200 3000 10} 1 {1} {1}} }
	tab+2.conf { set defaultset {+ {0 1 20 1} {2 2 2 1} {0 10 20 1} {10 200 3000 10} 0 {1} {1}} }
	tab+3.conf { set defaultset {+ {0 1 20 1} {3 3 3 1} {0 10 20 1} {10 200 3000 10} 0 {1} {1}} }
	tab+4.conf { set defaultset {+ {0 1 20 1} {4 4 4 1} {0 10 20 1} {10 200 3000 10} 0 {1} {1}} }
	tab+5.conf { set defaultset {+ {0 1 20 1} {5 5 5 1} {0 10 20 1} {10 200 3000 10} 0 {1} {1}} }
	tab+6.conf { set defaultset {+ {0 1 20 1} {6 6 6 1} {0 10 20 1} {10 200 3000 10} 0 {1} {1}} }
	tab+7.conf { set defaultset {+ {0 1 20 1} {7 7 7 1} {0 10 20 1} {10 200 3000 10} 0 {1} {1}} }
	tab+8.conf { set defaultset {+ {0 1 20 1} {8 8 8 1} {0 10 20 1} {10 200 3000 10} 0 {1} {1}} }
	tab+9.conf { set defaultset {+ {0 1 20 1} {9 9 9 1} {0 10 20 1} {10 200 3000 10} 0 {1} {1}} }
	tabx2.conf { set defaultset {* {0 1 20 1} {2 2 2 1} {0 10 20 1} {10 200 3000 10} 0 {1} {1}} }
	tabx3.conf { set defaultset {* {0 1 20 1} {3 3 3 1} {0 10 20 1} {10 200 3000 10} 0 {1} {1}} }
	tabx4.conf { set defaultset {* {0 1 20 1} {4 4 4 1} {0 10 20 1} {10 200 3000 10} 0 {1} {1}} }
	tabx5.conf { set defaultset {* {0 1 20 1} {5 5 5 1} {0 10 20 1} {10 200 3000 10} 0 {1} {1}} }
	tabx6.conf { set defaultset {* {0 1 20 1} {6 6 6 1} {0 10 20 1} {10 200 3000 10} 0 {1} {1}} }
	tabx7.conf { set defaultset {* {0 1 20 1} {7 7 7 1} {0 10 20 1} {10 200 3000 10} 0 {1} {1}} }
	tabx8.conf { set defaultset {* {0 1 20 1} {8 8 8 1} {0 10 20 1} {10 200 3000 10} 0 {1} {1}} }
	tabx9.conf { set defaultset {* {0 1 20 1} {9 9 9 1} {0 10 20 1} {10 200 3000 10} 0 {1} {1}} }

}
. configure -background white
frame .geneframe -background white -height 300 -width 400
pack .geneframe -side left
wm title . [mc {Editeur}]
#################################################génération interface
proc interface {arg} {
global sysFont defaultset

variable complement
catch {destroy .geneframe}

catch {destroy .leftframe}

frame .leftframe -background white -height 480 -width 100 
pack .leftframe -side left -anchor n

frame .geneframe -background white -height 300 -width 200
pack .geneframe -side left -anchor n

label .leftframe.lignevert1 -bg red -text "" -font {Arial 1}
pack .leftframe.lignevert1 -side right -expand 1 -fill y

######################leftframe.frame1
frame .leftframe.frame1 -background white -width 100 
pack .leftframe.frame1 -side top 

label .leftframe.frame1.lab1 -foreground red -bg white -text "[mc {Editeur}] [file rootname $arg]" -font $sysFont(l)
pack .leftframe.frame1.lab1 -side top -fill x -pady 5

label .leftframe.frame1.lab2 -bg blue -foreground white -text [mc {Niveau}] -font $sysFont(l)
pack .leftframe.frame1.lab2 -side top -fill x -expand 1 -pady 5

listbox .leftframe.frame1.listsce -yscrollcommand ".leftframe.frame1.scrollpage set" -width 15 -height 6
scrollbar .leftframe.frame1.scrollpage -command ".leftframe.frame1.listsce yview" -width 7
pack .leftframe.frame1.listsce .leftframe.frame1.scrollpage -side left -fill y -expand 1 -pady 10
bind .leftframe.frame1.listsce <ButtonRelease-1> "changelistsce %x %y"

############################leftframe.frame2
frame .leftframe.frame2 -background white -width 100 
pack .leftframe.frame2 -side top 
button .leftframe.frame2.but0 -text [mc {Nouveau}] -command "additem" -activebackground white -font $sysFont(s)
pack .leftframe.frame2.but0 -pady 13 -padx 2 -side left
button .leftframe.frame2.but1 -text [mc {Supprim.}] -command "delitem" -activebackground white -font $sysFont(s)
pack .leftframe.frame2.but1 -pady 13 -padx 2 -side left


########################leftframe.frame3
frame .leftframe.frame3 -background white -width 100
pack .leftframe.frame3 -side top 


button .leftframe.frame3.but0 -text [mc {ok}] -command "fin" -activebackground white
pack .leftframe.frame3.but0 -side bottom -pady 2


#############paramètres edition
  label .geneframe.lab0_0 -bg white -text [mc {Operation}] -font $sysFont(s)
  grid .geneframe.lab0_0 -column 0 -row 0 -sticky "w e"
  label .geneframe.labl1 -bg red -text "" -font {Arial 1}
  grid .geneframe.labl1 -column 0 -row 1 -columnspan 8 -sticky "w e"

label .geneframe.operation -font $sysFont(t) -bg white
grid .geneframe.operation -column 0 -row 2 -sticky "w e n s"


################colonne nombre 1
  label .geneframe.lab3_0 -bg #78F8FF -text [mc {Nombre 1 (Min Max)}] -font $sysFont(s)
  grid .geneframe.lab3_0 -column 1 -row 0 -sticky "w e" -columnspan 6
  
  button .geneframe.buttonmin1 -text "<" -command "scalemin 1" -bg #78F8FF
  grid .geneframe.buttonmin1 -column 1 -row 2 -sticky "w e n s"
  scale .geneframe.scale1 -orient horizontal -length 120 -from [lindex [lindex $defaultset 1] 0] -to [lindex [lindex $defaultset 1] 2] -tickinterval [expr [lindex [lindex $defaultset 1] 2] - [lindex [lindex $defaultset 1] 0]] -borderwidth 0 -bg #78F8FF -activebackground #78F8FF -highlightcolor #78F8FF -highlightbackground #78F8FF -font $sysFont(s) -resolution [lindex [lindex $defaultset 1] 3]
  grid .geneframe.scale1 -column 2 -row 2 -sticky "w e"
  bind .geneframe.scale1 <ButtonRelease-1> "changescalemin 1"
  button .geneframe.buttonmax1 -text ">" -command "scalemax 1" -bg #78F8FF
  grid .geneframe.buttonmax1 -column 3 -row 2 -sticky "w e n s"

  button .geneframe.buttonmin11 -text "<" -command "scalemin 11" -bg #78F8FF
  grid .geneframe.buttonmin11 -column 4 -row 2 -sticky "w e n s"
  scale .geneframe.scale11 -orient horizontal -length 120 -from [lindex [lindex $defaultset 1] 0] -to [lindex [lindex $defaultset 1] 2] -tickinterval [expr [lindex [lindex $defaultset 1] 2] - [lindex [lindex $defaultset 1] 0]] -borderwidth 0 -bg #78F8FF -activebackground #78F8FF -highlightcolor #78F8FF -highlightbackground #78F8FF -font $sysFont(s) -resolution [lindex [lindex $defaultset 1] 3]
  grid .geneframe.scale11 -column 5 -row 2 -sticky "w e"
  bind .geneframe.scale11 <ButtonRelease-1> "changescalemax 1"
  button .geneframe.buttonmax11 -text ">" -command "scalemax 11" -bg #78F8FF
  grid .geneframe.buttonmax11 -column 6 -row 2 -sticky "w e n s"

  label .geneframe.lab3_2 -bg white -text [mc {Pas nb 1}] -font $sysFont(s)
  grid .geneframe.lab3_2 -column 7 -row 0 -sticky "w e"

  menubutton .geneframe.pas1 -menu .geneframe.pas1.m1 -relief raised -indicatoron 1

  menu .geneframe.pas1.m1 -tearoff 0
  for {set k 0} {$k < [llength [lindex $defaultset 6]]} {incr k 1} { 
  .geneframe.pas1.m1 add command -label [lindex [lindex $defaultset 6] $k] -command "resolution 1 [lindex [lindex $defaultset 6] $k]"
  }
  grid .geneframe.pas1 -column 7 -row 2


  label .geneframe.labl2 -bg red -text "" -font {Arial 1}
  grid .geneframe.labl2 -column 0 -row 3 -columnspan 8 -sticky "w e"
  label .geneframe.labl3 -bg red -text "" -font {Arial 1}
  grid .geneframe.labl3 -column 0 -row 5 -columnspan 8 -sticky "w e"

########################################################""
  label .geneframe.lab0_1 -bg white -text [mc {Complement}] -font $sysFont(s)
  grid .geneframe.lab0_1 -column 0 -row 4 -sticky "w e"

checkbutton .geneframe.comple -text [mc {Complem.}] -bg white -font $sysFont(s) -variable complement
grid .geneframe.comple -column 0 -row 6 -sticky "w e"
######################################################################nombre 2

  label .geneframe.lab3_1 -bg #78F8FF -text [mc {Nombre 2 (Min Max)}] -font $sysFont(s)
  grid .geneframe.lab3_1 -column 1 -row 4 -sticky "w e" -columnspan 6
  
  button .geneframe.buttonmin2 -text "<" -command "scalemin 2" -bg #78F8FF
  grid .geneframe.buttonmin2 -column 1 -row 6 -sticky "w e n s"
  scale .geneframe.scale2 -orient horizontal -length 120 -from [lindex [lindex $defaultset 2] 0] -to [lindex [lindex $defaultset 2] 2] -tickinterval [expr [lindex [lindex $defaultset 2] 2] - [lindex [lindex $defaultset 2] 0]] -borderwidth 0 -bg #78F8FF -activebackground #78F8FF -highlightcolor #78F8FF -highlightbackground #78F8FF -font $sysFont(s) -resolution [lindex [lindex $defaultset 2] 3]
  grid .geneframe.scale2 -column 2 -row 6 -sticky "w e"
  bind .geneframe.scale2 <ButtonRelease-1> "changescalemin 2"
  button .geneframe.buttonmax2 -text ">" -command "scalemax 2" -bg #78F8FF
  grid .geneframe.buttonmax2 -column 3 -row 6 -sticky "w e n s"

  button .geneframe.buttonmin22 -text "<" -command "scalemin 22" -bg #78F8FF
  grid .geneframe.buttonmin22 -column 4 -row 6 -sticky "w e n s"
  scale .geneframe.scale22 -orient horizontal -length 120 -from [lindex [lindex $defaultset 2] 0] -to [lindex [lindex $defaultset 2] 2] -tickinterval [expr [lindex [lindex $defaultset 2] 2] - [lindex [lindex $defaultset 2] 0]] -borderwidth 0 -bg #78F8FF -activebackground #78F8FF -highlightcolor #78F8FF -highlightbackground #78F8FF -font $sysFont(s) -resolution [lindex [lindex $defaultset 2] 3]
  grid .geneframe.scale22 -column 5 -row 6 -sticky "w e"
  bind .geneframe.scale22 <ButtonRelease-1> "changescalemax 2"
  button .geneframe.buttonmax22 -text ">" -command "scalemax 22" -bg #78F8FF
  grid .geneframe.buttonmax22 -column 6 -row 6 -sticky "w e n s"

  label .geneframe.lab3_3 -bg white -text [mc {Pas nb 2}] -font $sysFont(s)
  grid .geneframe.lab3_3 -column 7 -row 4 -sticky "w e"

  menubutton .geneframe.pas2 -menu .geneframe.pas2.m2 -relief raised -indicatoron 1
  menu .geneframe.pas2.m2 -tearoff 0
  for {set k 0} {$k < [llength [lindex $defaultset 7]]} {incr k 1} { 
  .geneframe.pas2.m2 add command -label [lindex [lindex $defaultset 7] $k] -command "resolution 2 [lindex [lindex $defaultset 7] $k]"
  }
  grid .geneframe.pas2 -column 7 -row 6

  label .geneframe.labl13 -bg red -text "" -font {Arial 1}
  grid .geneframe.labl13 -column 0 -row 7 -columnspan 8 -sticky "w e"

  label .geneframe.labl14 -bg red -text "" -font {Arial 1}
  grid .geneframe.labl14 -column 0 -row 9 -columnspan 8 -sticky "w e"


################colonne total
  label .geneframe.lab5_0 -bg white -text [mc {Nb. items}] -font $sysFont(s)
  grid .geneframe.lab5_0 -column 0 -row 8 -sticky "w e" 
  scale .geneframe.scale3 -orient horizontal -length 80 -from [lindex [lindex $defaultset 3] 0] -to [lindex [lindex $defaultset 3] 2] -tickinterval [expr [lindex [lindex $defaultset 3] 2] - [lindex [lindex $defaultset 3] 0]] -borderwidth 0 -bg white -activebackground white -highlightcolor white -highlightbackground white -font $sysFont(s) -resolution [lindex [lindex $defaultset 3] 3]
  grid .geneframe.scale3 -column 0 -row 10 -sticky "w e"

################colonne timing
  label .geneframe.lab6_0 -bg #78F8FF -text [mc {Delai (ms)}] -font $sysFont(s)
  grid .geneframe.lab6_0 -column 1 -row 8 -sticky "w e" -columnspan 6
  scale .geneframe.scale4 -orient horizontal -length 80 -from [lindex [lindex $defaultset 4] 0] -to [lindex [lindex $defaultset 4] 2] -tickinterval [expr [lindex [lindex $defaultset 4] 2] - [lindex [lindex $defaultset 4] 0]] -borderwidth 0 -bg #78F8FF -activebackground #78F8FF -highlightcolor #78F8FF -highlightbackground #78F8FF -font $sysFont(s) -resolution [lindex [lindex $defaultset 4] 3]
  grid .geneframe.scale4 -column 1 -row 10 -sticky "w e" -columnspan 6

charge 0
}


####################################################################"""
proc charge {index} {
global listdata activelist indexpage totalpage arg Home defaultset operation

variable complement


set f [open [file join $Home data $arg] "r"]
set listdata [gets $f]
close $f


set totalpage [llength $listdata] 
set indexpage $index
	.leftframe.frame1.listsce delete 0 end
	for {set k 1} {$k <= [llength $listdata]} {incr k 1} {
	.leftframe.frame1.listsce insert end "[mc {Niveau}] $k"
	}
set activelist [lindex $listdata $indexpage]
.leftframe.frame1.listsce selection set $indexpage
.geneframe.scale1 configure -resolution [lindex $activelist 6]
.geneframe.scale11 configure -resolution [lindex $activelist 6]
.geneframe.scale2 configure -resolution [lindex $activelist 7]
.geneframe.scale22 configure -resolution [lindex $activelist 7]
.geneframe.scale1 set [lindex [lindex $activelist 1] 0]
.geneframe.scale11 set [lindex [lindex $activelist 1] 1]
.geneframe.scale2 set [lindex [lindex $activelist 2] 0]
.geneframe.scale22 set [lindex [lindex $activelist 2] 1]
.geneframe.scale3 set [lindex $activelist 3]
.geneframe.scale4 set [lindex $activelist 4]
set complement [lindex $activelist 5]
set operation [lindex $activelist 0]
.geneframe.operation configure -text $operation
.geneframe.pas1 configure -text [lindex $activelist 6] 

.geneframe.pas2 configure -text [lindex $activelist 7] 



}



###################################################################################"
proc enregistre_sce {} {
global indexpage listdata activelist totalpage arg Home operation
variable complement


set activelist $operation\040\173[.geneframe.scale1 get]\040[.geneframe.scale11 get]\175\040\173[.geneframe.scale2 get]\040[.geneframe.scale22 get]\175\040[.geneframe.scale3 get]\040[.geneframe.scale4 get]\040$complement\040[.geneframe.pas1 cget -text]\040[.geneframe.pas2 cget -text]


set listdata [lreplace $listdata $indexpage $indexpage $activelist]
set f [open [file join $Home data $arg] "w"]
puts $f $listdata
close $f
}

###################################################################
proc scalemin {what} {
.geneframe.scale$what set [expr [.geneframe.scale$what get] - [.geneframe.pas[string range $what 0 0] cget -text]]
changescalemax [string range $what 0 0]
}

proc scalemax {what} {
.geneframe.scale$what set [expr [.geneframe.scale$what get] + [.geneframe.pas[string range $what 0 0] cget -text]]
changescalemin [string range $what 0 0]
}

proc changescalemin {min} {
if {[.geneframe.scale$min get] > [.geneframe.scale$min$min get] } { 
.geneframe.scale$min$min set [.geneframe.scale$min get] 
}
}

proc changescalemax {max} {
if {[.geneframe.scale$max$max get] < [.geneframe.scale$max get] } { 
.geneframe.scale$max set [.geneframe.scale$max$max get]
}
}

proc changelistsce {x y} {
enregistre_sce
set ind [.leftframe.frame1.listsce index @$x,$y]
charge $ind
}

proc fin {} {
enregistre_sce
destroy .
}

#######################################################################
proc delitem {} {
global listdata indexpage totalpage arg Home
	if {$totalpage < 2} {
	tk_messageBox -message [mc {Impossible de supprimer la fiche}] -type ok -title [mc $arg]
	return
	}
set response [tk_messageBox -message [format [mc {Voulez-vous vraiment supprimer la fiche %1$s ?}] [expr $indexpage +1]] -type yesno -title [mc $arg]]

	if {$response == "yes"} {
	set totalpage [expr $totalpage - 1]
		set listdata [lreplace $listdata $indexpage $indexpage]
     		if {$indexpage > 0} {
     		set indexpage [expr $indexpage - 1]						
		} 
	set f [open [file join $Home data $arg] "w"]
	puts $f $listdata
	close $f
	charge $indexpage

	} 

}


proc additem {} {
global listdata indexpage totalpage defaultset arg Home
enregistre_sce
set indexpage $totalpage
incr totalpage

lappend listdata [lindex $defaultset 0]\040\173[lindex [lindex $defaultset 1] 1]\040[lindex [lindex $defaultset 1] 1]\175\040\173[lindex [lindex $defaultset 2] 1]\040[lindex [lindex $defaultset 2] 1]\175\040[lindex [lindex $defaultset 3] 1]\040[lindex [lindex $defaultset 4] 1]\040[lindex $defaultset 5]\040[lindex [lindex $defaultset 6] 0]\040[lindex [lindex $defaultset 7] 0]

set f [open [file join $Home data $arg] "w"]
puts $f $listdata
close $f
charge $indexpage
}

proc resolution {what res} {
.geneframe.pas$what configure -text $res
.geneframe.scale$what configure -resolution [.geneframe.pas$what cget -text]
.geneframe.scale$what$what configure -resolution [.geneframe.pas$what cget -text]

}


interface $arg
