#!/bin/sh
#choix.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jean-louis.sendral@toulouse.iufm.fr
#  Modifier: andre.connes@toulouse.iufm.fr
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     Jean- Louis Sendral
#  @modifier   Andre Connes
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *****************************************

#############Pr�paration du calcul : nom, classe, choix du sc�nario ou de la s�quence.

###################"gestion du son
#"global sound
#"if {[catch {package require -exact snack 2.1}]} {
#"    set sound 0
#"} else {
#"    set sound 1
#"}

set basedir [pwd]
#[file dir $argv0]
cd $basedir
source calculs.conf
source i18n
# #########################################################################
# internationalisation (c) andre.connes@toulouse.iufm.fr
# le fichier i18n, inclus par la commande "source i18n", contient par exemple
#    set i18n(fr,hello) "Bonjour..."
#    set i18n(uk,hello) "Hello..."
# appel   : set message [gettext hello]
# ou bien : puts stdout [gettext hello]
# #########################################################################
proc gettext { m } {
  global LG i18n
  if { [info exists i18n($LG,$m)] } {
    return $i18n($LG,$m)
  } elseif { [info exists i18n(fr,$m)] } {
    return $i18n(fr,$m)
  } else {
    return "err.i18n_$LG : \"$m\" needed."
  }
} ;# proc gettext


global scenario
. configure  -height 480 -width 640
frame .frame -background #ffff80 -height 480 -width 320
grid .frame -column 0 -row 0

label .frame.lab_prenom -text [gettext nom] -background #ffff80
place .frame.lab_prenom -x 110 -y 5
entry .frame.ent_nom
place .frame.ent_nom -x 80 -y 25
focus .frame.ent_nom
#set nom_elev $tcl_platform(user)
#[exec  id -u -n ]
#.frame.ent_nom insert end $nom_elev
#set nom_elev [.frame.ent_prenom get]

label .frame.lab_classe -text [gettext classe] -background #ffff80
place .frame.lab_classe -x 110 -y 50
entry .frame.ent_classe
place .frame.ent_classe -x 80  -y 70
#set nom_classe [.frame.ent_classe get]
if { $tcl_platform(platform) == "unix" } {
set nom_elev $tcl_platform(user)
set nom_classe [lindex [exec  id -G -n ] 1]
} else {
 set nom_elev eleve
 set nom_classe classe
}
.frame.ent_nom insert end $nom_elev
.frame.ent_classe insert end  $nom_classe
label .frame.duree_saut -text [gettext duree1] -background #ffff80
place .frame.duree_saut -x 0 -y 100
entry .frame.ent_duree
place .frame.ent_duree -x 225  -y 100 -width 35
.frame.ent_duree insert end 2000
set duree_saut [.frame.ent_duree get]
label .frame.ent_text_approx -text [gettext approx] -background #ffff80
place .frame.ent_text_approx -x 0 -y 380
entry .frame.ent_approx
place .frame.ent_approx -x 150  -y 380 -width 35

listbox .frame.list1 -background #c0c0c0 -height 10 -width 20 -yscrollcommand ".frame.scroll1 set"
scrollbar .frame.scroll1 -command ".frame.list1 yview"
place .frame.list1 -x 80 -y 175
place .frame.scroll1 -x 227 -y 165 -height 185
label .frame.menu3 -text [gettext double_clic1 ]  -background #ffff80
place .frame.menu3 -x 0 -y 130
label .frame.scenario -text [gettext scena_seq]
place .frame.scenario -x 95 -y 155
#label .frame.menu1 -text "Quel est ton nom :" -background #ffff80
#place .frame.menu1 -x 80 -y 340
#"button .frame.ajouter -text Ajouter
#"place .frame.ajouter -x 180 -y 420
#entry .frame.text1
#place .frame.text1 -x 80 -y 370
#set nom_elev [.frame.text1 get]
bind .frame.list1 <Double-ButtonRelease-1> { calc }
#pour lister les sc�narios et les s�quences d�j� construits.

proc peuplelist1 {} {
global basedir DOSSIER_EXOS
cd $DOSSIER_EXOS
    set ext .sce ; set ext1 .seq
    .frame.list1 delete 0 end
    catch {foreach i [lsort [glob [file join *$ext]]] {
            .frame.list1 insert end $i ;#"[file rootname $i]
        }
    }
catch {foreach i [lsort [glob [file join *$ext1]]] {
        .frame.list1 insert end $i ;#"[file rootname $i]
    }
}


cd $basedir
}
peuplelist1

#pour lancer le ou les claculs##############

proc calculs {} {
return [.frame.list1 get active]
}

proc  calc {} {
set fich [calculs]
global basedir DOSSIER_EXOS
if  {[file extension $fich] ==".sce" } {
    set liste_sce [list $fich]
} else  {
    cd $DOSSIER_EXOS
    set f [open [file join $fich] "r"  ]
    set liste_sce [gets $f]
}
foreach exo  $liste_sce {
    if {[file extension [lindex $liste_sce 0]] != ".sce" } {
        set exo "${exo}.sce"
    }
cd $basedir    
exec  wish  pluie_app2.tcl $exo  [.frame.ent_nom  get] [.frame.ent_classe get] [.frame.ent_duree  get] [.frame.ent_approx get]
destroy . 
}
} ;#"return [.frame.list1 get active]




