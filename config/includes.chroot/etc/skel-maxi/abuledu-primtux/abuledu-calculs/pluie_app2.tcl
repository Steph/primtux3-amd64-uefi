#!/bin/sh
#pluie.tcl   ESSAI DE RECONSTRUCTION D'UN Logiciel IPT (auteur ?)
#\
exec wish "$0" ${1+"$@"}
###########################################################################
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jean-louis.sendral@toulouse.iufm.fr
#  Modifier: andre.connes@toulouse.iufm.fr
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     Jean-Louis Sendral
#  @modifier   Andre Connes
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *************************************************************************
source calculs.conf
source i18n
# #########################################################################
# internationalisation (c) andre.connes@toulouse.iufm.fr
# le fichier i18n, inclus par la commande "source i18n", contient par exemple
#    set i18n(fr,hello) "Bonjour..."
#    set i18n(uk,hello) "Hello..."
# appel   : set message [gettext hello]
# ou bien : puts stdout [gettext hello]
# #########################################################################
proc gettext { m } {
  global LG i18n
  if { [info exists i18n($LG,$m)] } {
    return $i18n($LG,$m)
  } elseif { [info exists i18n(fr,$m)] } {
    return $i18n(fr,$m)
  } else {
    return "err.i18n_$LG : \"$m\" needed."
  }
} ;# proc gettext

set bgcolor #ffff80
set bgl #ff80c0
wm geometry . +0+0
wm  title . [gettext titre]
set date [clock format [clock seconds] -format "%H %M %D"]
set nom_elev  [lindex $argv 1 ] ;#init nom �l�ve et nom clase
set nom_classe [lindex $argv 2 ]
set tempo [lindex $argv 3]
if {[lindex $argv 4] == {} } {
set approx 5
   } else {
set approx [lindex $argv 4]
}
set fich [lindex $argv 0 ]
set f [open [file join $fich] "r"  ]
set listdata [gets $f] ;#init de la liste des couples.
#set fich $f
#"set fich [file tail $f]
close $f
set oper [lindex  $listdata 0]
#set n_lig [lindex  $listdata 2]
#set n_col [lindex  $listdata 3]
#set listdata {+ {1 2 5 3 14 5 7 8 4 5 }}
#set nom_elev toto
#set nom_classe cp
#set approx 5
#init des couples de nombre en jeu, de l'op�ration, des couples au d�part
#des bons calculs et des mauvais
set listcouples [lindex $listdata 1] ; set oper [lindex $listdata 0]
set uu [llength $listcouples]
set nbrecouples [expr $uu /2]
# les widgets :
. configure  -height 480 -width 640
frame .frame -background #ffff80 -height 340 -width 640
grid .frame -column 0 -row 0
label .frame.bonjour -text "[gettext bonjour] $nom_elev" -background #ffff80 -highlightcolor #aaff14 -anchor e
place .frame.bonjour -x 200 -y 5
label .frame.couple -text [gettext calcul_a_faire] -font {Helvetica 10}  ; label .frame.calcul -text [gettext calcul] -font {Helvetica 10}   
place .frame.couple -x 10   -y 30 ; place .frame.calcul -x 300   -y 30 
entry .frame.couples -width  10 -font {Helvetica 18} -justify center  ; entry .frame.resultat -width 6 -font {Helvetica 18} -justify center  ; label .frame.approx -text [gettext signe_approx ] -font {Helvetica 12}   
place .frame.couples -x 50 -y 70 ; place .frame.approx -x 210 -y 72 ; place .frame.resultat -x 350 -y 70 
label .frame.texte1 -text "\([gettext a].. $approx pr�s \)" -foreground red ; place .frame.texte1 -x 450 -y 75
label .frame.texte2 -text [gettext reponses]   ; label .frame.texte3 -text [gettext evaluation]
#place .frame.texte2  -x 25 -y 210  
 place  .frame.texte3 -x 335 -y 120
entry .frame.reponses ; entry .frame.eval
#place .frame.reponses -x 20 -y 230 
 place .frame.eval -x 340 -y 145
button .frame.lance_calcul -text [gettext consigne2] -command " remplir "
place .frame.lance_calcul -x 70 -y 115  ; button .frame.aide -text [gettext aide] -command  "exec wish aide_app2.tcl" ; place  .frame.aide -x  550  -y 130
bind .frame.resultat <Return> { teste_rep [.frame.resultat get] ; calcul [.frame.resultat get]  } 
#.hframe.lab_bilan1${ii}  configure -text "${nx}${oper}${ny}"  -background #ffff00 -font {Arial 20}

 
#set listcouples [lrange $listcouples [expr 2*$ndepart] end]
set nbrecouplesrestant 0 
#"destroy .hframe  #" de foreach
##############################################################################################
# programme principal avec usage pseudo recursif. R�le de after pas tr�s clair encore
# A l'arr�t : travail sauv� ds un fichier qui doit exister
##############################################################################################
proc teste_rep {rep} {
if {[ regexp {(^[0-9]+$)} $rep] == 1 } {
    } else { .frame.resultat delete 0 end ; .frame.resultat insert 0 {}
	}
}
proc calcul { input } {
global  resultat approx nx ny oper liste_rep a_sauver
if { $input == {} } {
return 1
}
set fini 0  
            while { ! $fini } {
# set input [.frame.resultat get ] 
set resultat [expr $nx $oper $ny] ; set evaluation [expr $resultat - $input] 
      if {  abs($evaluation) <= $approx }  {
        .frame.eval delete 0 end ; .frame.eval insert end [gettext rep_bonne] 
	   set liste_rep [lappend $liste_rep $input ]
        .frame.reponses insert end [list :: $nx  $ny  $oper $liste_rep \| ] ; .frame.resultat delete 0 end
.frame.couples delete 0 end ;	 set fini 1
         } else {
          .frame.resultat delete 0 end  ; .frame.eval delete 0 end ; .frame.eval insert end [gettext rep_mauvaise] ; \
   .frame.reponses insert end "$input " ; return
              }
#set liste_rep [list $liste_rep $input]
  } ;#fin wwhile
return
 }


proc remplir {} {
global fich tempo approx  nx ny  oper nom_elev reponse listcouples liste_rep a_sauver \
mrep nbrecouplesrestant indexcol oper  bonneliste echecliste date
set liste_rep {}
#"update
        if  { $listcouples == ""  } {
set a_sauver [.frame.reponses get] ;  sauver $date $nom_elev  $fich $a_sauver ; return
}
set acal [ lrange $listcouples 0 1 ]
            set listcouples [lrange $listcouples 2 end ]
            set nx [lindex $acal 0] ; set ny [lindex $acal 1]
	   .frame.couples delete 0 end ; .frame.couples insert end  "$nx$oper$ny"
focus -force .frame.resultat
}	   
 

proc clignote {texte lig col  nclic} { ; #after supprim� car semble pos� des pbs de // avec l'autre after
#"   if  { $nclic ==1 } {.hframe.lab_bilan${lig}${col}  configure -text "______"  -background #ffffff -font {Arial 20} ; return 1}
for  {set cl 1} {$cl <= $nclic} {incr cl} {
.hframe.lab_bilan${lig}${col}  configure -text "$texte"  -background #ffff00 -font {Arial 20}
.hframe.lab_bilan${lig}${col}  configure -text "$texte"  -background #ffffff -font {Arial 10}
#"  after 50  "clignote $texte $lig $col [expr $nclic - 1 ]" ;
#".hframe.lab_bilan${lig}${col}  configure -text "______"  -background #ffffff -font {Arial 20}
}
}
# sauver les r�sultats
proc sauver {dat nom fic a_sauv } { ; # � s�curiser
global savingFile nom_classe TRACEDIRX
cd $TRACEDIRX
#set types {
#    {"Cat�gories"		{.txt}	}
#}
#catch {set file [tk_getSaveFile -filetypes $types]}
#set f [open [file join $file] "a"]
#set travail  "date : $dat nom : $nom fichier : $fic $oper bon : $listereussite echec : $listechec  mauvais cal : $badcal"
set basefile [file rootname $fic]
set savingFile ${nom_classe}.bil 
#  catch {set f [open [file join $savingFile] "a"]}
  set travail  ":$dat:$nom:$basefile:$a_sauv"
  exec /usr/bin/leterrier_log --message=$travail --logfile=$TRACEDIR/$savingFile
#puts $f $travail
#close $f
}








