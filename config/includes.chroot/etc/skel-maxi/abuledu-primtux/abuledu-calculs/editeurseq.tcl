#!/bin/sh
#editeurseq.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}
#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : $$$
#  Author  : jlsendral@free.fr
#  Modifier: 
#  Date    : 24/04/2002
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version
#  @author     Jean-Louis Sendral
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
#
#  *************************************************************************


###################"gestion du son
#"global sound
#"if {[catch {package require -exact snack 2.1}]} {
#"    set sound 0
#"} else {
#"    set sound 1
#"}

# Construction des widgets de l'interface
# frame1( � doite !) pour modifier ou construire une s�quence et la sauver ########

set basedir [pwd]
source calculs.conf
source path.tcl
source i18n
set basedir [pwd]
set  DOSSIER_EXOS1 [lindex $argv 0 ]

. configure  -height 450 -width 660
##wm resizable . 0 0

bind . <F1> "showaide cmental"
global sysFont test_elev
source msg.tcl
source fonts.tcl
wm geometry . +0+0 ; wm title . [mc {edit_seq}]
set test_elev 1
if { $tcl_platform(platform) == "unix" && [lindex [exec  id -G -n ] 0 ]  == "eleves" } {
if { [string match  "${basedir}*"  [lindex $argv 0 ] ] == 1 } {
set test_elev 0
} 
}
frame .frame1 -background #aaaaaa -height 450 -width 340
grid .frame1 -column 1 -row 0
listbox .frame1.list1 -background #c0c0c0 -height 10 -width 35  \
        -yscrollcommand ".frame1.scroll1 set"
scrollbar .frame1.scroll1 -command ".frame1.list1 yview"
place .frame1.list1 -x 5 -y 60
place .frame1.scroll1 -x 237 -y 60 -height 172
label .frame1.menu3 -text [mc {Double_clic_scena}] -background #aaaaaa
place .frame1.menu3 -x 0 -y 40
label .frame1.categorie -text [mc {Sc�narios}]
place .frame1.categorie -x 100 -y 10
label .frame1.label1 -text [mc {liste_scena}] -background #aaaaaa
place .frame1.label1 -x 70 -y 235
button .frame1.ajouter -text  [mc {sauv_seq}]
place .frame1.ajouter -x 100 -y 310
set message1 "[mc {Puis}] :\n4) [mc {seq-choisie}]\n5) [mc {double_clic_sce}]\n6) [mc {double_clic_seq}]"
message  .frame1.consigne -text $message1 -foreground red  -aspect 300
place .frame1.consigne -x 0 -y 350

#"button .frame1.supprimer -text Supprimer
#"place .frame1.supprimer -x 100 -y 420
entry .frame1.text1 -width 60 -xscrollcommand  ".frame1.scroll2 set"
scrollbar .frame1.scroll2 -command ".frame1.text1 xview" -orient horizontal
place .frame1.text1 -x 0 -y 255
place .frame1.scroll2 -x 0 -y 275 -width 345 -anchor nw
bind .frame1.list1 <Double-ButtonRelease-1> "ajout_scenario"
bind .frame1.ajouter <1> "sauver_sequence"

# frame2 pour ajouter un nom de s�quence, supprimer une s�quence########
#ou d�rouler une s�quence pour la modifier.#############
button .frame1.aide -text [mc {Aide}]
	  place .frame1.aide -x 275 -y 350
	  bind .frame1.aide <1> "exec $progaide file:${basedir}/aide1.html#cmental &"

	  ### "cd  $basedir ; exec $progaide  ./aide.html#cmental"
          
frame .frame2 -background #aaaaaa -height 450 -width 340
grid .frame2 -column 0 -row 0
listbox .frame2.list2 -background #c0c0c0 -height 13 -width 40  \
        -yscrollcommand ".frame2.scroll2 set"
scrollbar .frame2.scroll2 -command ".frame2.list2 yview"
place .frame2.list2 -x  20 -y 60
place .frame2.scroll2 -x 280 -y 60 -height 220
  label .frame2.menu3 -text [mc {double_clic_seq}] -background #aaaaaa
place .frame2.menu3 -x 0 -y 40
label .frame2.categorie -text [mc {sequences}]
place .frame2.categorie -x 100 -y 10
label .frame2.menu1 -text [mc {ajout_supp_seq}] -background #aaaaaa
place .frame2.menu1 -x 45 -y 282
entry .frame2.text2 -width 40
place .frame2.text2 -x 20 -y 300

set message "1)[ mc {tap_seq}]\n2) [ mc {clic_ajsup}]\n3) [ mc {double_clic_nom}]"
message  .frame2.consigne -text $message -foreground red -aspect 300
place .frame2.consigne -x 50  -y 360

button .frame2.ajouter -text [mc {Ajouter}]
place .frame2.ajouter -x 60 -y 330
button .frame2.supprimer -text [mc {Supprimer}]
place .frame2.supprimer -x 180 -y 330
bind .frame2.ajouter <ButtonRelease-1> "ajout_sequence"
bind .frame2.supprimer <ButtonRelease-1> "supprime_sequence"
bind .frame2.list2 <Double-ButtonRelease-1> "montre_scenarios"

set scenario ""
set currentlist ""

#pour  lister les s�quences existantse
 proc peuplelist1 {} {
    global DOSSIER_EXOS basedir  test_elev env  DOSSIER_EXOS1  choix_lieu   Home
  .frame2.supprimer configure  -state  normal
 .frame2.ajouter configure  -state  normal
 .frame1.ajouter  configure  -state  normal
      if { $DOSSIER_EXOS1 ==  $basedir || $test_elev == 0} {
 .frame2.supprimer configure  -state  disabled
 .frame2.ajouter configure  -state  disabled
 .frame1.ajouter  configure  -state  disabled
}
cd $DOSSIER_EXOS1
set ext .seq
 ;#les fichiers s�quences ont pour extension .seq
    .frame2.list2 delete 0 end
    catch {foreach i [lsort [glob [file join *$ext]]] {
            .frame2.list2 insert end [file rootname $i]
        }
    }
cd $basedir
}
peuplelist1

#pour  lister les sc�narios  existants
proc peuplelist2 {} {
   global DOSSIER_EXOS1  basedir   env      choix_lieu     DOSSIER_EXOS  Home
  cd $DOSSIER_EXOS1
set ext .sce
    .frame1.list1 delete 0 end
    catch {foreach i [lsort [glob [file join *$ext]]] {
            .frame1.list1 insert end [file rootname $i]
        }
    }
cd $basedir
}
peuplelist2

# pour ins�rer un sc�nario dans la s�quence
proc ajout_scenario {} {
.frame1.text1 insert end " "
.frame1.text1 insert end [.frame1.list1 get active]

}

# pour ajouter un nom de s�quence et en mettre � jour la liste.

proc ajout_sequence {} {
  global DOSSIER_EXOS1  basedir
##cd $DOSSIER_EXOS1
 set sequence [.frame2.text2 get]
    set ext .seq
    if {[catch {set f [open [file join  $DOSSIER_EXOS1  $sequence$ext] "w"]}]} {
        bell
        return
    } else {
             close $f  ; .frame2.list2 delete 0 end
           .frame2.text2 delete 0 end
              catch {foreach i [lsort [glob -dir $DOSSIER_EXOS1 -type f  *.seq]  ] {
                .frame2.list2 insert end [file rootname [file tail $i] ]
           ##       peuplelist1
          }
##cd $basedir
}
 }
}
# pour supprimer une s�quence et en mettre � jour la liste.############

proc supprime_sequence {} {
global DOSSIER_EXOS1 basedir
##cd $DOSSIER_EXOS1
set sequence [.frame2.text2 get]
set ext .seq
if {$sequence==""} {
    bell
    return
}
catch {[file delete [file join $DOSSIER_EXOS1 $sequence$ext]]}
##peuplelist1
    .frame2.list2 delete 0 end
    catch {foreach i [lsort [glob -dir $DOSSIER_EXOS1 -type f  *.seq]  ]] {
                .frame2.list2 insert end [file rootname [file tail $i]]
              }
             }
             .frame2.text2 delete 0 end
##cd $basedir
}

# pour lister les sc�narios de la s�quence � modifier.###########

proc montre_scenarios {} {
   global DOSSIER_EXOS basedir DOSSIER_EXOS1
cd $DOSSIER_EXOS1
 set ext .seq   
    set sequence [.frame2.list2 get active]
       catch {
        set f [open [file join $sequence$ext] "r"]
        set listdata [gets $f]
        close $f
            }
    .frame1.text1 delete  0 end
    .frame1.text1 insert 0 $listdata
    .frame1.label1 configure -text "[mc {list_scena}] $sequence"
    .frame1.ajouter configure -text " [mc {sauv_seq}]$sequence"
cd $basedir
}

#pour sauver la s�quence modifi�e ou nouvelle.
proc sauver_sequence {} {
global DOSSIER_EXOS1 basedir
cd $DOSSIER_EXOS1
set ext .seq
set sequence [.frame2.list2 get active]
if {[catch {set f [open [file join $sequence$ext] "w"]}]} {
    bell
    return
} else {
    set data [.frame1.text1 get ]
    puts $f $data
    close $f
   .frame1.text1 delete  0 end
 }
cd $basedir
}






