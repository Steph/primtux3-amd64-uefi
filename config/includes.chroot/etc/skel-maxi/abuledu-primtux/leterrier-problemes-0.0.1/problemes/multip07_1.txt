set etapes 1
set niveaux {0 1 2 3 4 5}
::1
set niveau 2
set ope {{3 5} {2 4}}
set interope {{1 9 1} {1 9 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set volatil 0
set fact1 $ope1
for {set k 1} {$k < $ope2} {incr k} {set fact1 [concat $fact1 + $ope1]}
set fact2 $ope2
for {set k 1} {$k < $ope1} {incr k} {set fact2 [concat $fact2 + $ope2]}
regsub -all {[\040]} $fact1 "" fact1
regsub -all {[\040]} $fact2 "" fact2
set operations [list $fact1 $fact2 [list [expr $ope1]*[expr $ope2]] [list [expr $ope2]*[expr $ope1]] [list [expr $ope1*$ope2]/[expr $ope2]]]
set editenon "($ope1 immeubles � construire.)"
set enonce "Les cubes.\nJ'ai [expr $ope1*$ope2] cubes.\nJe construis des immeubles de $ope2 �tages.\nCombien puis-je faire d'immeubles?"
set cible {{7 9 {} source0}}
set intervalcible 50
set taillerect 40
set orgy 40
set orgxorig 50
set orgsourcey 100
set orgsourcexorig 600
set source {cube.gif}
set orient 0
set labelcible {{Immeubles}}
set quadri 0
set reponse [list [list {1} [list {Je peux faire} [expr $ope1] {immeubles de} [expr $ope2]  {�tages.}]]]
set ensembles [list [expr $ope1*$ope2]]
set canvash 350
set c1height 160
set opnonautorise {0 1}
::
