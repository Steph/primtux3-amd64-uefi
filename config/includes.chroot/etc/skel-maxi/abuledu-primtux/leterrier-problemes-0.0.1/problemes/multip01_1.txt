set etapes 1
set niveaux {0 1 2 4}
::1
set niveau 2
set ope {{2 6} {2 5}}
set interope {{1 10 1} {1 10 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set volatil 0
set fact1 $ope1
for {set k 1} {$k < $ope2} {incr k} {set fact1 [concat $fact1 + $ope1]}
set fact2 $ope2
for {set k 1} {$k < $ope1} {incr k} {set fact2 [concat $fact2 + $ope2]}
regsub -all {[\040]} $fact1 "" fact1
regsub -all {[\040]} $fact2 "" fact2
set operations [list $fact1 $fact2 [list [expr $ope1]*[expr $ope2]] [list [expr $ope2]*[expr $ope1]]]
set enonce "Au jardin.\nJ'ai plant� cette rang�e de [expr $ope2] salades.\nCombien aurai-je de salades si je plante en tout [expr $ope1] rang�es?"
set cible [list [list $ope2 $ope1 [list salade.gif [expr $ope2]] source0]]
set intervalcible 20
set taillerect 50
set orgy 40
set orgxorig 50
set orgsourcey 80
set orgsourcexorig 600
set source {salade.gif}
set orient 0
set labelcible {{Mon jardin}}
set quadri 1
set reponse [list [list {1} [list {Il y a} [expr $ope1*$ope2] {salades dans mon jardin.}]]]
set ensembles [list [expr $ope2*$ope1 - $ope2]]
set canvash 350
set c1height 160
set opnonautorise {0 1}
::




