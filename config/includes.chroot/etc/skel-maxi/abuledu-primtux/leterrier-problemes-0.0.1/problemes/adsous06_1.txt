set etapes 1
set niveaux {0 1 2 3 5}
::1
set niveau 2
set ope {{2 7} {1 4}}
set interope {{1 20 1} {1 20 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set volatil 0
#set operations {{11+15} {15+11}}
set operations [list [list [expr $ope1 ]+[expr $ope2]] [list [expr $ope2]+[expr $ope1]]]
set enonce "Les biscuits.\nTim a mang� $ope1 biscuits.\nTom en a mang� $ope2 de plus.\nCombien Tom en a-t-il mang�?"
set cible {{5 5 {} source0} {5 5 {} source0}}
set intervalcible 20
set taillerect 45
set orgy 40
set orgxorig 20
set orgsourcey 100
set orgsourcexorig 600
set source {biscuit.gif}
set orient 0
set labelcible {Tim Tom}
set quadri 0
set reponse [list [list {1} [list {Il a mang�} [expr $ope1 + $ope2] {biscuits.}]]]
#set ensembles {11 15}
set ensembles [list [expr $ope1] [expr $ope2 + $ope1]]
set canvash 300
set c1height 160
set opnonautorise {}
::