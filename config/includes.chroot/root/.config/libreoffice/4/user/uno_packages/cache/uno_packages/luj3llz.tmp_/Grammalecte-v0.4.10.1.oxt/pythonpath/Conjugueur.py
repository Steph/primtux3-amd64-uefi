# -*- coding: utf8 -*-
# Conjugueur
# by Olivier R.
# License: GPL 3

import unohelper
import uno
import sys
import traceback
import re

import conjfr

from com.sun.star.task import XJobExecutor
from com.sun.star.awt import XActionListener


reSTARTVOY = re.compile(u"^[aeéiouœê]")
reNEEDTEUPH = re.compile(u"[tdc]$")
#reNEEDACCENTWITHJE = re.compile("[^i]e$")

dPROOBJ = { "je": "me ", "tu": "te ", "il": "se ", "elle": "se ", "on": "se ", "nous": "nous ", "vous": "vous ", "ils": "se ", "elles": "se " }
dPROOBJEL = { "je": u"m’", "tu": u"t’", "il": u"s’", "elle": u"s’", "on": u"s’", "nous": "nous ", "vous": "vous ", "ils": u"s’", "elles": u"s’" }
dIMPEPRO = { "2sg": "-toi", "1pl": "-nous", "2pl": "-vous" }
dIMPEPRONEG = { "2sg": "ne te ", "1pl": "ne nous ", "2pl": "ne vous " }
dIMPEPROEN = { "2sg": u"-t’en", "1pl": "-nous-en", "2pl": "-vous-en" }
dIMPEPRONEGEN = { "2sg": u"ne t’en ", "1pl": "ne nous en ", "2pl": "ne vous en " }

dGROUP = { "0": u"verbe auxiliaire", "1": u"verbe du 1ᵉʳ groupe", "2": u"verbe du 2ᵉ groupe", "3": u"verbe du 3ᵉ groupe" }


class Conjugueur (unohelper.Base, XActionListener, XJobExecutor):
    def __init__ (self, ctx):
        self.ctx = ctx
        self.xSvMgr = self.ctx.ServiceManager
        self.xContainer = None
        self.xDialog = None
        self.lDropDown = [u"être", "avoir", "aller", "vouloir", "pouvoir", "devoir", "faire", "envoyer", "prendre", u"connaître", \
                          "savoir", "partir", u"répondre", "dire", "voir", "mettre", "tenir", "sentir", "finir", "manger"]

    def _addWidget (self, name, wtype, x, y, w, h, **kwargs):
        xWidget = self.xDialog.createInstance('com.sun.star.awt.UnoControl%sModel' % wtype)
        xWidget.Name = name
        xWidget.PositionX = x
        xWidget.PositionY = y
        xWidget.Width = w
        xWidget.Height = h
        for k, w in kwargs.items():
            setattr(xWidget, k, w)
        self.xDialog.insertByName(name, xWidget)
        return xWidget

    def run (self, sArgs=""):
        ## dialog
        self.xDialog = self.xSvMgr.createInstanceWithContext('com.sun.star.awt.UnoControlDialogModel', self.ctx)
        self.xDialog.Width = 250
        self.xDialog.Title = u"Grammalecte · Conjugueur"

        xFD0 = uno.createUnoStruct("com.sun.star.awt.FontDescriptor")
        xFD0.Height = 12
        xFD0.Weight = uno.getConstantByName("com.sun.star.awt.FontWeight.BOLD")
        xFD0.Name = "Verdana"

        xFD1 = uno.createUnoStruct("com.sun.star.awt.FontDescriptor")
        xFD1.Height = 16
        xFD1.Name = "Constantia"

        xFD2 = uno.createUnoStruct("com.sun.star.awt.FontDescriptor")
        xFD2.Height = 11
        xFD2.Weight = uno.getConstantByName("com.sun.star.awt.FontWeight.BOLD")
        xFD2.Name = "Constantia"

        xFD3 = uno.createUnoStruct("com.sun.star.awt.FontDescriptor")
        xFD3.Height = 10
        xFD3.Weight = uno.getConstantByName("com.sun.star.awt.FontWeight.BOLD")
        xFD3.Name = "Verdana"

        xFDsmall = uno.createUnoStruct("com.sun.star.awt.FontDescriptor")
        xFDsmall.Height = 6
        xFDsmall.Name = "Verdana"

        ## widgets
        nGroupBoxWith = (self.xDialog.Width - 16) // 2
        nWidth = nGroupBoxWith-10
        nHeight = 10
        nColorHead = 0xAA2200
        nColorHead2 = 0x0022AA

        # grid
        nX1 = 10; nX2 = nX1+123;
        nY0 = 5; nY1 = nY0+15; nY2 = nY1+30; nY3 = nY2+30; nY4 = nY3+45; nY5 = nY4+45; nY6 = nY5+45; nY7 = nY6+45; nY6b = nY6+20; nY7b = nY6b+45

        # input field + button
        self.input = self._addWidget('input', 'ComboBox', nX1-5, nY0, nWidth+10, 16, FontDescriptor = xFD0, TextColor = 0x666666, Dropdown = True, LineCount = 20)
        for n, s in enumerate(self.lDropDown):
            self.input.insertItemText(n, s)
        self.cbutton = self._addWidget('cbutton', 'Button', nX2, nY0, nWidth-30, 16, Label = "Conjuguer", FontDescriptor = xFD0)
        #xray(self.input)

        # group box // indicatif
        gb_infi = self._addWidget('groupbox_infi', 'GroupBox', nX1-5, nY1, nGroupBoxWith, 30, Label = "Infinitif", FontDescriptor = xFD1, FontRelief = 1, TextColor = nColorHead)
        self.infi = self._addWidget('infi', 'FixedText', nX1, nY1+15, nWidth, nHeight, Label = "", FontDescriptor = xFD3)
        self.type1 = self._addWidget('type1', 'FixedText', nX2, nY1+5, nWidth, nHeight)
        self.type2 = self._addWidget('type2', 'FixedText', nX2, nY1+15, nWidth, nHeight)

        # group box // participe présent
        gb_ppre = self._addWidget('groupbox_ppre', 'GroupBox', nX1-5, nY2, nGroupBoxWith, 30, Label = u"Participe présent", FontDescriptor = xFD1, FontRelief = 1, TextColor = nColorHead)
        self.ppre = self._addWidget('ppre', 'FixedText', nX1, nY2+16, nWidth, nHeight, Label = "")

        # options
        self.oneg = self._addWidget('oneg', 'CheckBox', nX2-5, nY2, 120, nHeight, Label = u"négation")
        self.opro = self._addWidget('opro', 'CheckBox', nX2-5, nY2+10, 60, nHeight, Label = u"pronominal")
        self.oint = self._addWidget('oint', 'CheckBox', nX2-5, nY2+20, 60, nHeight, Label = u"interrogation")
        self.otco = self._addWidget('otco', 'CheckBox', nX2+55, nY2+10, 60, nHeight, Label = u"Temps composés")
        self.ofem = self._addWidget('ofem', 'CheckBox', nX2+55, nY2+20, 60, nHeight, Label = u"Féminin")

        # group box // participe passé
        gb_ppas = self._addWidget('groupbox_ppas', 'GroupBox', nX1-5, nY3, nGroupBoxWith, 45, Label = u"Participe passé", FontDescriptor = xFD1, FontRelief = 1, TextColor = nColorHead)
        self.ppas1 = self._addWidget('ppas1', 'FixedText', nX1, nY3+14, nWidth, nHeight, Label = "")
        self.ppas2 = self._addWidget('ppas2', 'FixedText', nX1, nY3+20, nWidth, nHeight, Label = "")
        self.ppas3 = self._addWidget('ppas3', 'FixedText', nX1, nY3+26, nWidth, nHeight, Label = "")
        self.ppas4 = self._addWidget('ppas4', 'FixedText', nX1, nY3+32, nWidth, nHeight, Label = "")

        # group box // impératif
        gb_impe = self._addWidget('groupbox_impe', 'GroupBox', nX2-5, nY3, nGroupBoxWith, 45, Label = u"Impératif", FontDescriptor = xFD1, FontRelief = 1, TextColor = nColorHead)
        self.impe = self._addWidget('impe', 'FixedText', nX2, nY3+12, nWidth, nHeight, Label = u"Présent", FontDescriptor = xFD2, FontRelief = 1, TextColor = nColorHead2)
        self.impe1 = self._addWidget('impe1', 'FixedText', nX2, nY3+20, nWidth, nHeight, Label = "")
        self.impe2 = self._addWidget('impe2', 'FixedText', nX2, nY3+26, nWidth, nHeight, Label = "")
        self.impe3 = self._addWidget('impe3', 'FixedText', nX2, nY3+32, nWidth, nHeight, Label = "")

        # group box // indicatif
        gb_ind = self._addWidget('groupbox_ind', 'GroupBox', nX1-5, nY4, nGroupBoxWith, 198, Label = u"Indicatif", FontDescriptor = xFD1, FontRelief = 1, TextColor = nColorHead)
        self.ipre = self._addWidget('ipre', 'FixedText', nX1, nY4+12, nWidth, nHeight, Label = u"Présent", FontDescriptor = xFD2, FontRelief = 1, TextColor = nColorHead2)
        self.ipre1 = self._addWidget('ipre1', 'FixedText', nX1, nY4+20, nWidth, nHeight, Label = "")
        self.ipre2 = self._addWidget('ipre2', 'FixedText', nX1, nY4+26, nWidth, nHeight, Label = "")
        self.ipre3 = self._addWidget('ipre3', 'FixedText', nX1, nY4+32, nWidth, nHeight, Label = "")
        self.ipre4 = self._addWidget('ipre4', 'FixedText', nX1, nY4+38, nWidth, nHeight, Label = "")
        self.ipre5 = self._addWidget('ipre5', 'FixedText', nX1, nY4+44, nWidth, nHeight, Label = "")
        self.ipre6 = self._addWidget('ipre6', 'FixedText', nX1, nY4+50, nWidth, nHeight, Label = "")

        self.iimp = self._addWidget('iimp', 'FixedText', nX1, nY5+12, nWidth, nHeight, Label = u"Imparfait", FontDescriptor = xFD2, FontRelief = 1, TextColor = nColorHead2)
        self.iimp1 = self._addWidget('iimp1', 'FixedText', nX1, nY5+20, nWidth, nHeight, Label = "")
        self.iimp2 = self._addWidget('iimp2', 'FixedText', nX1, nY5+26, nWidth, nHeight, Label = "")
        self.iimp3 = self._addWidget('iimp3', 'FixedText', nX1, nY5+32, nWidth, nHeight, Label = "")
        self.iimp4 = self._addWidget('iimp4', 'FixedText', nX1, nY5+38, nWidth, nHeight, Label = "")
        self.iimp5 = self._addWidget('iimp5', 'FixedText', nX1, nY5+44, nWidth, nHeight, Label = "")
        self.iimp6 = self._addWidget('iimp6', 'FixedText', nX1, nY5+50, nWidth, nHeight, Label = "")

        self.ipsi = self._addWidget('ipsi', 'FixedText', nX1, nY6+12, nWidth, nHeight, Label = u"Passé Simple", FontDescriptor = xFD2, FontRelief = 1, TextColor = nColorHead2)
        self.ipsi1 = self._addWidget('ipsi1', 'FixedText', nX1, nY6+20, nWidth, nHeight, Label = "")
        self.ipsi2 = self._addWidget('ipsi2', 'FixedText', nX1, nY6+26, nWidth, nHeight, Label = "")
        self.ipsi3 = self._addWidget('ipsi3', 'FixedText', nX1, nY6+32, nWidth, nHeight, Label = "")
        self.ipsi4 = self._addWidget('ipsi4', 'FixedText', nX1, nY6+38, nWidth, nHeight, Label = "")
        self.ipsi5 = self._addWidget('ipsi5', 'FixedText', nX1, nY6+44, nWidth, nHeight, Label = "")
        self.ipsi6 = self._addWidget('ipsi6', 'FixedText', nX1, nY6+50, nWidth, nHeight, Label = "")

        self.ifut = self._addWidget('ifut', 'FixedText', nX1, nY7+12, nWidth, nHeight, Label = u"Futur", FontDescriptor = xFD2, FontRelief = 1, TextColor = nColorHead2)
        self.ifut1 = self._addWidget('ifut1', 'FixedText', nX1, nY7+20, nWidth, nHeight, Label = "")
        self.ifut2 = self._addWidget('ifut2', 'FixedText', nX1, nY7+26, nWidth, nHeight, Label = "")
        self.ifut3 = self._addWidget('ifut3', 'FixedText', nX1, nY7+32, nWidth, nHeight, Label = "")
        self.ifut4 = self._addWidget('ifut4', 'FixedText', nX1, nY7+38, nWidth, nHeight, Label = "")
        self.ifut5 = self._addWidget('ifut5', 'FixedText', nX1, nY7+44, nWidth, nHeight, Label = "")
        self.ifut6 = self._addWidget('ifut6', 'FixedText', nX1, nY7+50, nWidth, nHeight, Label = "")

        self.infomsg = self._addWidget('infomsg', 'FixedText', nX1-5, nY7+66, 120, 20, Label = u"De nombreux verbes n’ont pas encore été étiquetés et contrôlés. Pour ceux-ci, les options “usage pronominal” et “temps composés” sont désactivées.", FontDescriptor = xFDsmall, MultiLine = True, TextColor = 0x333333)

        # group box // subjonctif
        gb_sub = self._addWidget('groupbox_sub', 'GroupBox', nX2-5, nY4, nGroupBoxWith, 108, Label = u"Subjonctif", FontDescriptor = xFD1, FontRelief = 1, TextColor = nColorHead)
        self.spre = self._addWidget('spre', 'FixedText', nX2, nY4+12, nWidth, nHeight, Label = u"Présent", FontDescriptor = xFD2, FontRelief = 1, TextColor = nColorHead2)
        self.spre1 = self._addWidget('spre1', 'FixedText', nX2, nY4+20, nWidth, nHeight, Label = "")
        self.spre2 = self._addWidget('spre2', 'FixedText', nX2, nY4+26, nWidth, nHeight, Label = "")
        self.spre3 = self._addWidget('spre3', 'FixedText', nX2, nY4+32, nWidth, nHeight, Label = "")
        self.spre4 = self._addWidget('spre4', 'FixedText', nX2, nY4+38, nWidth, nHeight, Label = "")
        self.spre5 = self._addWidget('spre5', 'FixedText', nX2, nY4+44, nWidth, nHeight, Label = "")
        self.spre6 = self._addWidget('spre6', 'FixedText', nX2, nY4+50, nWidth, nHeight, Label = "")

        self.simp = self._addWidget('simp', 'FixedText', nX2, nY5+12, nWidth, nHeight, Label = u"Imparfait", FontDescriptor = xFD2, FontRelief = 1, TextColor = nColorHead2)
        self.simp1 = self._addWidget('simp1', 'FixedText', nX2, nY5+20, nWidth, nHeight, Label = "")
        self.simp2 = self._addWidget('simp2', 'FixedText', nX2, nY5+26, nWidth, nHeight, Label = "")
        self.simp3 = self._addWidget('simp3', 'FixedText', nX2, nY5+32, nWidth, nHeight, Label = "")
        self.simp4 = self._addWidget('simp4', 'FixedText', nX2, nY5+38, nWidth, nHeight, Label = "")
        self.simp5 = self._addWidget('simp5', 'FixedText', nX2, nY5+44, nWidth, nHeight, Label = "")
        self.simp6 = self._addWidget('simp6', 'FixedText', nX2, nY5+50, nWidth, nHeight, Label = "")

        # group box // conditionnel
        gb_cond = self._addWidget('groupbox_cond', 'GroupBox', nX2-5, nY6b, nGroupBoxWith, 108, Label = u"Conditionnel", FontDescriptor = xFD1, FontRelief = 1, TextColor = nColorHead)
        self.conda = self._addWidget('conda', 'FixedText', nX2, nY6b+12, nWidth, nHeight, Label = u"Présent", FontDescriptor = xFD2, FontRelief = 1, TextColor = nColorHead2)
        self.conda1 = self._addWidget('conda1', 'FixedText', nX2, nY6b+20, nWidth, nHeight, Label = "")
        self.conda2 = self._addWidget('conda2', 'FixedText', nX2, nY6b+26, nWidth, nHeight, Label = "")
        self.conda3 = self._addWidget('conda3', 'FixedText', nX2, nY6b+32, nWidth, nHeight, Label = "")
        self.conda4 = self._addWidget('conda4', 'FixedText', nX2, nY6b+38, nWidth, nHeight, Label = "")
        self.conda5 = self._addWidget('conda5', 'FixedText', nX2, nY6b+44, nWidth, nHeight, Label = "")
        self.conda6 = self._addWidget('conda6', 'FixedText', nX2, nY6b+50, nWidth, nHeight, Label = "")

        self.condb = self._addWidget('condb', 'FixedText', nX2, nY7b+12, nWidth, nHeight, Label = "", FontDescriptor = xFD2, FontRelief = 1, TextColor = nColorHead2)
        self.condb1 = self._addWidget('condb1', 'FixedText', nX2, nY7b+20, nWidth, nHeight, Label = "")
        self.condb2 = self._addWidget('condb2', 'FixedText', nX2, nY7b+26, nWidth, nHeight, Label = "")
        self.condb3 = self._addWidget('condb3', 'FixedText', nX2, nY7b+32, nWidth, nHeight, Label = "")
        self.condb4 = self._addWidget('condb4', 'FixedText', nX2, nY7b+38, nWidth, nHeight, Label = "")
        self.condb5 = self._addWidget('condb5', 'FixedText', nX2, nY7b+44, nWidth, nHeight, Label = "")
        self.condb6 = self._addWidget('condb6', 'FixedText', nX2, nY7b+50, nWidth, nHeight, Label = "")

        # dialog height
        self.xDialog.Height = 350

        ## container
        self.xContainer = self.xSvMgr.createInstanceWithContext('com.sun.star.awt.UnoControlDialog', self.ctx)
        self.xContainer.setModel(self.xDialog)
        self.xContainer.setVisible(False)

        #self.xContainer.getControl('input').addEventListener(self)
        #self.xContainer.getControl('input').addEventCommand('New')
        self.xContainer.getControl('cbutton').addActionListener(self)
        self.xContainer.getControl('cbutton').setActionCommand('New')
        self.xContainer.getControl('oneg').addActionListener(self)
        self.xContainer.getControl('oneg').setActionCommand('Change')
        self.xContainer.getControl('opro').addActionListener(self)
        self.xContainer.getControl('opro').setActionCommand('Change')
        self.xContainer.getControl('oint').addActionListener(self)
        self.xContainer.getControl('oint').setActionCommand('Change')
        self.xContainer.getControl('otco').addActionListener(self)
        self.xContainer.getControl('otco').setActionCommand('Change')
        self.xContainer.getControl('ofem').addActionListener(self)
        self.xContainer.getControl('ofem').setActionCommand('Change')

        ## set verb
        self.input.Text = sArgs  if sArgs  else u"être"
        self._newVerb()

        ## mysterious action
        xToolkit = self.xSvMgr.createInstanceWithContext('com.sun.star.awt.ExtToolkit', self.ctx)
        self.xContainer.createPeer(xToolkit, None)
        self.xContainer.execute()

    # XActionListener
    def actionPerformed (self, xActionEvent):
        try:
            if xActionEvent.ActionCommand == 'Close':
                self.xContainer.endExecute()
            elif xActionEvent.ActionCommand == 'New':
                self._newVerb()
            elif xActionEvent.ActionCommand == 'Change':
                if not self.otco.State:
                    self.sVerbCj = self.sVerbRq
                self._displayResults()
            else:
                print(str(xActionEvent))
        except:
            traceback.print_exc()

    # XJobExecutor
    def trigger (self, args):
        try:
            xDialog = Conjugueur(self.ctx)
            xDialog.run(args)
        except:
            traceback.print_exc()

    def _newVerb (self):
        self.oneg.State = False
        self.opro.State = False
        self.oint.State = False
        self.otco.State = False
        self.ofem.State = False
        # request analyzing
        self.sVerbRq = self.input.Text.strip().lower().replace(u"’", "'").replace("  ", " ")
        if self.sVerbRq:
            if self.sVerbRq.startswith("ne pas "):
                self.oneg.State = True
                self.sVerbRq = self.sVerbRq[7:]
            if self.sVerbRq.startswith("se "):
                self.opro.State = True
                self.sVerbRq = self.sVerbRq[3:]
            elif self.sVerbRq.startswith("s'"):
                self.opro.State = True
                self.sVerbRq = self.sVerbRq[2:]
            if self.sVerbRq.endswith("?"):
                self.oint.State = True
                self.sVerbRq = self.sVerbRq.rstrip(" ?")
            if not conjfr.isVerb(self.sVerbRq):
                self.input.TextColor = 0xAA2200
            else:
                self.input.TextColor = 0x666666
                self.sVerbCj = self.sVerbRq
                self.tTagsRq = conjfr.getTags(self.sVerbRq)
                self.sVcode = conjfr.getVtyp(self.sVerbRq)
                self._setInfoLabels()
                if self.sVcode.endswith("??"):
                    self.opro.State = False
                    self.opro.Label = u"désactivé"
                    self.opro.Enabled = False
                    self.otco.State = False
                    self.otco.Enabled = False
                else:
                    if self.sVcode[5] == "_":
                        self.opro.Label = "non applicable"
                        self.opro.State = False
                        self.opro.Enabled = False
                    elif self.sVcode[5] == "q" or self.sVcode[5] == "e":
                        self.opro.State = False
                        self.opro.Enabled = True
                    elif self.sVcode[5] == "p" or self.sVcode[5] == "r":
                        self.opro.State = True
                        self.opro.Enabled = False
                    elif self.sVcode[5] == "!":
                        self.opro.Label = "cas spécifiques"
                        self.opro.State = False
                        self.opro.Enabled = False
                    else:
                        self.opro.Label = "# erreur #"
                        self.opro.State = False
                        self.opro.Enabled = False
                    self.otco.Enabled = True
                self.bProWithEn = self.sVcode[5] == "e"
                self._displayResults()

    def _displayResults (self):
        try:
            self._setTitles()
            # participes passés
            self.ppas1.Label = conjfr.getConjWithTags(self.sVerbRq, self.tTagsRq, "pp", "ppas1")
            self.ppas2.Label = conjfr.getConjWithTags(self.sVerbRq, self.tTagsRq, "pp", "ppas2")
            self.ppas3.Label = conjfr.getConjWithTags(self.sVerbRq, self.tTagsRq, "pp", "ppas3")
            self.ppas4.Label = conjfr.getConjWithTags(self.sVerbRq, self.tTagsRq, "pp", "ppas4")
            # temps composés ?
            if self.otco.State:
                if self.opro.State or self.sVcode[7] == "e":
                    self.sVerbCj = u"être"
                else:
                    self.sVerbCj = u"avoir"
                self.tTagsCj = conjfr.getTags(self.sVerbCj)
            else:
                self.tTagsCj = self.tTagsRq
            # options labels
            self._setOptionLabels()
            # infinitif
            self.infi.Label = self._infi()
            # participe présent
            self.ppre.Label = self._ppre()
            # conjugaisons
            if not self.ofem.State:
                sPro3sg = "il"
                sPro3pl = "ils"
            else:
                sPro3sg = "elle"
                sPro3pl = "elles"
            if self.sVcode[5] == "r":
                sPro3sg = "on"
            self.ipre1.Label = self._conj("ipre", "1sg", "je")
            self.ipre2.Label = self._conj("ipre", "2sg", "tu")
            self.ipre3.Label = self._conj("ipre", "3sg", sPro3sg)
            self.ipre4.Label = self._conj("ipre", "1pl", "nous", True)
            self.ipre5.Label = self._conj("ipre", "2pl", "vous", True)
            self.ipre6.Label = self._conj("ipre", "3pl", sPro3pl, True)
            self.iimp1.Label = self._conj("iimp", "1sg", "je")
            self.iimp2.Label = self._conj("iimp", "2sg", "tu")
            self.iimp3.Label = self._conj("iimp", "3sg", sPro3sg)
            self.iimp4.Label = self._conj("iimp", "1pl", "nous", True)
            self.iimp5.Label = self._conj("iimp", "2pl", "vous", True)
            self.iimp6.Label = self._conj("iimp", "3pl", sPro3pl, True)
            self.ipsi1.Label = self._conj("ipsi", "1sg", "je")
            self.ipsi2.Label = self._conj("ipsi", "2sg", "tu")
            self.ipsi3.Label = self._conj("ipsi", "3sg", sPro3sg)
            self.ipsi4.Label = self._conj("ipsi", "1pl", "nous", True)
            self.ipsi5.Label = self._conj("ipsi", "2pl", "vous", True)
            self.ipsi6.Label = self._conj("ipsi", "3pl", sPro3pl, True)
            self.ifut1.Label = self._conj("ifut", "1sg", "je")
            self.ifut2.Label = self._conj("ifut", "2sg", "tu")
            self.ifut3.Label = self._conj("ifut", "3sg", sPro3sg)
            self.ifut4.Label = self._conj("ifut", "1pl", "nous", True)
            self.ifut5.Label = self._conj("ifut", "2pl", "vous", True)
            self.ifut6.Label = self._conj("ifut", "3pl", sPro3pl, True)
            self.conda1.Label = self._conj("cond", "1sg", "je")
            self.conda2.Label = self._conj("cond", "2sg", "tu")
            self.conda3.Label = self._conj("cond", "3sg", sPro3sg)
            self.conda4.Label = self._conj("cond", "1pl", "nous", True)
            self.conda5.Label = self._conj("cond", "2pl", "vous", True)
            self.conda6.Label = self._conj("cond", "3pl", sPro3pl, True)
            if not self.oint.State:
                self.spre1.Label = self._conj("spre", "1sg", "je")
                self.spre2.Label = self._conj("spre", "2sg", "tu")
                self.spre3.Label = self._conj("spre", "3sg", sPro3sg)
                self.spre4.Label = self._conj("spre", "1pl", "nous", True)
                self.spre5.Label = self._conj("spre", "2pl", "vous", True)
                self.spre6.Label = self._conj("spre", "3pl", sPro3pl, True)
                self.simp1.Label = self._conj("simp", "1sg", "je")
                self.simp2.Label = self._conj("simp", "2sg", "tu")
                self.simp3.Label = self._conj("simp", "3sg", sPro3sg)
                self.simp4.Label = self._conj("simp", "1pl", "nous", True)
                self.simp5.Label = self._conj("simp", "2pl", "vous", True)
                self.simp6.Label = self._conj("simp", "3pl", sPro3pl, True)
                self.impe1.Label = self._impe("2sg")
                self.impe2.Label = self._impe("1pl", True)
                self.impe3.Label = self._impe("2pl", True)
            else:
                self.spre.Label = ""
                self.spre1.Label = ""
                self.spre2.Label = ""
                self.spre3.Label = ""
                self.spre4.Label = ""
                self.spre5.Label = ""
                self.spre6.Label = ""
                self.simp.Label = ""
                self.simp1.Label = ""
                self.simp2.Label = ""
                self.simp3.Label = ""
                self.simp4.Label = ""
                self.simp5.Label = ""
                self.simp6.Label = ""
                self.impe.Label = ""
                self.impe1.Label = ""
                self.impe2.Label = ""
                self.impe3.Label = ""
            if self.otco.State:
                self.condb1.Label = self._conj("simp", "1sg", "je")
                self.condb2.Label = self._conj("simp", "2sg", "tu")
                self.condb3.Label = self._conj("simp", "3sg", sPro3sg)
                self.condb4.Label = self._conj("simp", "1pl", "nous", True)
                self.condb5.Label = self._conj("simp", "2pl", "vous", True)
                self.condb6.Label = self._conj("simp", "3pl", sPro3pl, True)
            else:
                self.condb1.Label = ""
                self.condb2.Label = ""
                self.condb3.Label = ""
                self.condb4.Label = ""
                self.condb5.Label = ""
                self.condb6.Label = ""
            self.input.Text = ""
            # refresh
            self.xContainer.setVisible(True)
        except:
            traceback.print_exc()

    def _setTitles (self):
        if not self.otco.State:
            self.ipre.Label = u"Présent"
            self.ifut.Label = u"Futur"
            self.iimp.Label = u"Imparfait"
            self.ipsi.Label = u"Passé simple"
            self.spre.Label = u"Présent"
            self.simp.Label = u"Imparfait"
            self.conda.Label = u"Présent"
            self.condb.Label = u""
            self.impe.Label = u"Présent"
        else:
            self.ipre.Label = u"Passé composé"
            self.ifut.Label = u"Futur antérieur"
            self.iimp.Label = u"Plus-que-parfait"
            self.ipsi.Label = u"Passé antérieur"
            self.spre.Label = u"Passé"
            self.simp.Label = u"Plus-que-parfait"
            self.conda.Label = u"Passé (1ʳᵉ forme)"
            self.condb.Label = u"Passé (2ᵉ forme)"
            self.impe.Label = u"Passé"

    def _setInfoLabels (self):
        self.type1.Label = dGROUP[self.sVcode[0]]
        s = u""
        if self.sVcode[3] == "t":
            s = u"transitif"
        elif self.sVcode[4] == "n":
            s = u"transitif indirect"
        elif self.sVcode[2] == "i":
            s = u"intransitif"
        elif self.sVcode[5] == "r":
            s = u"pronominal réciproque"
        elif self.sVcode[5] == "p":
            s = u"pronominal"
        if self.sVcode[5] == "q" or self.sVcode[5] == "e":
            s = s + u" (+ usage pronominal)"
        if not s:
            s = u"# erreur - code : " + self.sVcode
        if self.sVcode[6] == "m":
            s = s + u" impersonnel"
        self.type2.Label = s

    def _setOptionLabels (self):
        try:
            # option pronominale
            if self.opro.Enabled or self.opro.State:
                if not self.otco.State:
                    if self.bProWithEn:
                        self.opro.Label = u"s’en " + self.sVerbCj
                    else:
                        self.opro.Label = u"s’" + self.sVerbCj  if reSTARTVOY.search(self.sVerbCj)  else  "se " + self.sVerbCj
                else:
                    self.opro.Label = u"s’en être " + self._getPpas()  if self.bProWithEn  else  u"s’être " + self._getPpas()
            # options négation et interrogation
            if self.opro.State and not self.opro.Enabled:
                self.oneg.Label = u"ne pas " + self.opro.Label
                self.oint.Label = self.opro.Label + " ?"
            else:
                if not self.otco.State:
                    self.oneg.Label = u"ne pas " + self.sVerbCj
                    self.oint.Label = self.sVerbCj + " ?"
                else:
                    self.oneg.Label = u"ne pas " + self.sVerbCj + " " + self._getPpas()
                    self.oint.Label = self.sVerbCj + " " + self._getPpas() + " ?"
        except:
            traceback.print_exc()

    def _infi (self, bPlur=False):
        s = self.sVerbCj
        if self.opro.State:
            if self.bProWithEn:
                s = u"s’en " + s
            else:
                s = u"s’" + s  if reSTARTVOY.search(s)  else  u"se " + s
        if self.oneg.State:
            s = u"ne pas " + s
        if self.otco.State:
            s += u" " + self._getPpas(bPlur)
        if self.oint.State:
            s += u" … ?"
        return s

    def _ppre (self, bPlur=False):
        if not conjfr.hasConjWithTags(self.tTagsRq, "pp", "ppre"):
            return ""
        s = conjfr.getConjWithTags(self.sVerbCj, self.tTagsCj, "pp", "ppre")
        if not s:
            return ""
        bEli = True  if reSTARTVOY.search(s)  else  False
        if self.opro.State:
            if self.bProWithEn:
                s = u"s’en " + s
            else:
                s = u"s’" + s  if bEli  else  u"se " + s
        if self.oneg.State:
            if bEli and not self.opro.State:
                s = u"n’" + s + " pas"
            else:
                s = u"ne " + s + " pas"
        if self.otco.State:
            s += u" " + self._getPpas(bPlur)
        if self.oint.State:
            s += u" … ?"
        return s

    def _conj (self, sTemps, sWho, sProSuj, bPlur=False):
        if not conjfr.hasConjWithTags(self.tTagsRq, sTemps, sWho):
            return ""
        if self.oint.State and sWho == "1sg" and conjfr.hasConjWithTags(self.tTagsCj, sTemps, "1isg"):
            sWho = "1isg"
        s = conjfr.getConjWithTags(self.sVerbCj, self.tTagsCj, sTemps, sWho)
        if not s:
            return ""
        bEli = True  if reSTARTVOY.search(s)  else  False
        if self.opro.State:
            if not self.bProWithEn:
                s = dPROOBJEL[sProSuj] + s  if bEli  else dPROOBJ[sProSuj] + s
            else:
                s = dPROOBJEL[sProSuj] + "en " + s
        if self.oneg.State:
            s = u"n’" + s  if bEli and not self.opro.State  else  u"ne " + s
        if self.oint.State:
            if (sProSuj == "il" or sProSuj == "elle" or sProSuj == "on") and not reNEEDTEUPH.search(s):
                s += u"-t"
            s += u"-" + sProSuj
        else:
            if sProSuj == "je" and bEli and not self.oneg.State and not self.opro.State:
                s = u"j’" + s
            else:
                s = sProSuj + " " + s
        if self.oneg.State:
            s += u" pas"
        if self.otco.State:
            s += u" " + self._getPpas(bPlur)
        if self.oint.State:
            s += u" … ?"
        return s

    def _impe (self, sWho, bPlur=False):
        if not conjfr.hasConjWithTags(self.tTagsRq, "impe", sWho):
            return ""
        s = conjfr.getConjWithTags(self.sVerbCj, self.tTagsCj, "impe", sWho)
        if not s:
            return ""
        bEli = True  if reSTARTVOY.search(s)  else  False
        if self.oneg.State:
            if self.opro.State:
                if not self.bProWithEn:
                    if bEli and sWho == "2sg":
                        s = u"ne t’" + s + " pas"
                    else:
                        s = dIMPEPRONEG[sWho] + s + " pas"
                else:
                    s = dIMPEPRONEGEN[sWho] + s + " pas"
            else:
                s = u"n’" + s + " pas"  if bEli  else  u"ne " + s + " pas"
        elif self.opro.State:
            s = s + dIMPEPROEN[sWho]  if self.bProWithEn  else  s + dIMPEPRO[sWho]
        if self.otco.State:
            return s + " " + self._getPpas(bPlur)
        return s

    def _getPpas (self, bPlur=False):
        if self.sVerbCj == "avoir":
            return self.ppas1.Label
        if not self.ofem.State:
            return self.ppas2.Label  if bPlur and self.ppas2.Label  else  self.ppas1.Label
        if not bPlur:
            return self.ppas3.Label  if self.ppas3.Label  else  self.ppas1.Label
        return self.ppas4.Label  if self.ppas4.Label  else  self.ppas1.Label


# g_ImplementationHelper = unohelper.ImplementationHelper()
# g_ImplementationHelper.addImplementation(Conjugueur, 'dicollecte.Conjugueur', ('com.sun.star.task.Job',))
