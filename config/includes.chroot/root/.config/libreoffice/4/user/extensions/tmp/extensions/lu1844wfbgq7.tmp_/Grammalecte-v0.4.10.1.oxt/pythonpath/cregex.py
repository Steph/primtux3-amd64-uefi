# -*- encoding: UTF-8 -*-
# Grammalecte - Compiled regular expressions

import re

#### Next or previous word
NextWord = re.compile(u" +(\w[\w-]*)")
PrevWord = re.compile(u"(\w[\w-]*) +$")

#### Lemme
Lemma = re.compile("st:(\w[\w-]+)")

#### Analyses
GENDER = re.compile("is:(mas|fem|epi)")
NUMBER = re.compile("is:(sg|pl|inv)")

#### Nom et adjectif
NA = re.compile("po:(?:nom|adj)")

## nombre
NAs = re.compile("po:(?:nom|adj) .*is:sg")
NAp = re.compile("po:(?:nom|adj) .*is:pl")
NAi = re.compile("po:(?:nom|adj) .*is:inv")
NAsi = re.compile("po:(?:nom|adj) .*is:(?:sg|inv)")
NApi = re.compile("po:(?:nom|adj) .*is:(?:pl|inv)")

## genre
NAm = re.compile("po:(?:nom|adj) .*is:mas")
NAf = re.compile("po:(?:nom|adj) .*is:fem")
NAe = re.compile("po:(?:nom|adj) .*is:epi")
NAme = re.compile("po:(?:nom|adj) .*is:(?:mas|epi)")
NAfe = re.compile("po:(?:nom|adj) .*is:(?:fem|epi)")

## nombre et genre
# singuilier
NAms = re.compile("po:(?:nom|adj) .*is:mas .*is:sg")
NAfs = re.compile("po:(?:nom|adj) .*is:fem .*is:sg")
NAes = re.compile("po:(?:nom|adj) .*is:epi .*is:sg")
NAmes = re.compile("po:(?:nom|adj) .*is:(?:mas|epi) .*is:sg")
NAfes = re.compile("po:(?:nom|adj) .*is:(?:fem|epi) .*is:sg")

# singulier et invariable
NAmsi = re.compile("po:(?:nom|adj) .*is:mas .*is:(?:sg|inv)")
NAfsi = re.compile("po:(?:nom|adj) .*is:fem .*is:(?:sg|inv)")
NAesi = re.compile("po:(?:nom|adj) .*is:epi .*is:(?:sg|inv)")
NAmesi = re.compile("po:(?:nom|adj) .*is:(?:mas|epi) .*is:(?:sg|inv)")
NAfesi = re.compile("po:(?:nom|adj) .*is:(?:fem|epi) .*is:(?:sg|inv)")

# pluriel
NAmp = re.compile("po:(?:nom|adj) .*is:mas .*is:pl")
NAfp = re.compile("po:(?:nom|adj) .*is:fem .*is:pl")
NAep = re.compile("po:(?:nom|adj) .*is:epi .*is:pl")
NAmep = re.compile("po:(?:nom|adj) .*is:(?:mas|epi) .*is:pl")
NAfep = re.compile("po:(?:nom|adj) .*is:(?:fem|epi) .*is:pl")

# pluriel et invariable
NAmpi = re.compile("po:(?:nom|adj) .*is:mas .*is:(?:pl|inv)")
NAfpi = re.compile("po:(?:nom|adj) .*is:fem .*is:(?:pl|inv)")
NAepi = re.compile("po:(?:nom|adj) .*is:epi .*is:(?:pl|inv)")
NAmepi = re.compile("po:(?:nom|adj) .*is:(?:mas|epi) .*is:(?:pl|inv)")
NAfepi = re.compile("po:(?:nom|adj) .*is:(?:fem|epi) .*is:(?:pl|inv)")

# divers
AD = re.compile("po:(?:adj|nb)")

#### Verbe
Vconj = re.compile("po:[123](?:sg|pl)")
Vconj123 = re.compile("po:v[123].* po:[123](?:sg|pl)")

#### Nom | Adjectif | Verbe
NVconj = re.compile("po:(nom|[123](?:sg|pl))")
NAVconj = re.compile("po:(nom|adj|[123](?:sg|pl))")

#### Spécifique
NnotA = re.compile("po:(?:nom(?! po:adj))")
PNnotA = re.compile("po:(?:nom(?! po:adj)|ppas)")

#### Noms propres
NP = re.compile("po:(?:prn|patr|npr|titr)")
NPm = re.compile("po:(?:prn|patr|npr|titr) is:mas")
NPf = re.compile("po:(?:prn|patr|npr|titr) is:fem")
NPe = re.compile("po:(?:prn|patr|npr|titr) is:epi")


#### FONCTIONS

def getLemmaOfMorph (s):
    return Lemma.search(s).group(1)

def checkAgreement (l1, l2, sReqMorph):
    # check number agreement
    if not mbInv(l1) and not mbInv(l2):
        if mbSg(l1) and not mbSg(l2):
            return False
        if mbPl(l1) and not mbPl(l2):
            return False
    # check gender agreement
    if mbEpi(l1) or mbEpi(l2):
        return True
    if mbMas(l1) and not mbMas(l2):
        return False
    if mbFem(l1) and not mbFem(l2):
        return False
    return True

def checkConjVerb (lMorph, sReqConj):
    for s in lMorph:
        if sReqConj in s:
            return True
    return False

def getGender (lMorph):
    "returns gender of word ('mas', 'fem', 'epi' or empty string)."
    sGender = ""
    for sMorph in lMorph:
        m = GENDER.search(sMorph)
        if m:
            if not sGender:
                sGender = m.group(1)
            elif sGender != m.group(1):
                return "epi"
    return sGender

def getNumber (lMorph):
    "returns number of word ('sg', 'pl', 'inv' or empty string)."
    sNumber = ""
    for sMorph in lMorph:
        m = NUMBER.search(sWord)
        if m:
            if not sNumber:
                sNumber = m.group(1)
            elif sNumber != m.group(1):
                return "inv"
    return sNumber

# NOTE :  isWhat (lMorph)    returns True   if lMorph contains nothing else than What
#         mbWhat (lMorph)    returns True   if lMorph contains What at least once

## isXXX = it’s certain

def isNom (lMorph):
    for s in lMorph:
        if "po:nom" not in s:
            return False
    return True

def isNomNotAdj (lMorph):
    for s in lMorph:
        if not NnotA.search(s):
            return False
    return True

def isAdj (lMorph):
    for s in lMorph:
        if "po:adj" not in s:
            return False
    return True

def isNomAdj (lMorph):
    for s in lMorph:
        if not NA.search(s):
            return False
    return True

def isNomVconj (lMorph):
    for s in lMorph:
        if not NVconj.search(s):
            return False
    return True

def isInv (lMorph):
    for s in lMorph:
        if not "is:inv" in s:
            return False
    return True
def isSg (lMorph):
    for s in lMorph:
        if not "is:sg" in s:
            return False
    return True
def isPl (lMorph):
    for s in lMorph:
        if not "is:pl" in s:
            return False
    return True
def isEpi (lMorph):
    for s in lMorph:
        if not "is:epi" in s:
            return False
    return True
def isMas (lMorph):
    for s in lMorph:
        if not "is:mas" in s:
            return False
    return True
def isFem (lMorph):
    for s in lMorph:
        if not "is:fem" in s:
            return False
    return True


## mbXXX = MAYBE XXX

def mbNom (lMorph):
    for s in lMorph:
        if "po:nom" in s:
            return True
    return False

def mbAdj (lMorph):
    for s in lMorph:
        if "po:adj" in s:
            return True
    return False

def mbAdjNb (lMorph):
    for s in lMorph:
        if AD.search(s):
            return True
    return False

def mbNomAdj (lMorph):
    for s in lMorph:
        if NA.search(s):
            return True
    return False

def mbNomNotAdj (lMorph):
    b = False
    for s in lMorph:
        if "po:adj" in s:
            return False
        if "po:nom" in s:
            b = True
    return b

def mbPpasNomNotAdj (lMorph):
    for s in lMorph:
        if PNnotA.search(s):
            return True
    return False

def mbVconj (lMorph):
    for s in lMorph:
        if Vconj.search(s):
            return True
    return False

def mbVconj123 (lMorph):
    for s in lMorph:
        if Vconj123.search(s):
            return True
    return False

def mbMG (lMorph):
    for s in lMorph:
        if "po:mg" in s:
            return True
    return False

def mbInv (lMorph):
    for s in lMorph:
        if "is:inv" in s:
            return True
    return False
def mbSg (lMorph):
    for s in lMorph:
        if "is:sg" in s:
            return True
    return False
def mbPl (lMorph):
    for s in lMorph:
        if "is:pl" in s:
            return True
    return False
def mbEpi (lMorph):
    for s in lMorph:
        if "is:epi" in s:
            return True
    return False
def mbMas (lMorph):
    for s in lMorph:
        if "is:mas" in s:
            return True
    return False
def mbFem (lMorph):
    for s in lMorph:
        if "is:fem" in s:
            return True
    return False

def mbNpr (lMorph):
    for s in lMorph:
        if NP.search(s):
            return True
    return False

def mbNprMasNotFem (lMorph):
    for s in lMorph:
        if NPf.search(s):
            return False
    for s in lMorph:
        if NPm.search(s):
            return True
    return False
