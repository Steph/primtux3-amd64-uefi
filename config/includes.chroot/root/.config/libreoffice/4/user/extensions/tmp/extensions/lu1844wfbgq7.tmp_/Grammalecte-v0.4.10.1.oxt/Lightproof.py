# -*- encoding: UTF-8 -*-
# Lightproof grammar checker for LibreOffice and OpenOffice
# 2009-2012 (c) Laszlo Nemeth (nemeth at numbertext org), license: MPL 1.1 / GPLv3+ / LGPLv3+

import uno
import unohelper
import os
import sys
import traceback

from com.sun.star.linguistic2 import XProofreader, XSupportedLocales
from com.sun.star.linguistic2 import ProofreadingResult
from com.sun.star.lang import XServiceInfo, XServiceName, XServiceDisplayName
from com.sun.star.lang import Locale

from lightproof_impl_grammalecte import locales
from lightproof_impl_grammalecte import pkg
import lightproof_impl_grammalecte as impl
#import lightproof_handler_grammalecte as opthandler
import Options

#import time


class Lightproof (unohelper.Base, XProofreader, XServiceInfo, XServiceName, XServiceDisplayName, XSupportedLocales):

    def __init__ (self, ctx, *args):
        self.ctx = ctx
        self.ServiceName = "com.sun.star.linguistic2.Proofreader"
        self.ImplementationName = "org.openoffice.comp.pyuno.Lightproof." + pkg
        self.SupportedServiceNames = (self.ServiceName, )
        self.locales = []
        for i in locales:
            l = locales[i]
            self.locales.append(Locale(l[0], l[1], l[2]))
        self.locales = tuple(self.locales)
        # spellchecker
        currentContext = uno.getComponentContext()
        impl.SMGR = currentContext.ServiceManager
        impl.spellchecker = impl.SMGR.createInstanceWithContext("com.sun.star.linguistic2.SpellChecker", currentContext)
        # load options
        #opthandler.load(currentContext)
        Options.load(currentContext)
        # store for results of big paragraphs
        self.dResult = {}

    # XServiceName method implementations
    def getServiceName (self):
        return self.ImplementationName

    # XServiceInfo method implementations
    def getImplementationName (self):
        return self.ImplementationName

    def supportsService (self, ServiceName):
        return (ServiceName in self.SupportedServiceNames)

    def getSupportedServiceNames (self):
        return self.SupportedServiceNames

    # XSupportedLocales
    def hasLocale (self, aLocale):
        if aLocale in self.locales:
            return True
        for i in self.locales:
            if (i.Country == aLocale.Country or i.Country == "") and aLocale.Language == i.Language:
                return True
        return False

    def getLocales (self):
        return self.locales

    # XProofreader
    def isSpellChecker (self):
        return False

    def doProofreading (self, nDocId, rText, rLocale, nStartOfSentencePos, nSuggestedSentenceEndPos, rProperties):
        xRes = ProofreadingResult()
        #xRes = uno.createUnoStruct("com.sun.star.linguistic2.ProofreadingResult")
        xRes.aDocumentIdentifier = nDocId
        xRes.aText = rText
        xRes.aLocale = rLocale
        xRes.nStartOfSentencePosition = nStartOfSentencePos
        xRes.nStartOfNextSentencePosition = nSuggestedSentenceEndPos
        xRes.aProperties = ()
        xRes.xProofreader = self
        xRes.aErrors = ()

        # PATCH FOR LO 4
        # Fix for http://nabble.documentfoundation.org/Grammar-checker-Undocumented-change-in-the-API-for-LO-4-td4030639.html
        if nStartOfSentencePos != 0:
            return xRes
        xRes.nStartOfNextSentencePosition = len(rText)
        # END OF PATCH

        # WORKAROUND FOR AVOIDING REPEATED ACTIONS ON HEAVY PARAGRAPHS
        if xRes.nStartOfNextSentencePosition > 3000:
            nHashedVal = hash(rText)
            if nHashedVal in self.dResult:
                return self.dResult[nHashedVal]
        # WORKAROUND ->>>

        xRes.nBehindEndOfSentencePosition = xRes.nStartOfNextSentencePosition

        #start = time.clock()

        try:
            xRes.aErrors = impl.parse(rText, rLocale, nStartOfSentencePos, xRes.nBehindEndOfSentencePosition, rProperties)
            # ->>> WORKAROUND
            if xRes.nStartOfNextSentencePosition > 3000:
                if len(self.dResult) > 1000:
                    self.dResult = {}
                self.dResult[nHashedVal] = xRes
            # END OF WORKAROUND
        except Exception as e:
            if sys.version_info.major == 3:
                traceback.print_exc()

        #end = time.clock()
        #echo("elapsed time: %s\t <  %s" % (end-start, sText[:10]))
        return xRes

    def ignoreRule (self, rid, aLocale):
        impl.aIgnoredRules.add(rid)

    def resetIgnoreRules (self):
        impl.aIgnoredRules.clear()

    # XServiceDisplayName
    def getServiceDisplayName (self, aLocale):
        return impl.name


g_ImplementationHelper = unohelper.ImplementationHelper()
g_ImplementationHelper.addImplementation(Lightproof, "org.openoffice.comp.pyuno.Lightproof."+pkg, ("com.sun.star.linguistic2.Proofreader",),)

# g_ImplementationHelper.addImplementation( opthandler.LightproofOptionsEventHandler, \
#     "org.openoffice.comp.pyuno.LightproofOptionsEventHandler." + pkg, ("com.sun.star.awt.XContainerWindowEventHandler",),)
